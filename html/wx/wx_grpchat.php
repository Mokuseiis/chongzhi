<?php

require_once("../CommonUtil.php");
require_once('../../wxsdk/WXBizMsgCrypt.php');
require_once('../../mysqli_connect.php');
require_once '../includes/config.inc.php';

function https_request($url) {
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $data = curl_exec($curl);
    if (curl_errno($curl)) {
        return 'ERROR ' . curl_error($curl);
    }
    curl_close($curl);
    return $data;
}

function getOpenidFromCode($code) {

    $url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid=' . AppId .
            '&secret=' . AppSecret . '&code=' . $code . '&grant_type=authorization_code';

    $access_token_json = https_request($url);

    //echo $access_token_json;

    $access_token_array = json_decode($access_token_json, true);
    // echo $access_token_array;
    $access_token = $access_token_array['access_token'];
    $openid = $access_token_array['openid'];

    return $openid;
}

function addPromo($dbc, $openid, $promo) {

    $q = "select * from WXUserPromo where code = '$promo'";
    $res = @mysqli_query($dbc, $q);

    $cnt = mysqli_affected_rows($dbc);
    if ($cnt >= 3)
        return $cnt;

    while ($row = mysqli_fetch_array($res, MYSQLI_ASSOC)) {
        $oid = $row['openid'];
        if ($oid == $openid)
            return 'repeat';
    }

    $q = "insert into WXUserPromo " .
            " (openid,    code,   amount, state, create_time, expire_day) values " .
            " ('$openid', '$promo', 100,  1,      now(),       1,) ";
    $log .= "$q\n";
    @mysqli_query($dbc, $q);
    if (mysqli_affected_rows($dbc) == 1)
        return 'ok';
    return 'fail';
}

$util = new CommonUtil($dbc);
$code = isset($_GET['code']) ? $_GET['code'] : '';
$openid = strlen($code) > 0 ? getOpenidFromCode($code) : '';
$promo = isset($_GET['promo']) ? $_GET['promo'] : '';

if (strlen($promo) > 0 && strlen($openid) > 0) {

    echo "<html><header><title>【畅通狮城公众号】新加坡手机充值 #新电信#星河#m1 三网全覆盖!</title>";
    echo '<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0">';
    echo "</header><body style=\"background-color: rgb(131,190,70);\">";

    echo '<img width="100%" src="http://www.chongzhi.sg/images/share/share2_3.jpg"></img>';

    $res = addPromo($dbc, $openid, $promo);
    if ($res == 'ok')
        echo "恭喜你，红包已经添加到账户，请关注畅通狮城查看！";
    else if ($res == 'fail')
        echo "抱歉，添加红包失败（未知错误）";
    else if ($res == 'repeat')
        echo "该红包已添加，请勿重复访问此链接";
    else
        echo "很抱歉，该红包领取人数已达上限 $res 人，下次手再快点吧！";

    echo "</body></html>";
}
else {
	echo "请使用微信访问。";
}

?>












