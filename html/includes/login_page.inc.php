<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$page_title = '登录';
include('includes/header.html');

if (isset($errors) && !empty($errors)) {
    echo '<h2>出错了!</h2>
    <p class="error">';
    foreach ($errors as $msg) {
        echo " - $msg<br/>\n";
    }
    echo '</p><p>请再试一次</p>';
}

?>

<h2>登录</h2>
<form action="login.php" method="post" onSubmit="javascript: this.submit.disabled=true">
    <table align="center">
        <tr>
            <td>Email</td>
            <td><input type="text" name="email" size="20" maxlength="60" /></td>
        </tr>
        <tr>
            <td>密码</td>
            <td><input type="password" name="pass" size="20" maxlength="20" /></td>
        </tr>
        <tr>
            <td></td>
            <td align="left"><input type="submit" name="submit" value="登录" /></td>
        </tr>
    </table>
</form>

<br><br>

<a href="forgot.php">忘记密码</a>

<?php include ('includes/footer.html'); ?>