<?php

// bought on taobao
// define("TOKEN", "longfan1011");
// define("AppId", "wx2becbdaff28afe1e");
// define("EncodingAESKey", "QwU2VK0CA9z7ZqgBLmVeO8sHXc7WoTpMDyNtcYKSKR4");

// test
define("TOKEN", "longfan1011");
define("AppId", "wx0b9a6c07910a346b");
define("EncodingAESKey", "QwU2VK0CA9z7ZqgBLmVeO8sHXc7WoTpMDyNtcYKSKR4");

require_once('../wxsdk/WXBizMsgCrypt.php');

$wechatObj = new wechatCallbackapiTest();
$code = "";

if (isset($_GET['echostr'])) {
    $wechatObj->valid();
}
else if (isset[$_GET['code']]) {
    $code = $_GET['code'];
    echo $code;
}
else {
    $wechatObj->responseMsg();
}

class wechatCallbackapiTest {

    public function valid() {
        $echoStr = $_GET["echostr"];

        if ($this->checkSignature()) {
            echo $echoStr;
            exit;
        }
    }

    private function checkSignature() {

        $echoStr = $_GET["echostr"];
        $signature = $_GET["signature"];
        $timestamp = $_GET["timestamp"];
        $nonce = $_GET["nonce"];
        $tmpArr = array(TOKEN, $timestamp, $nonce);
        sort($tmpArr);
        $tmpStr = implode($tmpArr);
        $tmpStr = sha1($tmpStr);

        if ($tmpStr == $signature) {
            return true;
        } else {
            return false;
        }
    }

    public function responseMsg() {

        $timestamp = $_GET['timestamp'];
        $nonce = $_GET['nonce'];
        $msg_signature = $_GET['msg_signature'];
        $encrypt_type = "raw";
        if (isset($_GET['encrypt_type'])) {
            $encrypt_type = $_GET['encrypt_type'] == 'aes' ? "aes" : "raw";
        }
        $this->logger('encrypt_type='.$encrypt_type);

        $postStr = $GLOBALS["HTTP_RAW_POST_DATA"];
        if (!empty($postStr)) {
            // decrypt it
            if ($encrypt_type == 'aes') {
                $pc = new WXBizMsgCrypt(TOKEN, EncodingAESKey, AppId);
                $this->logger(" D \r\n" . $postStr);
                $decryptMsg = "";
                $errCode = $pc->DecryptMsg($msg_signature, $timestamp,
                    $nonce, $postStr, $decryptMsg);
                $postStr = $decryptMsg;
            }

            $this->logger(" R \r\n" . $postStr);

            $postObj = simplexml_load_string($postStr, 
                'SimpleXMLElement', LIBXML_NOCDATA);
            
            $this->logger('postObj='.$postObj);

            $RX_TYPE = trim($postObj->MsgType);

            $this->logger('rx_type='.$RX_TYPE);

            // $result = '';

            $result = $this->receiveText($postObj);
            // switch($RX_TYPE) {
            //  case "event":
            //      $result = $this->receiveEvent($postObj);
            //      break;
            //  case "text":
            //      $result = $this->receiveText($postObj);
            //      break;
            // }

            $this->logger(" R \r\n" . $result);

            // encrypt it
            if ($encrypt_type == 'aes') {
                $encryptMsg = '';
                $errCode = $pc->encryptMsg($result, $timestamp, $nonce,
                    $encryptMsg);
                $result = $encryptMsg;
                $this->logger(" E \r\n" . $result);
            }

            echo $result;
        }
        else {
            echo "";
            exit;
        }
    }

    private function receiveEvent($object) {
        $content = "";
        switch ($object->Event) {
            case "subscribe":
                $content = "欢迎关注我们！";
                break;
        }
        $result = $this->transmitText($object, $content);
        return $result;
    }

    private function receiveText($object) {

        $keyword = trim($object->Content);

        $this->logger('keyword=' . $keyword);

        if (strstr($keyword, "text")) {
            $content = "this is text msg";
        }
        else if (strstr($keyword, "pic")) {
            $content = array();
            $content[] =
            array("Title"=>"single pic title",
                  "Description"=>"single pic content",
                  "PicUrl"=>"http://img1.imgtn.bdimg.com/it/u=1151512896,3470104745&fm=21&gp=0.jpg",
                  "Url"=>"http://chinapacers.com/");
            $content[] =
            array("Title"=>"single pic title 2",
                  "Description"=>"single pic content 2",
                  "PicUrl"=>"http://img1.imgtn.bdimg.com/it/u=1151512896,3470104745&fm=21&gp=0.jpg",
                  "Url"=>"http://chinapacers.com/");
        }

        $this->logger('content='.$content);

        if (is_array($content)) {
            if (isset($content[0])) {
                $result = $this->transmitNews($object, $content);
            }
        }
        else {
            $result = $this->transmitText($object, $content);
        }
        return $result;
    }

    private function transmitText($object, $content) {
        
        $this->logger('transmitText');

        $xmlTpl = "<xml>
                <ToUserName><![CDATA[%s]]></ToUserName>
                <FromUserName><![CDATA[%s]]></FromUserName>
                <CreateTime>%s</CreateTime>
                <MsgType><![CDATA[text]]></MsgType>
                <Content><![CDATA[%s]]></Content>
                </xml>";
        $result = sprintf($xmlTpl, $object->FromUserName,
            $object->ToUserName, time(), $content);
        $this->logger('result='.$result);
        return $result;
    }

    private function transmitNews($object, $newsArray) {
        if (!is_array($newsArray)) {
            return;
        }

        $itemTpl = "<item>
        <Title><![CDATA[%s]]</Title>
        <Description><![CDATA[%s]]></Description>
        <PicUrl><![CDATA[%s]]></PicUrl>
        <Url><![CDATA[%s]]></Url>
        </item>";
        $item_str = "";
        foreach ($newsArray as $item) {
            $item_str .=
            sprintf($itemTpl, $item['Title'],
                $item['Description'],
                $item['PicUrl'],
                $item['Url']);
        }
        $xmlTpl = "<xml>
                <ToUserName><![CDATA[%s]]></ToUserName>
                <FromUserName><![CDATA[%s]]></FromUserName>
                <CreateTime>%s</CreateTime>
                <MsgType><![CDATA[news]]></MsgType>
                <ArticleCount>%s</ArticleCount>
                <Articles>$item_str</Articles>
                </xml>";

        $result = sprintf($xmlTpl, 
            $object->FromUserName,
            $object->ToUserName, 
            time(), count($newsArray));
        return result;
    }

    public function logger($log_content) {
        if (isset($_SERVER['HTTP_APPNAME'])) {
            // SAE 205
            sae_set_display_errors(false);
            sae_debug($log_content);
            sae_set_display_errors(true);
        }
        else if ($_SERVER['REMOTE_ADDR'] != '127.0.0.1') {
            // LOCAL
            $max_size = 500000;
            $log_filename = "log.xml";
            if (file_exists($log_filename) and
                (abs(filesize($log_filename)) > $max_size)) {
                unlink($log_filename);
            }
            file_put_contents($log_filename,
                date('Y-m-d H:i:s').$log_content."\r\n",
                FILE_APPEND);
        }
    }
}


?>















