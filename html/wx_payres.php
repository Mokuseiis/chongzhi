<?php

ini_set('date.timezone', 'Asia/Shanghai');
error_reporting(E_ERROR);

require_once "CommonUtil.php";
require_once "wxpay/lib/WxPay.Api.php";
require_once 'wxpay/lib/WxPay.Notify.php';
require_once 'log.php';
require_once '../mysqli_connect.php';
require_once 'includes/config.inc.php';

//初始化日志
//$logHandler = new CLogFileHandler("logs/" . date('Y-m-d') . '.log');
//$log = Log::Init($logHandler, 15);

$email_admin = "longfan1011@outlook.com";

//$email_xy = "26959915@qq.com,xiaoyi0310@yahoo.com";
//$email_all = "longfan1011@outlook.com,26959915@qq.com,xiaoyi0310@yahoo.com";

class PayNotifyCallBack extends WxPayNotify {

    //查询订单
    private $dbc = null;
    private $tid = null;

    public function __construct($pdbc) {
        $this->dbc = $pdbc;
    }

    public function Queryorder($transaction_id) {
        $input = new WxPayOrderQuery();
        $input->SetTransaction_id($transaction_id);
        $result = WxPayApi::orderQuery($input);
//        Log::DEBUG("query:" . json_encode($result));

        if (array_key_exists("return_code", $result) && array_key_exists("result_code", $result) && $result["return_code"] == "SUCCESS" && $result["result_code"] == "SUCCESS") {
            return true;
        }
        return false;
    }

    private function mark_trans_ok($trans, $prod, $balance_admin, $npn_session, $resp) {

        $dbc = $this->dbc;

        $email_all = "longfan1011@outlook.com,26959915@qq.com,xiaoyi0310@yahoo.com";

        $tid = $trans['tid'];
        $uid = $trans['uid'];
        $openid = $trans['openid'];
        $mobile = $trans['mobile'];
        $upid_lst = $trans['upid_lst'];
        $menu_id = $prod['menu_id'];
        $total_fee = $prod['total_fee'];


        // update npn balance
        $old_admin_balance = 0;
        $diff = 0;
        $diffstr = '?';
        if ($balance_admin > 0) {
            $q = "select * from Settings where setting_name = 'npn_balance'";
            $r = @mysqli_query($dbc, $q);
            if (mysqli_affected_rows($dbc) == 1) {
                $row = mysqli_fetch_array($r, MYSQLI_ASSOC);
                $old_admin_balance = $row['setting_value'];
                if (strlen($old_admin_balance) > 0) {
                    $diff = floatval($old_admin_balance) - $balance_admin;
                    $diffstr = number_format($diff, 2);
                    $diff = intval($diff * 100);
                }

                $this->addLog('ok', "diff=$diff");
            }
        }

        // state = 0 started
        // state = 1 weixin called back
        // state = 2 npn requesting
        // state = 3 npn ok
        // state = 101 npn failed

        $mode = strlen(SG_DEMO_CARD) > 0 ? 1 : 2;

        $q = "update WXTrans set state=3, topup_date = now(), update_date = now(), npn_session = '$npn_session', " .
                "mode = $mode, npn_cost = $diff, topup_err = '$resp' where tid=$tid";
        $r = @mysqli_query($dbc, $q);

        // add a log
        $status = 'ok';
        if (mysqli_affected_rows($dbc) == 1) {
            $this->addLog('ok', "updated state to 3");
        } else {
            $status = 'failed';
            // add a new log
            $this->addLog('err', "updated state to 3");
            mail("longfan1011@outlook.com", 'Error', "Failed to update state to 3, tid=$tid", EMAIL_NO_REPLY);
        }


        $log = '';
        // update upid list, set all state to 1
        $used_promo = false;
        $log .= "upid_lst = $upid_lst\n";

        if ($upid_lst != null && strlen($upid_lst) > 0) {
            $used_promo = true;
        }


        if (strlen(SG_DEMO_CARD) < 1) {
            $q = "update Settings set setting_value = '$balance_admin' where setting_name = 'npn_balance'";
            @mysqli_query($dbc, $q);

            if ($balance_admin < 1000) {
                mail("longfan1011@outlook.com,26959915@qq.com", 'SgTopup 余额不足:$' . $balance_admin, '请尽快充值!!!', EMAIL_NO_REPLY);
            } else if ($balance_admin < 2500) {
                mail("26959915@qq.com", 'SgTopup 余额不足:$' . $balance_admin, '请尽快充值!!!', EMAIL_NO_REPLY);
            }
        }

        $price_s = number_format($trans['total_fee'] / 100.0, 2);
        $full_name = $prod['full_name'];



        // add refer hongbao
        if ($used_promo == false) {

            $log .= "promo = false\n";

            // check if user has referer
            $q = "select * from WXRefer where openid = '$openid'";
            $res = @mysqli_query($dbc, $q);
            if (mysqli_affected_rows($dbc) > 0) {
                $row = mysqli_fetch_array($res, MYSQLI_ASSOC);
                $refid = $row['refid'];

                // calculate amount of hongbao
                $q = "select * from WXTrans where openid = '$openid' and state=3";
                @mysqli_query($dbc, $q);
                $amount = mysqli_affected_rows($dbc) > 1 ? 50 : 600;

                // get referer's uid
                $util = new CommonUtil($dbc);
                $ref_user = $util->getUser($refid);
                $ref_uid = $ref_user ? $ref_user['uid'] : -1;

                // add hongbao to it's referer
                $q = "insert into WXUserPromo " .
                        " (uid,      openid,    code,   amount, state, create_time, expire_day, from_tid,  mobile) values " .
                        " ($ref_uid, '$refid', 'refer', $amount,  1,     now(),       30,         $tid,    '$mobile') ";
                $log .= "$q\n";
                @mysqli_query($dbc, $q);

                // add hongbao to self
                $hb_value = number_format($total_fee * 0.01, 0);
                $q = "insert into WXUserPromo " .
                        " (uid,      openid,    code,    amount,     state, create_time, expire_day, from_tid, mobile) values " .
                        " ($uid,     '$openid', 'refer', $hb_value,  1,     now(),       30,         $tid,    '$mobile') ";
                $log .= "$q\n";
                // @mysqli_query($dbc, $q);
            }
        }

        $body = " menu_id = $menu_id \n prod = $full_name \n price = RMB $price_s \n" .
                " mobile = $mobile \n" .
                " status = $status \n user = $uid \n" .
                " admin cost = SGD $diffstr \n admin balance = SGD $balance_admin \n discount = $upid_lst\n" .
                " http://www.chongzhi.sg/wx_trans.php \n" .
                " http://www.chongzhi.sg/wx_updateprod.php \n" .
                " log --- \n" . $log;




        if ($used_promo) {

            $lst1 = preg_split("/,/", $upid_lst);

            $q2 = "update WXUserPromo set tid = $tid, state=2, mobile = $mobile where";
            $cnt = 0;
            foreach ($lst1 as $upid) {
                if ($upid != null && strlen($upid) > 0) {
                    $q2 .= $cnt == 0 ? " upid = $upid" : " or upid = $upid";
                    $cnt++;
                }
            }

            if ($cnt > 0) {
                @mysqli_query($dbc, $q2);
            }
        }

        return $balance_admin;
    }

    private function addLog($level, $msg) {
        $dbc = $this->dbc;
        $tid = $this->tid;

        if ($tid == null) {
            $tid = -1;
        }

        // add a new log
        $q = "insert into WXLog (kind, id1, level, detail) values ('notify', '$tid', '$level', '$msg')";
        @mysqli_query($dbc, $q);
    }

    private function topup_main($trans, $time0) {

        $dbc = $this->dbc;

        $tid = $trans['tid'];
        $uid = $trans['uid'];
        $this->tid = $tid;
        $mobile = $trans['mobile'];
        $pid = $trans['pid'];

        $q = "select * from WXProducts where pid = '$pid'";
        $reslst = @mysqli_query($dbc, $q);
        $prod = flase;
        if (mysqli_affected_rows($dbc) == 1) {
            $prod = mysqli_fetch_array($reslst, MYSQLI_ASSOC);
        }

        if ($prod === false) {
            $this->addLog('err', "fail get pid=$pid");
            return;
        }

        $menu_id = $prod['menu_id'];

        $q = "update WXTrans set state=2 where tid = $tid";
        @mysqli_query($dbc, $q);

        $time1 = new DateTime('now');
        $time1 = $time1->getTimestamp();

        list($resp, $out, $url) = $this->call_npn_api($tid, $menu_id, $mobile);

        $time2 = new DateTime('now');
        $time2 = $time2->getTimestamp();

        $result = count($out) >= 1 ? $out[0] : 'ok';
        $npn_session = count($out) >= 3 ? $out[2] : null;
        $balance = count($out) >= 4 ? $out[3] : 0;

        if (count($out) < 1) {
			mail("longfan1011@outlook.com,26959915@qq.com", "微信订单，NPN返回空值！$mobile tid=$tid", '请与上游核实', EMAIL_NO_REPLY);
        }

        if ($result == 'ERROR') {
            $q = "update WXTrans set topup_err='$resp', state=102, topup_date=now(), update_date=now() where tid=$tid";
            @mysqli_query($dbc, $q);

            $body = "tid = $tid \n uid = $uid \n menu_id = $menu_id \n" .
                    " mobile = $mobile \n resp = $resp";
            mail("longfan1011@outlook.com,26959915@qq.com", "微信充值失败! $mobile tid=$tid", $body, EMAIL_NO_REPLY);
        } else {
        	$timestr = ($time1 - $time0) . "+" . ($time2 - $time1);
            $this->mark_trans_ok($trans, $prod, $balance, $npn_session, $resp);
        }

        //Success:
        //Result|TAG|SESSION          |BALANCE      |Card-SerialNo|Card-Pin|Card-Passwd|Card-Expi redTime
        //OK    |5  |V1424744833345235|0.00         ||||1
    }

    private function call_npn_api($tid, $menu_id, $mobile) {

        if (strlen(SG_DEMO_CARD) > 0) {
            $menu_id = '461';
        }

        $url = NPN_URL .
                "CMD=RECHARGE" .
                "&TAG=" . $tid .
                "&MENU=" . $menu_id .
                "&ACTION=13810" .
                "&PARAM=" . $mobile .
                "&USER=" . NPN_USER .
                "&PASSWD=" . NPN_PASS;
//    $url = NPN_URL;
        $this->addLog('ok', $url);
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
//    curl_setopt($ch, CURLOPT_HEADER, array(
//        "CMD: RECHARGE",
//        "TAG: $tid",
//        "MENU: $menu_id",
//        "ACTION: 13810",
//        "PARAM: $mob" .
//        "USER: " . NPN_USER,
//        "PASSWD: " . NPN_PASS));

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); // do not print out!
        $resp = trim(curl_exec($ch));
        curl_close($ch);

        //Success:Result|TAG|SESSION|BALANCE|Card-SerialNo|Card-Pin|Card-Passwd|Card-Expi redTime
        //OK|5|V1424744833345235|0.00||||1
        $this->addLog('ok', "resp=$resp");
        $out = array();
        $tok = strtok($resp, '|');

        while ($tok !== false) {
            array_push($out, trim($tok));
            $tok = strtok('|');
        }

        return array($resp, $out, $url);
    }

    //重写回调处理函数
    public function NotifyProcess($data, &$msg) {

        $ts_now = new DateTime('now');
        $ts_now = $ts_now->getTimestamp();

        $attach = $data['attach'];
        $tid = $attach;
        $this->tid = $tid + 0;

        $dbc = $this->dbc;

        $q = "select * from WXTrans where tid = $tid";
        $results = @mysqli_query($dbc, $q);
        $trans = false;
        if (mysqli_affected_rows($dbc) == 1) {
            $trans = mysqli_fetch_array($results, MYSQLI_ASSOC);
        }


        if ($trans === false) {
            $this->addLog('err', "failed to get tid=$tid");
            return true;
        }

        $create_date = $trans['create_date'];
        $create_date = strtotime($create_date);


        $time_diff = $ts_now - $create_date;

        if ($time_diff > 60 * 60) {
            $this->addLog('err', "order expired..." + $create_date);
            return true;
        }

        $state = $trans['state'];
        if ($state != 0) {
            $this->addLog('ok', "notify again? state=$state");
            return true;
        } else {
            $this->addLog('ok', "start");
        }

        //Log::DEBUG("call back:" . json_encode($data));

        $notfiyOutput = array();

        if (!array_key_exists("transaction_id", $data)) {
            $msg = "输入参数不正确";
            return false;
        }
        //查询订单，判断订单真实性
        if (!$this->Queryorder($data["transaction_id"])) {
            $msg = "订单查询失败";
            return false;
        }

        $transaction_id = $data['transaction_id'];
        $out_trade_no = $data['out_trade_no'];
        $total_fee = $data['total_fee'];
        // $cash_fee = $data['cash_fee'];        

        $q = "update WXTrans set transaction_id = '$transaction_id', out_trade_no = '$out_trade_no', " .
                "total_fee = $total_fee, state=1, update_date = now() where tid = $tid";
        $res = @mysqli_query($dbc, $q);

        if (mysqli_affected_rows($dbc) < 1) {
            $msg = "更新Trans失败";
            return false;
        }

        $trans['transaction_id'] = $transaction_id;
        $trans['out_trade_no'] = $out_trade_no;
        $trans['total_fee'] = $total_fee;
        $trans['state'] = 1;

        $prod_fee = $trans['prod_fee'];
        if ($prod_fee != $total_fee) {
            $this->addLog('err', "tid=$tid, prod_fee=$prod_fee, total_fee=$total_fee");
            return true;
        }

        $this->topup_main($trans, $create_date);

        return true;
    }

}

//Log::DEBUG("begin notify");
$notify = new PayNotifyCallBack($dbc);
$notify->Handle(false);










