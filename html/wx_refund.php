<?php


require_once("CommonUtil.php");
require_once("wxpay/WxPayHelper.php");
require_once("wxpay/lib/WxPay.Api.php");
require_once("wxpay/lib/WxPay.Data.php");
require_once("wxpay/lib/WxPay.Exception.php");
require_once('../wxsdk/WXBizMsgCrypt.php');
require_once('../mysqli_connect.php');

function get_by_curl($url, $post = false) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    if ($post) {
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    }
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}

function https_request($url) {
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $data = curl_exec($curl);
    if (curl_errno($curl)) {
        return 'ERROR ' . curl_error($curl);
    }
    curl_close($curl);
    return $data;
}

function getOpenidFromCode($code) {

    $url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid=' . AppId .
            '&secret=' . AppSecret . '&code=' . $code . '&grant_type=authorization_code';

    $access_token_json = https_request($url);

    $access_token_array = json_decode($access_token_json, true);
    // echo $access_token_array;
    $access_token = $access_token_array['access_token'];
    $openid = $access_token_array['openid'];

    return $openid;
}

function writeInfo($str) {
    if (strlen($str) < 1) {
        return;
    }
    echo '<div style="background-color: rgb(255, 255, 255);">';
    echo '<div class="middle-box">';
    echo '  <div class="middle-box" style="padding-bottom: 0px;">';
    echo "    <p class=\"general-prompt\">$str</p>";
    echo '  </div>';
    echo '</div></div>';
}

function writeHtml1($dbc, $util, $user) {

    $openid = $user['openid'];
    $map = getPidFullNameMap($dbc, $util);

    echo '<div style="background-color: rgb(255, 255, 255);">';
    echo '<div class="middle-box">';

    echo '  <div class="middle-box" style="padding-bottom: 0px;">';
    echo "    <p class=\"general-prompt\">请选择【充值失败】的订单进行退款, 如有疑问请查看";
    echo "        <a href=\"http://www.chongzhi.sg/wx_refundp.php\">查看退款政策</a>";
    echo "        , 或联系客服微信 vicki_xiao<p>";
    echo '  </div>';


    $q = "select * from WXTrans where openid = '$openid' and state > 0 order by tid desc limit 10";
    //mail('longfan1011@outlook.com', 'My trans list', $q, 'From: no-reply@chongzhi.sg');

    $res = @mysqli_query($dbc, $q);

    echo '<form name="rform" action="http://www.chongzhi.sg/wx_refund.php" class="refund-form" method="post">';
    echo '  <input type="hidden" id="pickedtid" class="submit-refund-tid" name="tid" value="">';
    echo "  <input type=\"hidden\" name=\"openid\" value=\"$openid\">";

    echo '  <table width="90%" cellspacing="20" cellpadding="20" align="center">';
    $row_cnt = mysqli_affected_rows($dbc);
    if ($row_cnt > 0) {
        $item = '';
        while ($row = mysqli_fetch_array($res)) {

            $item = "<br>日期: " . $row['create_date'];
            $item .= "<br>手机: " . $row['mobile'];
            $item .= "<br>产品: " . $map[$row['pid']];
            $paid = $row['total_fee'];
            $item .= "<br>支付: ￥" . number_format($paid / 100.0, 2);
            $item .= "<br>状态: " . $util->getStateStr($row['state']) . '<br>';
            $state = $row['state'] + 0;
            $tid = $row['tid'];

            echo "<tr><td>$item</td><td>";
            if ($state == 200) {
                echo "退款成功";
                //mail('longfan1011@outlook.com', '退款成功My trans list', $q, 'From: no-reply@chongzhi.sg');
            } else if ($state == 102) {
                echo "<input type=\"button\" class=\"btn btn-primary btn-block btn-lg\" value='退款' onclick=\"pick_trans('$tid');\"></button>";
            } else if ($state == 101) {
				echo "疑似失败，核实中";
            } else {
                echo $util->getStateStr($state);
            }
            echo '</td></tr>';
        }
    }
    echo '  </table></form>';

    echo '</div></div>';

    if ($row_cnt < 1) {
        echo "<p>无任何订单</p>";
    }

    echo '<script type="text/javascript">';

    echo 'function pick_trans(tid) {';
    echo "  var ip = document.getElementById(\"pickedtid\"); ";
    echo "  ip.value = tid; ";
    echo "  document.rform.submit(); ";
    echo '} ';
    echo '</script> ';
}

function getPidFullNameMap($dbc, $util) {

    $q = 'select pid, full_name from WXProducts';
    $res = @mysqli_query($dbc, $q);

    $map;

    if (mysqli_affected_rows($dbc) > 0) {
        while ($row = mysqli_fetch_array($res, MYSQLI_ASSOC)) {
            $map[$row['pid']] = $row['full_name'];
        }
    }

    return $map;
}

function printf_info($data) {
    foreach ($data as $key => $value) {
        echo "<font color='#f00;'>$key</font> : $value <br/>";
    }
}

function writeFooter() {
    echo '</body></html>';
}

$util = new CommonUtil($dbc);
$util->writeHeader("/images/ctsc256.jpg", "申请退款");

if (isset($_POST['tid']) && isset($_POST['openid'])) {

    // echo $_POST['tid'];
    $tid = $_POST['tid'] + 0;
    $openid = $_POST['openid'];

    $q = "select * from WXTrans where tid = $tid";
    $res = @mysqli_query($dbc, $q);
    $trans = flase;
    if (mysqli_affected_rows($dbc) == 1) {
        $trans = mysqli_fetch_array($res, MYSQLI_ASSOC);
    }
    if ($trans == false) {
        writeFooder();
        exit();
    }

    $state = $trans['state'];
    if ($state != 102 || $trans['openid'] != $openid) {
        writeFooder();
        exit();
    }

    $transaction_id = $trans['transaction_id'];
    $out_trade_no = $trans["out_trade_no"];
    $total_fee = $trans["total_fee"];
    $refund_fee = $total_fee;

    $input = new WxPayRefund();
    $input->SetOut_trade_no($out_trade_no);
    $input->SetTotal_fee($total_fee);
    $input->SetRefund_fee($refund_fee);
    $input->SetOut_refund_no(WxPayConfig::MCHID . date("YmdHis"));
    $input->SetOp_user_id(WxPayConfig::MCHID);

    $info = WxPayApi::refund($input);

    $refund_id = $info['refund_id'];
    $out_refund_no = $info['out_refund_no'];
    $refund_fee = $info['refund_fee'];
    $return_msg = $info['return_msg'];
    $return_code = $info['return_code'];
    $result_code = $info['result_code'];
    $err_code_des = $info['err_code_des'];

    if (strtolower($result_code) == 'success') {
        $q = "insert into WXRefund (tid, transaction_id, refund_id, refund_fee, return_code, return_msg, out_refund_no, result_code) values " .
                "($tid, '$transaction_id', '$refund_id', $refund_fee, '$return_code', '$return_msg', '$out_refund_no', '$result_code' ) ";
        $res = @mysqli_query($dbc, $q);

        $q = "update WXTrans set refund_id = '$refund_id', refund_fee = $refund_fee, state = 200 where tid = $tid";
        $res = @mysqli_query($dbc, $q);
        writeInfo("退款成功!");
        mail('longfan1011@outlook.com', '退款成功', $q, 'From: no-reply@chongzhi.sg');
    }
    else {
        writeInfo("退款失败! " . $err_code_des . '<br>如有疑问请联系管理员微信 vicki_xiao');
        mail('longfan1011@outlook.com', '退款失败', $q, 'From: no-reply@chongzhi.sg');
    }
    
    writeFooder();
    // printf_info($info);
}
else if (isset($_GET['code'])) {
    $code = $_GET['code'];
    $util->addLog('refund', 'ok', -1, 'init');

    $openid = getOpenidFromCode($code);

    $user = $util->getCreateDbUser($openid);

//    $level = intval($user['level']);
//    if ($level < 1) {
//        writeFooder();
//    }

    writeHtml1($dbc, $util, $user);
    writeFooder();
}
else {
    writeFooter();
}


?>















