<?php

require_once("CommonUtil.php");

$util = new CommonUtil();

$util->writeHeader("/images/ctsc256.jpg", "畅通狮城 红包政策");

echo '<div style="color:#eeeeee; padding:8px; ">';

echo '红包政策<br>';
echo '红包政策是畅通狮城用于回馈消费者购买时享受的福利政策<br><br>';

echo '关注送红包<br>';
echo '第一次关注成功后，在畅通狮城【公众号】内回复subs 领取3元关注红包，关注红包的有效期是1天<br><br>';

echo '分享送红包<br>';
echo '分享你的专属链接给小伙伴，小伙伴第一次成功充值后，你将获得1元朋友红包*，此小伙伴以后每次成功充值，你都会获得0.5元朋友红包*，小伙伴越多，朋友红包越多，朋友红包的有效期是30天<br><br>';

echo '红包使用说明<br>';
echo '1. 红包仅限在畅通狮城公众号使用<br>';
echo '2. 红包可累积、可直接抵扣订单金额<br>';
echo '3. 红包不找零、不兑现 <br>';
echo '4. 使用红包支付的订单发生退款时，将不返还红包<br>';
echo '5. 订单取消时，红包自动返还<br>';
echo '6. 红包过期作废，请务必在有效期内使用<br>';
echo '*红包面额如有变化，以畅通狮城公众号公布的面额为准 <br><br><br>';


echo '</div>';

echo '</body></html>';


?>