<?php

require_once('../mysqli_connect.php');


// $q = "select * from WXTrans where uid is null";
// $res = @mysqli_query($dbc, $q);
// while ($row = mysqli_fetch_array($res, MYSQLI_ASSOC)) {
// 	$openid = $row['openid'];
// 	$tid = $row['tid'];
// 	if ($openid == null)
// 		continue;
// 	$q2 = "select * from WXUsers where openid = '$openid'";
// 	$res2 = @mysqli_query($dbc, $q2);
// 	while ($row2 = mysqli_fetch_array($res2, MYSQLI_ASSOC)) {
// 		$uid = $row2['uid'];
// 		$q3 = "update WXTrans set uid = $uid where openid = '$openid' and tid = $tid";
// 		echo $q3 . '<br>';
// 		@mysqli_query($dbc, $q3);
// 	}
// }

//require_once 'includes/config.inc.php';
// mail('26959915@qq.com,xiaoyi0310@yahoo.com,xiaoyi0310@gmail.com,longfan1011@outlook.com', 'test', 'test sending from server', 'From: no-reply@chongzhi.sg');

function getHbMap($dbc) {

	$q = "select * from WXUserPromo";
	$res = @mysqli_query($dbc, $q);
	$map;
    while ($row = mysqli_fetch_array($res, MYSQLI_ASSOC)) {
    	$key = '' . $row['upid'];
    	$value = $row['amount'];
//    	echo "$key, $value<br>";
    	$map[$key] = $value;
    }
    return $map;
}


function show($dbc, $from, $to, $map_hb) {
    $map_cost;
    $map_prod;
    $map_user;

	$hb = 0;
    $cnt_refund = 0;
    $cnt_ok = 0;
    $cnt_fail = 0;
    $money_xy = 0; //15
    $money_lf = 0; //13, 16, 27
    $rate = 4.92;
    $profit = 0.0;
    $cost = 0.0;
    $revenue = 0.0;
    $cost_npn = 0.0;

    $q = "select * from WXTrans where create_date >= '$from' and create_date < '$to' and tid > 530";
    $res = @mysqli_query($dbc, $q);
    while ($row = mysqli_fetch_array($res, MYSQLI_ASSOC)) {
    	// echo $row['create_date'] . ', ';
        $pid = $row['pid'];
        $total_fee = $row['total_fee'];
        if ($total_fee <= 1)
            continue;
        $npn_cost = $row['npn_cost'];
        $rmb_cost = $rate * $npn_cost;
        $test = $total_fee / $rmb_cost;
        if ($test > 0.9 && $test < 1.3) {
            // valid cost
            $map_cost[$pid] = $rmb_cost;
        }
        // else {
        // 	echo "<br>check $total_fee, $rmb_cost ..";
        // }
    }

    foreach ($map_cost as $key => $value) {
        // echo $key . ": " . number_format($value/100, 2) . "<br>";
    }

    $res = @mysqli_query($dbc, $q);

    $hb_total = 0;
    $refund_ttl = 0;

    while ($row = mysqli_fetch_array($res, MYSQLI_ASSOC)) {
        $uid = $row['uid'];
        $pid = $row['pid'];
        $state = $row['state'];
        $refund_id = $row['refund_id'];
        $npn_cost = $row['npn_cost'];
        $total_fee = $row['total_fee'];
        $upid = $row['upid'];
        $upid_lst = $row['upid_lst'];

		if ($uid == null) {
			continue;
		}
        else if ($uid == 15) {
        	if ($state == 3)
	            $money_xy += $total_fee;
            continue;
        }
        else if ($uid == 13 || $uid == 16 || $uid == 27) {
        	if ($state == 3)
	            $money_lf += $total_fee;
            continue;
        }
        else {
        	$map_user[$uid] = 1;
        }


        if ($state == 200 || strlen($refund_id) > 0) {
            $cnt_refund++;
            continue;
        }
        if ($state == 101) {
            $cnt_fail++;
            continue;
        }        
        if ($state != 3) {
        	continue;
        }
        $cnt_ok++;

        $prod_cnt = $map_prod[$pid];
        if ($prod_cnt == null)
        	$prod_cnt = 1;
        else 
        	$prod_cnt++;
        $map_prod[$pid] = $prod_cnt;

        if ($total_fee <= 1)
            continue;

        if ($upid > 0) {
        	$hb++;
        }
        else if (strlen($upid_lst) > 0) {
        	$htlst = explode(",", $upid_lst);
        	foreach($htlst as $tmp) {        		
        		$hb_total += $map_hb[$tmp];
        	}
        	$tmp = count($htlst);
        	// echo ">>$upid_lst $tmp ";
        	$hb += $tmp;
        }

		// npn cost is tricky
		// 1. we honor each transaction to get the cost
		// 2. we find in map
		// 3. we guess

        $rmb_cost = $npn_cost * $rate;
        $test = $total_fee / $rmb_cost;
        // echo "<br>### $total_fee $rmb_cost ";
        if ($test < 0.9 || $test > 1.3) {
        	// echo "<br>$pid ";
        	// echo "2) t=$test ($total_fee;$rmb_cost)";
            $rmb_cost = $map_cost[$pid];            
            $test = $total_fee / $rmb_cost;
            // echo "<br> 2.1) t=$test ($total_fee;$rmb_cost)";
            if ($test < 0.9 || $test > 1.3) {
            	// echo "<br> 3) t=$test ($total_fee;$rmb_cost)";
            	$rmb_cost = $total_fee * (1 - 0.06);
            	$test = $total_fee / $rmb_cost;
            	// echo "<br> 3.1) t=$test ($total_fee;$rmb_cost)";
            }
        }

		$cost_npn += $rmb_cost;
        $cost1 = $rmb_cost + $total_fee * 0.006;

        $cost += $cost1;
        $revenue += $total_fee;
        // $pro1 = $revenue1 - $cost1;
        // echo $test . " " . number_format($total_fee/100, 2) . " - " . number_format($rmb_cost/100, 2) . " = " . number_format($pro/100, 2) . "<br>";
    }

	$cost_npn /= 100.0;
    $money_lf /= 100.0;
    $money_xy /= 100.0;
    $revenue /= 100.0;
    $cost /= 100.0;
    $profit = $revenue - $cost;

    echo '<tr>  <td>' . $from . '</td>';
    echo '<td>' . number_format($revenue, 2) . '</td>';
    echo '<td>' . number_format($cost_npn, 2) . '</td>';
    echo '<td>' . number_format($profit, 2) . '</td>';
    echo '<td>' . number_format($money_xy, 2) . '</td>';
    echo '<td>' . number_format($money_lf, 2) . '</td>';
    echo '<td>' . count($map_user) . '</td>';
    echo '<td>' . $cnt_ok . '</td>';
    //echo '<td>' . $hb . '</td>  </tr>' . "\n";
    echo '<td>(' . $hb . ') ' . number_format($hb_total/100.0, 2) . '</td>  </tr>' . "\n";

	$prod_str = '<table border="1" style="text-align: left">';
	$keys = array_keys($map_prod);
	sort($keys);
	$total_order = 0;
	foreach ($keys as $key) {
		$value = $map_prod[$key];
		$total_order += 0 + $value;
		$prod_str .= "<tr> <td>$key</td> <td>$value</td> </tr>";
	}
	$prod_str .= "<tr> <td>TOTAL</td> <td>$total_order</td> </tr>";
	$prod_str .= '</table>';

	return $prod_str;

    // echo "from $from to $to (exclusive)<br>";
    // echo "xy cost=$money_xy, lf cost=$money_lf<br>";
    // echo "successful=$cnt_ok, failed=$cnt_fail, refunded=$cnt_refund<br>";
    // echo "npn cost = " . number_format($cost_npn, 2) . "<br>";
    // echo "revenue = " . number_format($revenue, 2) . " cost = " . number_format($cost, 2) . "<br>";
    // echo "profit = " . number_format($profit, 2) . "<br>";
    // echo "margin = " . number_format($profit * 100 / $cost, 4) . "%";
    // echo "<br>------------------------<br>";
}

echo '<table border="1" style="text-align: right">';
echo "<tr>  <td>month</td>  <td>revenue</td>  <td>npnCost</td>  <td>profit</td>  <td>XY cost</td>  <td>LF cost</td>  <td>activeUser</td>  <td>order</td>   <td>HB</td>  <tr>";

$map = getHbMap($dbc);
show($dbc, '20160601', '20160701', $map);
show($dbc, '20160701', '20160801', $map);
show($dbc, '20160801', '20160901', $map);
show($dbc, '20160901', '20161001', $map);
show($dbc, '20161001', '20161101', $map);
show($dbc, '20161101', '20161201', $map);
show($dbc, '20161201', '20170101', $map);
show($dbc, '20170101', '20170201', $map);
show($dbc, '20170201', '20170301', $map);
show($dbc, '20170301', '20170401', $map);
$prod4 = show($dbc, '20170401', '20170501', $map);
$prod5 = show($dbc, '20170501', '20170601', $map);
$prod6 = show($dbc, '20170601', '20170701', $map);
$prod7 = show($dbc, '20170701', '20170801', $map);


echo "</table>";
echo "activeUser = users purchased at least once for the month<br><br>";


echo "prod count for 2017.07<br>";
echo $prod7;
echo "prod count for 2017.06<br>";
echo $prod6;
echo "prod count for 2017.05<br>";
echo $prod5;
echo "prod count for 2017.04<br>";
echo $prod4;

exit();
?>









