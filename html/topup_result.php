<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

session_start(); // access existing sesison
require('../mysqli_connect.php');

if (!isset($_SESSION['user_id'])) {
    require('includes/login_functions.inc.php');
    header("Location: login.php");
    exit();
//    redirect_user('login.php');
}

$page_title = '充值结果';
include('includes/header.html');
require('includes/login_functions.inc.php');

if (!isset($_SESSION['tid']) || !isset($_SESSION['email'])) {
    header('Location:topup.php');
    exit();
}
$tid = trim($_SESSION['tid']);
$email = trim($_SESSION['email']);

require('includes/config.inc.php');

$q = "select * from Transactions where tid=$tid";
//echo $q;
$r = @mysqli_query($dbc, $q);
if (mysqli_affected_rows($dbc) == 1) {
    $row = mysqli_fetch_array($r);
    $mobile = $row['mobile'];
    $price = $row['price_rmb'];
    $email_t = trim($row['email']);
    if ($email_t != $email) {
        header('Location:topup.php');
        exit();
    }
    
    $balance = get_balance($dbc, $email);
    $balance = number_format($balance, 2, '.', '');
    echo '<h3>充值成功！</h3>';
    echo '<table align="center" cellspacing="10">';
    echo "<tr><td>手机号</td><td>$mobile</td></tr>";
    echo "<tr><td>费用</td><td>¥$price</td></tr>";
    echo "<tr><td>余额</td><td>¥$balance</td></tr>";
    
    
    $level = safe_value($dbc, $_SESSION, 'user_level', 0);    
    if ($level >= 2) {
        $balance_admin = safe_value($dbc, $_SESSION, 'balance_admin', '??');
        echo "<tr><td>NPN余额</td><td>SGD $balance_admin</td></tr>";
    }
    
    echo '</table>';
} else {
    header('Location:topup.php');
    exit();
}


include('includes/footer.html');
