<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$page_title = '找回密码';
include('includes/header.html');

$display_form = true;

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    require('../mysqli_connect.php');
    require('includes/config.inc.php');
    require('includes/login_functions.inc.php');

    $ue = strtolower(safe_value($dbc, $_POST, 'email'));

    $e = FALSE;

    if (filter_var($ue, FILTER_VALIDATE_EMAIL)) {
        $e = $ue;
    } else {
        echo '<p class="error">请输入有效Email</p>';
    }

    if ($e) {
        $q = "select email from Users where email = '$e'";
        $r = @mysqli_query($dbc, $q);

        $exist = false;
        if (mysqli_affected_rows($dbc) == 1) {
            $exist = true;
            $q = "delete from UserPass where email = '$e'";
            @mysqli_query($dbc, $q);

            $pass = substr(md5(uniqid(rand(), true)), 3, 20);
            $q = "insert into UserPass (email, email_hash, pass_sys, pass_sys_date) values ('$e', SHA1('$e'), SHA1('$pass'), NOW())";
            $r = @mysqli_query($dbc, $q);
            if (mysqli_affected_rows($dbc) == 1) {
                // send email
                $body = "你好,\n我们收到了你的找回密码请求，如果这是你*本人*的操作，请点击以下链接来重置密码: \n\n" .
                        BASE_URL .
                        'reset_forgot.php?x=' . $pass .
                        "&y=$e" .
                        " \n\n如果你并没有申请找回密码，请忽略！";
                $display_form = false;
                mail($e, '重置密码', $body, EMAIL_NO_REPLY);
            }
        }

        echo "重置链接即将发送，请查收你的邮箱 $e (及垃圾邮件)，以重置密码";
        if ($exist) {
            echo '。';
        }
    }

    mysqli_close($dbc);
}
?>

<h3>找回密码</h3>

<form action="forgot.php" method="post">
    <table align="center">
        <tr>
            <td align="right">Email</td>
            <td align="left"><input type="text" name="email" size="20" maxlength="50" 
                                    value="" required/></td>
        </tr>
        <tr>
            <td></td>
            <td align="left"><input type="submit" name="submit" value="提交" /></td>
        </tr>
    </table>
</form>

<?php include('includes/footer.html'); ?>




