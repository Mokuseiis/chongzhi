<?php

require_once("CommonUtil.php");
require_once('../wxsdk/WXBizMsgCrypt.php');
require_once('../mysqli_connect.php');
require_once('includes/config.inc.php');

// require_once 'includes/config.inc.php';

function get_by_curl($url, $post = false) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    if ($post) {
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    }
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}

function https_request($url) {
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $data = curl_exec($curl);
    if (curl_errno($curl)) {
        return 'ERROR ' . curl_error($curl);
    }
    curl_close($curl);
    return $data;
}

function getOpenidFromCode($code) {

    $url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid=' . AppId .
            '&secret=' . AppSecret . '&code=' . $code . '&grant_type=authorization_code';

    $access_token_json = https_request($url);

    //echo $access_token_json;

    $access_token_array = json_decode($access_token_json, true);
    // echo $access_token_array;
    $access_token = $access_token_array['access_token'];
    $openid = $access_token_array['openid'];

    return $openid;
}

function addRefer($dbc, $util, $refid, $openid, $src) {

	if ($dbc == null || $refid == null || $openid == null || strlen($refid) < 1 || strlen($openid) < 1) {
		return false;
	}

	$ref_user = $util->getUser($refid);
	$ref_uid = $ref_user == false ? -1 : $ref_user['uid'];

	$user = $util->getUser($openid);
	$uid = $user == false ? -1 : $user['uid'];

	if ($refid == $openid) {
		//mail(EMAIL_ADMIN, "share3 $ref_uid, $uid (self)", "mark", EMAIL_NO_REPLY);
		return false;
	}

    $q = "select * from WXRefer where openid = '$openid'";
    @mysqli_query($dbc, $q);
    if (mysqli_affected_rows($dbc) > 0) {
    	//mail(EMAIL_ADMIN, "share3 $ref_uid, $uid user had other owner", "mark", EMAIL_NO_REPLY);
    	return false;
        // do nothing, user already have a referer
    } 
    
    $q = "select * from WXRefer where refid = '$openid' and openid = '$refid' ";
	@mysqli_query($dbc, $q);
	if (mysqli_affected_rows($dbc) > 0) {
		//mail(EMAIL_ADMIN, "share3 $ref_uid, $uid mutual refer not allowed", "mark", EMAIL_NO_REPLY);
		// do nothing, do not support mutual refering
		return false;
	}

	
	$q = "insert into WXRefer " . 
	" (refid, openid, create_time, state, ref_uid, uid) values " .
    " ('$refid', '$openid', now(), 1, $ref_uid, $uid) ";
	@mysqli_query($dbc, $q);

	$q = "insert into WXStats (kind, tag1, uid1, uid2, openid2, create_time) values ('share3', '$src', $ref_uid, $uid, '$openid', now() )";
	@mysqli_query($dbc, $q);

	//mail(EMAIL_ADMIN, "ReferOk $ref_uid, $uid", "", EMAIL_NO_REPLY);

	return true;
}

$util = new CommonUtil($dbc);

if (isset($_GET['code']) || isset($_GET['nba'])) {

    // $util->writeHeader2("/images/ctsc256.jpg", "【畅通狮城】公众号预付卡微信充值 #新电信 #m1 #星河 三网全覆盖!", "畅通狮城");

    $refid = $_GET['refid'];
    $code = $_GET['code'];
    $openid = getOpenidFromCode($code);

	$src = isset($_GET['src']) ? $_GET['src'] : 'unknown';
    addRefer($dbc, $util, $refid, $openid, $src);

    echo "<html><header><title>畅通狮城</title></header><body>";

    echo '<img width="100%" src="http://www.chongzhi.sg/images/share/share3.jpg"></img>';

    echo "</html>";

    // $util->writeFooter();
}
?>












