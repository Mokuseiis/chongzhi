<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

session_start(); // access existing sesison
require('../mysqli_connect.php');
require('includes/login_functions.inc.php');    

if (!isset($_SESSION['user_id']) || !isset($_SESSION['email'])) {
//    require('includes/login_functions.inc.php');
    header("Location: login.php");
    exit();
//    redirect_user('login.php');
}

$page_title = '个人中心';
include('includes/header.html');
//require('includes/login_functions.inc.php');

$email = trim($_SESSION['email']);
$level = $_SESSION['user_level'];

echo '<br>';
echo '<a href="history.php">充值记录</a> | ';
echo '<a href="reset_pass.php">重置密码</a>';
if ($level >= 2) {
    echo ' | 管理员: <a href="adjust_money.php">调整用户余额</a>';
    echo ' | <a href="users_info.php">用户资料</a>';
}
echo '<br><br>';

$balance = get_balance($dbc, $email);
echo '<table align="center" cellpadding="10">';
$balstr = number_format($balance, 2, '.', '');
echo "<tr><td align=\"right\">我的余额</td><td align=\"right\">¥ $balstr</td><tr>";
if ($level >= 2) {
    $adm = number_format(get_admin_balance($dbc), 2, '.', '');
    echo "<tr><td align=\"right\">NPN余额</td><td align=\"right\">SGD $adm</td><tr>";
}
echo '</table>';


include('includes/footer.html');
