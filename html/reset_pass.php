<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

session_start(); // access existing sesison
require('../mysqli_connect.php');

if (!isset($_SESSION['user_id']) || !isset($_SESSION['email'])) {
    require('includes/login_functions.inc.php');
    header("Location: login.php");
    exit();
//    redirect_user('login.php');
}

$page_title = '重置密码';
include('includes/header.html');

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    require('includes/login_functions.inc.php');
    require('includes/config.inc.php');

    $pass_old = $_POST['pass_old'];
    $up1 = $_POST['pass1'];
    $up2 = $_POST['pass2'];
    $email = $_SESSION['email'];

    $valid = true;
    
    list($check, $data) = check_login($dbc, $email, $pass_old);

    if ($check) {
        if (!verify_pass($up1)) {
            echo '<p class="error">密码必须至少8位 包含大小写字母和数字</p>';
            $valid = false;
        } else if ($up1 != $up2) {
            echo '<p class="error">两次密码不相同</p>';
            $valid = false;
        } else if ($pass_old == $up1) {
            echo '<p class="error">新密码不能和现有密码相同</p>';
            $valid = false;
        }
    } else {
        echo '<p class="error">原密码错误</p>';
        $valid = false;
    } 
    
    if ($valid) {
        $q = "update Users set pass=SHA1('$up1') where email='$email'";
        $r = @mysqli_query($dbc, $q);
        if (mysqli_affected_rows($dbc) == 1) {
            echo '<p>重置密码成功！</p>';
        } else {
            echo '<p class="error">系统错误 1132，请稍后再试</p>';
        }
    }
    
    mysqli_close($dbc);
}

function verify_pass($pass) {
    if (strlen($pass) < 8 || strlen($pass) > 16) {
        return false;
    }

    if (!preg_match('/[A-Z]/', $pass) ||
            !preg_match('/[a-z]/', $pass) ||
            !preg_match('/\d/', $pass)
    ) {
        return false;
    }

    return true;
}
?>

<h3>重置密码</h3>

<form action="reset_pass.php" method="post">
    <table align="center">
        <tr>
            <td align=right">原密码</td>
            <td align="left"><input type="password" name="pass_old" size="20" maxlength="50" value="" placeholder="" /></td>
        </tr>
        <tr>
            <td align=right">新密码</td>
            <td align="left"><input type="password" name="pass1" size="20" maxlength="50" value="" placeholder="8-16位密码" /></td>
        </tr>
        <tr>
            <td align=right">重复密码</td>
            <td align="left"><input type="password" name="pass2" size="20" maxlength="50" value="" placeholder="8-16位密码" /></td>
        </tr>
        <tr>
            <td></td>
            <td align="left"><input type="submit" name="submit" value="提交" /></td>
        </tr>
    </table>
</form>

<?php include('includes/footer.html'); ?>




