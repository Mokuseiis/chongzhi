<?php

require_once("CommonUtil.php");
require_once("wxpay/WxPayHelper.php");
require_once("wxpay/lib/WxPay.Api.php");
require_once("wxpay/lib/WxPay.Data.php");
require_once("wxpay/lib/WxPay.Exception.php");
require_once('../wxsdk/WXBizMsgCrypt.php');
require_once('../mysqli_connect.php');

ini_set('date.timezone', 'Asia/Shanghai');

// require_once('includes/config.inc.php');
//$email_xy = "26959915@qq.com,xiaoyi0310@yahoo.com";
//$email_all = "longfan1011@outlook.com," . $email_xy;


$now = time();
$w = date('w', $now);
$h = date('H', $now) + 0;
$i = date('i', $now) + 0;
$s = date('s', $now) + 0;

// echo "$w $h $i $s";

function is_weekend()
{
    $now = time();
    $w = date('w', $now);

    $promo = false;
    // echo "$w $h $i $s";
    if ($w == 6 || $w == 0) {
        return true;
    }
    return false;

}

function is_promo($start, $end)
{
    $now = time();
    $w = date('w', $now);
    $h = date('H', $now) + 0;
    $i = date('i', $now) + 0;
    $s = date('s', $now) + 0;

    $promo = false;
    // echo "$w $h $i $s";
    if ($w == 6 || $w == 0) {
        if ($h >= $start && $h <= ($end - 2)) {
            $promo = true;
        } else if ($h == ($end - 1) && $i <= 50) {
            $promo = true;
        }
    }
    return $promo;
}

function get_by_curl($url, $post = false)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    if ($post) {
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    }
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}

function https_request($url)
{
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $data = curl_exec($curl);
    if (curl_errno($curl)) {
        return 'ERROR ' . curl_error($curl);
    }
    curl_close($curl);
    return $data;
}

function getOpenidFromCode($code)
{

    $url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid=' . AppId .
        '&secret=' . AppSecret . '&code=' . $code . '&grant_type=authorization_code';

    $access_token_json = https_request($url);

    //echo $access_token_json;

    $access_token_array = json_decode($access_token_json, true);
    // echo $access_token_array;
    $access_token = $access_token_array['access_token'];
    $openid = $access_token_array['openid'];

    return $openid;
}

function writeHtml1($user, $telco, $mobile)
{
    $bal = 1000;//$user['balance'];
    $balstr = number_format($bal / 100.0, 2);

    echo '<div style="background-color: rgb(255, 255, 255);" class="clearfix">';
    echo '  <div class="middle-box" style="padding-bottom: 0;">';

    echo '    <p class="general-prompt">请输入手机号码</p>';
    echo '    <div class="input-group input-group-lg m-b">';
    echo '      <span class="input-group-addon">+65</span>';
    echo '      <input class="topup-number form-control" value="' . $mobile . '" type="text" placeholder="手机号码" type="tel" inputmode="numeric" pattern="[0-9]*">';
    echo '    </div>';
    echo '    <p class="general-prompt">请选择产品</p>';
    echo '    <div class="plan-selection-container">';
    echo '      <span class="plan-selection-voice plan-selection-selected">话费充值</span>';
    echo '      <span class="plan-selection-data">套餐</span>';
    echo '      <span class="plan-selection-social">流量</span>';
    echo '    </div>';
    echo '  </div>';
    echo '</div>';
}

function logger($log_content)
{
    if (isset($_SERVER['HTTP_APPNAME'])) {
        // SAE 205
        sae_set_display_errors(false);
        sae_debug($log_content);
        sae_set_display_errors(true);
    } else if ($_SERVER['REMOTE_ADDR'] != '127.0.0.1') {
        // LOCAL
        $max_size = 1000000;
        $log_filename = "log.xml";
        if (file_exists($log_filename) and (abs(filesize($log_filename)) > $max_size)) {
            unlink($log_filename);
        }
        file_put_contents($log_filename, date('Y-m-d H:i:s') . $log_content . "\r\n", FILE_APPEND);
    }
}

function writeProductLst($dbc, $telco)
{
    $util = new CommonUtil($dbc);
    $rate = $util->getExRate($dbc);

    $r = @mysqli_query($dbc, "select * from WXProducts where telco='$telco' order by disp_order, pid");
    $lst_pid = array();
    $lst_brief = array();
    $lst_kind = array();

    if (mysqli_num_rows($r) < 1) {
        $util->writeFooter();
        return $lst_pid;
    }

    $class_kind = '';


    echo '<div class="plan-selection-switcher plan-selection-switcher-voice plan-selection-switcher-visible">';
    echo '  <div class="middle-box">';
    echo '    <div class="plan-voice-selection-container clearfix">';
    echo '          <p class="social-alert">暂无该产品，敬请期待。</p>>';

    // add each product name
    $idx = 0;

    $ts_now = new DateTime('now');

    $m1promo = is_promo(12, 20);
    $shpromo = is_promo(10, 24);

    while ($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {

        $pid = $row['pid'];
        $telco = $row['telco'];
        $state = $row['state'];
        $kind = $row['kind'];

        if ($state == 2)
            continue;

        if (strpos($pid, '_we') == true) {
            if ($telco == 'sgm1' && !$m1promo)
                continue;
            if ($telco == 'sgsh' && !$shpromo)
                continue;
        }

        // if ($pid == 'sgm1_we18' || $pid == 'sgm1_s130') {
        //     continue;
        // }
        // if ($m1promo && ($pid == 'sgm1_10' || $pid == 'sgm1_s130')) {
        //     continue;
        // }


        $brief = $row['brief'];
        $price_sgd = $row['price_sgd'];

        $price = $util->getPriceInt($price_sgd, $rate);
        $price_str = '价格: ￥' . $util->getPriceString($price);

//            $price = $util->getCnyPrice($dbc, $pid);
//            $sprice = '价格: ￥' . number_format($price/100.0, 2);

        $brief = $price_str . '>>' . $brief;

        array_push($lst_brief, $brief);
        array_push($lst_pid, $pid);

        if ($idx == 0) {
            if($kind == 1){
                echo ' <span class="voice plan-voice-selection-' . $pid . ' plan-voice-selection-selected">';
            }
        } else {
            if($kind == 1){
                echo ' <span class="voice plan-voice-selection-' . $pid . '">';
            }else if($kind == 2){
                echo ' <span class="data display-none plan-voice-selection-' . $pid . '">';
            }else if($kind == 3){
                echo ' <span class="social display-none plan-voice-selection-' . $pid . '">';
            }
        }
        echo '   <span><a style="white-space: nowrap;">' . $row['name'] . '</a></span>';
        echo ' </span>';
        $idx++;
    }

    echo '    </div>'; // plan-voice-selection-container

    writeHtmlForm();

    // add brief
    echo '<div class="plan-voice-details-container">';
    $idx = 0;
    foreach ($lst_brief as $brief) {
        $pid = $lst_pid[$idx];
        if ($idx == 0) {
            echo '<div class="plan-voice-details-' . $pid . ' plan-voice-details-visible">';
        } else {
            echo '<div class="plan-voice-details-' . $pid . '">';
        }

//popup
        echo '<p><a href="javascript:void(0);" class="terms-description pull-right">这是什么？</a></p>';
        // $brief = '主卡 Main  Account 充值>>可漫游 可开流量包>>主卡 10新币 有效90天>>Special Bonus 1.5新币 有效30天>>10天免费接听电话';
        $lst_single = explode('>>', $brief);

        foreach ($lst_single as $point) {
            // echo '<p><i class="fa fa-star"></i>' . $point . '</p>';
            echo '<p>• ' . $point . '</p>';
        }
        echo '</div>'; // plan-voice-details
        $idx++;
    }
    echo '</div>'; // plan-voice-details-container
    echo '</div>'; // middle-box
    echo '</div>'; // plan-selection-switcher


    $util->writeFooter();


    return $lst_pid;
}

/*function writeProductLst($dbc, $telco)
{

    $util = new CommonUtil($dbc);
    $rate = $util->getExRate($dbc);

    $r = @mysqli_query($dbc, "select * from WXProducts where telco='$telco' order by disp_order, pid");
    $lst_pid = array();
    $lst_brief = array();
    $lst_kind = array();

    if (mysqli_num_rows($r) < 1) {
        $util->writeFooter();
        return $lst_pid;
    }

    $class_kind = '';


    echo '<div class="plan-selection-switcher plan-selection-switcher-voice plan-selection-switcher-visible">';
    echo '  <div class="middle-box">';
    echo '    <div class="plan-voice-selection-container clearfix">';

    // add each product name
    $idx = 0;

    $ts_now = new DateTime('now');

    $m1promo = is_promo(12, 20);
    $shpromo = is_promo(10, 24);

    while ($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {

        $pid = $row['pid'];
        $telco = $row['telco'];
        $state = $row['state'];
        $kind = $row['kind'];

        if ($state == 2)
            continue;

        if (strpos($pid, '_we') == true) {
            if ($telco == 'sgm1' && !$m1promo)
                continue;
            if ($telco == 'sgsh' && !$shpromo)
                continue;
        }

        // if ($pid == 'sgm1_we18' || $pid == 'sgm1_s130') {
        //     continue;
        // }
        // if ($m1promo && ($pid == 'sgm1_10' || $pid == 'sgm1_s130')) {
        //     continue;
        // }


        $brief = $row['brief'];
        $price_sgd = $row['price_sgd'];

        $price = $util->getPriceInt($price_sgd, $rate);
        $price_str = '价格: ￥' . $util->getPriceString($price);

//            $price = $util->getCnyPrice($dbc, $pid);
//            $sprice = '价格: ￥' . number_format($price/100.0, 2);

        $brief = $price_str . '>>' . $brief;

        array_push($lst_brief, $brief);
        array_push($lst_pid, $pid);
        array_push($lst_kind, $kind);

        if ($idx == 0) {
            echo ' <span class="plan-voice-selection-' . $pid . ' plan-voice-selection-selected">';
        } else {
            echo ' <span class="plan-voice-selection-' . $pid . '">';
        }
        echo '   <span><a style="white-space: nowrap;">' . $row['name'] . '</a></span>';
        echo ' </span>';
        $idx++;
    }

    echo '    </div>'; // plan-voice-selection-container

    writeHtmlForm();

    // add brief
    echo '<div class="plan-voice-details-container">';
    $idx = 0;
    foreach ($lst_brief as $brief) {
        $pid = $lst_pid[$idx];
        if ($idx == 0) {
            echo '<div class="plan-voice-details-' . $pid . ' plan-voice-details-visible">';
        } else {
            echo '<div class="plan-voice-details-' . $pid . '">';
        }

//popup
        echo '<p><a href="javascript:void(0);" class="terms-description pull-right">这是什么？</a></p>';
        // $brief = '主卡 Main  Account 充值>>可漫游 可开流量包>>主卡 10新币 有效90天>>Special Bonus 1.5新币 有效30天>>10天免费接听电话';
        $lst_single = explode('>>', $brief);

        foreach ($lst_single as $point) {
            // echo '<p><i class="fa fa-star"></i>' . $point . '</p>';
            echo '<p>• ' . $point . '</p>';
        }
        echo '</div>'; // plan-voice-details
        $idx++;
    }
    echo '</div>'; // plan-voice-details-container
    echo '</div>'; // middle-box
    echo '</div>'; // plan-selection-switcher


    $util->writeFooter();


    return $lst_pid;
}*/

function writeHtmlForm()
{
    // create form
    // echo '<div style="background-color: rgb(255, 255, 255);">';
    echo '<div>';
    echo '<div class="middle-box">';
    // echo '<form method="post" action="/starhub/unified-checkout" onsubmit="return fillupform();">';
    // echo '<form>';
    // echo '<form method="post" action="wx_confirm.php/" onsubmit="return false;">';
    echo '<form method="post" action="http://www.chongzhi.sg/wx_confirm.php/" onsubmit="return fillupform()">';
    echo '  <input type="hidden" name="topup_openid" value="_VAR_OPENID_">';
    echo '  <input type="hidden" name="topup_merchant" value="fomodigital">';
    echo '  <input type="hidden" class="submit-topup-plan" name="topup_plan" value="">';
    echo '  <input type="hidden" class="submit-topup-number" name="mobile" value="">';
    echo '  <input type="hidden" class="submit-topup-extra" name="topup_extra" value="">';

    echo '  <input type="hidden" class="submit-topup-pid" name="pid" value="">';
    echo '  <input type="hidden" class="submit-topup-openid" name="openid" value="">';

    echo '  <button class="btn btn-primary btn-block btn-lg" type="submit">立即购买</button>';

    // echo '  <a href="http://www.chongzhi.sg/wx_confirm.php">go to there</a>';
    // echo '  <button class="btn btn-primary btn-block btn-lg" type="button" onclick="fillupform()">立即购买</button>';
    // echo '</form>';
    echo '</form>';


    echo '</div>';
    echo '</div>';
}

function writeHtmlScripts($lst_pid, $openid)
{
    echo '<div class="alert-layer">';
    echo '  <div class="alert-layer-mask"></div>';
    echo '  <div class="middle-box" style="padding-top: 48px; height: 100%; overflow-y: auto;">';
    echo '    <div class="alert-layer-background">';
    echo '      <a class="pull-right alert-layer-close">&times;</a>';
    echo '      <h3 class="alert-layer-title"><i class="fa fa-warning"></i> <span>错误</span></h3>';
    echo '      <p><strong>请填写正确的预付手机号码。</strong></p>';
    echo '    </div>';
    echo '  </div>';
    echo '</div>';

// what's this
    echo '<div class="terms-description-layer">';
    echo '  <div class="terms-description-layer-mask"></div>';
    echo '  <div class="middle-box" style="padding-top: 48px; height: 100%; overflow-y: auto;">';
    echo '    <div class="terms-description-layer-background">';
    echo '      <a class="pull-right terms-description-layer-close">&times;</a>';

    echo '      <h3 class="terms-description-layer-title">主账户(Main Account)</h3>';
    echo '      <p>[主账户]里的金额可以用来拨打电话，发送短信，激活增值服务和购买流量套餐，在新加坡以外地区漫游时也可以使用</p>';

    echo '      <h3 class="terms-description-layer-title">红利账户(Bonus Account)</h3>	';
    echo '      <p>[红利账户]里面的金额只能用来拨打电话，发送短信，*不能*用来购买流量套餐</p>';

    echo '      <h3 class="terms-description-layer-title">附加账户</h3>	';
    echo '      <p>[本地话费] 本地话费只能用于拨打新加坡本地电话，发送本地短信</p>';
    echo '      <p>[国际话费] 国际话费只能用于拨打国际电话，发送国际短信</p>';

    echo '      <h3 class="terms-description-layer-title">有效期</h3>	';
    echo '      <p>预付卡(Prepaid SIM Card)的有效期从拨打第一个电话开始，用户可以通过充值延长有效期。如果在有效期内进行一次成功的充值，预付卡的有效期就会被累加，新的有效期为充值前所剩余的有效期加上新的充值所提供的有效期，但累加后的有效期最长不可以超过180天</p>';

    echo '    </div>';
    echo '  </div>';
    echo '</div>';


    echo '<script src="static/jquery-1.11.3-min.js"></script>';
    echo '<script src="static/jquery.formatter.min.js"></script>';
    // echo '<script src="/static/jquery-1.12.3.min.js"></script>';
    // .plan-voice-selection-container span
    // plan-voice-selection-$pid
    // plan-voice-selection-selected
    // plan-voice-details-visible

    echo "<script>";
    echo "  $('.plan-voice-selection-container span').click(function() {";
    //          for each product, make it looks like not selected
    echo "      $('.plan-voice-selection-container span').removeClass('plan-voice-selection-selected');";
    echo "      $('.plan-voice-details-container div').removeClass('plan-voice-details-visible');";
    echo '      var $this = $(this);';
    echo '      $this.addClass(\'plan-voice-selection-selected\');';
    //          find out the one selected, make it looks like selected
    foreach ($lst_pid as $pid) {
        echo '  if ($this.hasClass(\'plan-voice-selection-' . $pid . '\')) {';
        echo "    $('.plan-voice-details-" . $pid . "').addClass('plan-voice-details-visible');";
        echo "  }";
    }
    echo "  });";


    echo "$('.terms-description-layer-close').click(function() {";
    echo "  $('body').removeClass('show-terms-description');";
    echo "});";

    echo "$('.alert-layer-close').click(function() {";
    echo "  $('body').removeClass('show-alert');";
    echo "});";

    echo "$('.terms-description').click(function() {";
    echo "  $('body').addClass('show-terms-description');";
    echo "});";

    echo "var show_alert = function() {";
    echo '  $(\'body\').addClass(\'show-alert\');';
    echo "};";

    echo '$(\'.topup-number\').formatter({';
    echo "  'pattern': '{{9999}} {{9999}}'";
    echo "});";

    //Tab function - edited by Sihua
    echo '  var $plan = $(\'.plan-selection-container span\');';
    echo '  var $detailBox = $(\'.plan-voice-details-container\');';
    echo '  var $txtAlert = $(\'.social-alert\');';
    echo '  var $btn = $(\'.btn.btn-block\');';
    echo '  function tabCtrl(tabTarget,sibling1,sibling2){';
    echo '      $(\'.plan-voice-selection-container \'+ tabTarget).css(\'display\',\'inline-block\').eq(0).addClass(\'plan-voice-selection-selected\').siblings().removeClass(\'plan-voice-selection-selected\');';
    echo '      $(\'.plan-voice-selection-container \' + sibling1 + \',\' + sibling2).hide();';
    echo '      if(tabTarget === \'.social\'){';
    echo '          $txtAlert.show();';
    echo '          $detailBox.hide();';
    echo '          $btn.hide();';
    echo '      }else{';
    echo '          $txtAlert.hide();';
    echo '          $detailBox.show();';
    echo '          $btn.show();';
    echo '      }';
    echo '  }';

    echo '  $plan.click(function(){';
    echo '      $(\'.plan-selection-container span\').removeClass(\'plan-selection-selected\');';
    echo '      var $this = $(this);';
    echo '      $this.addClass(\'plan-selection-selected\');';
    echo '      if ($this.hasClass(\'plan-selection-voice\')) {';
    echo '          tabCtrl(\'.voice\',\'.data\',\'.social\');';
    echo "          $('.plan-voice-details-" . 'sgm1_10' . "').addClass('plan-voice-details-visible').siblings().removeClass('plan-voice-details-visible');";
    echo '      } else if ($this.hasClass(\'plan-selection-data\')) {';
    echo '          tabCtrl(\'.data\',\'.voice\',\'.social\');';
    echo "          $('.plan-voice-details-" . 'sgm1_d30' . "').addClass('plan-voice-details-visible').siblings().removeClass('plan-voice-details-visible');";
    echo '      } else if ($this.hasClass(\'plan-selection-social\')) {';
    echo '          tabCtrl(\'.social\',\'.voice\',\'.data\');';
    echo '      }';
    echo '  });';

    // echo "function fillupform2() { window.location = 'http://www.chongzhi.sg/wx_confirm.php'; }";

    echo '  function disableSubmit(form) {';
    echo '    var submit = form.submit;';
    echo '    submit.style.visibility = "hidden";';
    echo '    form.submit();';
    echo '  }';

    echo "function fillupform() {";

    // echo '  window.location = "http://www.chongzhi.sg/wx_confirm.php"; return false; ';
    // echo '  window.location.href = "http://www.chongzhi.sg/wx_confirm.php/' . 
    // '&test=1"; return false; ';

    echo "  var phonenumber = $('.topup-number').val();";
    echo "  phonenumber = phonenumber.split(' ').join('');";

    /*if (strlen(SG_DEMO_CARD) > 0) {
        echo " phonenumber = '80001234'; ";
    } else {
        echo "  if (!(/^\d{8}$/.test(phonenumber)) || (phonenumber.charAt(0) != '9' && phonenumber.charAt(0) != '8') ) {";
        echo "    show_alert();";
        echo "    return false;";
        echo "  }";
    }*/

    echo "  $('.topup-number').val(''); "; // make it empty
    echo "  $('.button').attr('disabled', 'true'); ";

//    echo '  var $plan = $(\'.plan-selection-container span.plan-selection-selected\');';
    echo "  var plan = 'voice';";

    echo '  var $submit_plan =   $(\'.submit-topup-plan\');';
    echo '  var $submit_number = $(\'.submit-topup-number\');';
    echo '  var $submit_extra =  $(\'.submit-topup-extra\');';
    echo '  var $submit_pid =  $(\'.submit-topup-pid\');';
    echo '  var $submit_openid =  $(\'.submit-topup-openid\');';

    // echo '  return true; ';

    echo '  var $plan_voice = $(\'.plan-voice-selection-container span.plan-voice-selection-selected\'); ';
    foreach ($lst_pid as $pid) {
        echo '  if ($plan_voice.hasClass(\'plan-voice-selection-' . $pid . '\')) { ';

        // echo '    var url = "http://www.chongzhi.sg/wx_confirm.php/' .
        // '?openid=' . $openid . '&pid=' . $pid . '&mobile=" + phonenumber; ';
        // echo '    window.location.href = url; ';
        echo '    $submit_number.val(phonenumber);';
        echo '    $submit_pid.val(\'' . $pid . '\'); ';
        echo '    $submit_openid.val(\'' . $openid . '\'); ';
        echo '    return true; ';
        echo "  }";
        // // echo '  $submit_plan.val(\'voice\');';
        // // echo '  $submit_number.val(phonenumber);';
        // // echo '  $submit_extra.val(' . $pid . ');';
        // // echo "  return false;";
    }

//     //echo "location.reload(true);";
    echo "  return false;";
    echo "}";

    echo '</script>';

    //include('wx_terms.html');
}

/**
 *
 * 获取jsapi支付的参数
 * @param array $UnifiedOrderResult 统一支付接口返回的数据
 * @throws WxPayException
 *
 * @return json数据，可直接填入js函数作为参数
 */
function GetJsApiParameters($UnifiedOrderResult)
{


    if (!array_key_exists("appid", $UnifiedOrderResult) || !array_key_exists("prepay_id", $UnifiedOrderResult) || $UnifiedOrderResult['prepay_id'] == "") {
        throw new WxPayException("参数错误");
    }
    $jsapi = new WxPayJsApiPay();
    $jsapi->SetAppid($UnifiedOrderResult["appid"]);
    $timeStamp = time();
    $jsapi->SetTimeStamp("$timeStamp");
    $jsapi->SetNonceStr(WxPayApi::getNonceStr());
    $jsapi->SetPackage("prepay_id=" . $UnifiedOrderResult['prepay_id']);
    $jsapi->SetSignType("MD5");
    $jsapi->SetPaySign($jsapi->MakeSign());
    $parameters = json_encode($jsapi->GetValues());
    return $parameters;
}

function showProduct($dbc, $util, $telco, $code)
{

    $pttl = "在线充值";
    $debug = true;//strlen(SG_DEMO_CARD) > 0 ? true : false;
    if ($debug) {
        $pttl = "充值【!测试!】";
    }
//    $util->writeHeader("images/telco/$telco.png", $pttl);
    $util->writeHeader($telco, $pttl);

//    $openid = getOpenidFromCode($code);
//    $nba = $_GET['nba'];
    /*if ($nba == 'pacers' && strlen($openid) < 1) {
        $openid = 'oawUFwbvmUbnVT_NHrXsZyk0PntA';
    }*/

//    $util = new CommonUtil($dbc);
//    $user = $util->getCreateDbUser($openid);
    // check $user is false?

    /*if ($user == false || $user == null) {
        $util->writeInfo("请用微信客户端打开本页.");
        $util->writeFooter();
        exit();
    }*/

//    $level = $user['level'];
    /*if ($debug && $level < 2) {
        $util->writeInfo("十分抱歉, 本站正在维护中, 请10分钟后再试.");
        $util->writeFooter();
        exit();
    }*/

    /*$q = "select pid, mobile from WXTrans where openid = '$openid' and state = 3 order by tid desc limit 20";
    $res = @mysqli_query($dbc, $q);
    $mob1 = '';
    $mob2 = '';
    $idx = 1;
    while ($row = mysqli_fetch_array($res, MYSQLI_ASSOC)) {
        $pid = $row['pid'];
        if (substr($pid, 0, 4) == $telco) {
            $mobile = $row['mobile'];
            if (strlen($mobile) == 8) {
                $mobile = substr($mobile, 0, 4) . ' ' . substr($mobile, 4);
                if ($idx == 1) {
                    $mob1 = $mobile;
                    $idx++;
                } else if ($idx == 2) {
                    $mob2 = $mobile;
                    break;
                }
            }
        }
    }*/

    /*if (strlen($mob2) > 0 && $mob1 != $mob2) {
        $mob1 = '';
    }*/

    writeHtml1(null, $telco, '');

    $lst_pid = writeProductLst($dbc, $telco);

    writeHtmlScripts($lst_pid, '');

    echo '</body></html>';
}

$util = new CommonUtil($dbc);


//$query = $_SERVER['QUERY_STRING'];
$telco = 'sgm1';//$_GET['telco'];
//$code = $_GET['code'];
//$state = $_GET['state'];

//echo "chongzhi.sg<br>Welcome!";
$test = "1,2";
$lst = preg_split("/,/", $test);
foreach ($lst as $upid) {
    //echo "$upid<br>";
}

showProduct($dbc, $util, $telco, null);


class wechatCallbackapiTest
{

    private $dbc = null;
    private $util = null;

    public function __construct($dbc, $util)
    {
        $this->dbc = $dbc;
        $this->util = $util;
    }

    public function valid($dbc)
    {
        $echoStr = $_GET["echostr"];

        $this->logger('echoStr' . $echoStr);

        $util = new CommonUtil($dbc);

        if ($this->checkSignature($util)) {
            echo $echoStr;
            exit;
        }
    }

    private function checkSignature($util)
    {

        $this->logger('checkSignature()');

        $echoStr = $_GET["echostr"];
        $signature = $_GET["signature"];
        $timestamp = $_GET["timestamp"];
        $nonce = $_GET["nonce"];
        $tmpArr = array(TOKEN, $timestamp, $nonce);
        sort($tmpArr);
        $tmpStr = implode($tmpArr);
        $tmpStr = sha1($tmpStr);

        if ($tmpStr == $signature) {
            return true;
        } else {
            return false;
        }
    }

    public function responseMsg()
    {

        $util = $this->util;

        $timestamp = $_GET['timestamp'];
        $nonce = $_GET['nonce'];
        $msg_signature = $_GET['msg_signature'];
        $encrypt_type = "raw";
        if (isset($_GET['encrypt_type'])) {
            $encrypt_type = $_GET['encrypt_type'] == 'aes' ? "aes" : "raw";
        }

//        $this->logger('encrypt_type=' . $encrypt_type);

        $postStr = $GLOBALS["HTTP_RAW_POST_DATA"];
        if (!empty($postStr)) {
            // decrypt it

            if ($encrypt_type == 'aes') {
                $pc = new WXBizMsgCrypt(TOKEN, EncodingAESKey, AppId);
                $this->logger(" D \r\n" . $postStr);
                $decryptMsg = "";
                $errCode = $pc->DecryptMsg($msg_signature, $timestamp, $nonce, $postStr, $decryptMsg);
                $postStr = $decryptMsg;
            }

            $this->logger(" R \r\n" . $postStr);

            $postObj = simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);

//            $openid = $object->FromUserName;

            $this->logger('postObj=' . $postObj);

            $RX_TYPE = trim($postObj->MsgType);

            $this->logger('rx_type=' . $RX_TYPE);

            $eventId = $postObj->Event;

            // $result = '';
//            $result = $this->receiveText($postObj);
            switch ($RX_TYPE) {
                case "event":
                    $result = $this->receiveEvent($postObj);
                    break;
                case "text":
                    $result = $this->receiveText($postObj);
                    break;
                default:
                    mail('longfan1011@outlook.com', "微信其他类型消息=$RX_TYPE", '', 'From: no-reply@chongzhi.sg');
                    break;
            }

            $this->logger(" R \r\n" . $result);

            // encrypt it
            if ($encrypt_type == 'aes') {
                $encryptMsg = '';
                $errCode = $pc->encryptMsg($result, $timestamp, $nonce, $encryptMsg);
                $result = $encryptMsg;
                $this->logger(" E \r\n" . $result);
            }

            echo $result;
            exit;
        } else {
            echo "";
            exit;
        }
    }

    private function getPidFullNameMap()
    {
        $dbc = $this->dbc;

        $q = 'select pid, full_name from WXProducts';
        $res = @mysqli_query($dbc, $q);

        $map;

        $util = new CommonUtil($dbc);

        if (mysqli_affected_rows($dbc) > 0) {
            while ($row = mysqli_fetch_array($res, MYSQLI_ASSOC)) {
                $map[$row['pid']] = $row['full_name'];
            }
        }

        return $map;
    }

    private function refundCalled($object)
    {
        $dbc = $this->dbc;
        $util = $this->util;

        $openid = $object->FromUserName;

        $map = $this->getPidFullNameMap();

        $user = $this->util->getCreateDbUser($openid);
        $level = $user['level'];

        $q = "select * from WXTrans where state > 0 and openid = '$openid' order by tid desc limit 10";

        if ($level > 0) {
            $q = "select * from WXTrans where state > 0 order by tid desc limit 10";
        }

        // $q = "select * from WXTrans where openid = '$openid' order by tid desc limit 3";
        // return $this->transmitText($object, $q);

        $res = @mysqli_query($dbc, $q);
        $content = '';
        if (mysqli_affected_rows($dbc) > 0) {

            $content = "";

            while ($row = mysqli_fetch_array($res, MYSQLI_ASSOC)) {

                $item = "----";
                $item .= "\n时间:" . $row['create_date'];
                $item .= "\n手机:" . $row['mobile'];
                $item .= "\n产品:" . $map[$row['pid']];

                $pay = $row['total_fee'];
                $item .= "\n支付:￥" . number_format($pay / 100.0, 2);

                $s = $row['state'];

                $state = $util->getStateStr($s);

                $item .= "\n" . $state . " (" . $row['tid'] . ")";

                // $content .= "\n" . $row['create_date'] . "(" . $row['tid'] . ")" .
                // $row['mobile'] . "-" . $row['pid'] . "-" . $state;

                $mode = $row['mode'] + 0;
                if ($mode == 1) {
                    $item .= "\n方式:【测试充值】";
                }

                $content = $item . "\n" . $content;
            }

            $content = "最近的交易记录【请仔细核对号码】\n" . $content;

            return $this->transmitText($object, $content);
        } else {
            return $this->transmitText($object, "您没有任何已付款订单，如仍需退款，请添加微信ID vicki_xiao 咨询，注明：充值退款");
        }
    }

    private function sendGroup($object)
    {

        $content = array();
        $content[] = array("Title" => "点击领取充值红包！",
            "Description" => "",
            "PicUrl" => "http://www.chongzhi.sg/images/welcome2.jpg",
            "Url" => "http://www.chongzhi.sg/images/ctsc_kf.jpg");
        $result = $this->transmitNews($object, $content);
        return $result;
    }

    private function receiveEvent($object)
    {

        $util = $this->util;
        $dbc = $this->dbc;

        $content = "";
        $eventKey = "";

        $event = $object->Event;
        $openid = $object->FromUserName;

        if ($event == "subscribe") {
            //##

            $user = $util->getCreateDbUser($openid);
            $uid = $user != false ? $user['uid'] : -1;
            $ref_uid = $util->getRefId($openid);
            $refmsg = $ref_uid > 0 ? "(推荐人=$ref_uid)" : "(无推荐人)";

            $q = "select * from WXUserPromo where openid = '$openid' and code = 'subs'";
            $res = @mysqli_query($dbc, $q);

            $content = "这里是畅通狮城,请点击输入栏最左开始充值~\n回复'红包'查看你的红包\n右下菜单可以查看号码和余额\n" .
                "如需退款请选择'退款'菜单\n如有疑问请添加客服微信: vicki_xiao"; //@@

            if (mysqli_affected_rows($dbc) < 1) {
                // first time subscribe
                $q = "insert into WXUserPromo (uid, openid, code, amount, state, create_time, expire_day) values "
                    . "                   ($uid, '$openid', 'subs', 600, 1,   now(),      3000) ";//###
                @mysqli_query($dbc, $q);
                $content .= "\n\n恭喜你，获得【6元充值红包，3小时内有效！】，选好商品，支付时自动抵扣价格！(3小时内有效，请尽快使用)";
            }

        } else if ($event == "CLICK") {
            $eventKey = $object->EventKey;
            if ($eventKey == 'history') {
                return $this->refundCalled($object);
            }
            if ($eventKey == 'check_hongbao') {
                $content = $util->getHongbaoDesc($openid);
            }
            if ($eventKey == 'check_number') {
                $content = "新电信SingTel，手机拨打 *1955\n星河StarHub，手机拨打 *113#\n第一通M1，手机拨打 #100*2*2#";
            }
            if ($eventKey == 'check_balance') {
                $content = "查询新电信SingTel回复 101\n";
                $content .= "查询星河Starhub回复 102\n";
                $content .= "查询第一通m1回复 103\n";
            }
            if ($eventKey == 'buy_data') {

                $content = "新电信SingTel用户请拨打 *363 按照短信指示，回复短信来订购流量包\n\n";
                $content .= "星河StarHub用户请拨打 *123*300#按照短信指示，回复短信来订购流量包\n\n";
                $content .= "第一通M1用户请拨打 #100*3#按照短信指示，回复短信来订购流量包\n\n";
                $content .= "流量包订购成功后，将会从您的主账户(Main)扣钱";
            }
        }


        $result = $this->transmitText($object, $content);
        return $result;
    }

    private function randCode($len)
    {
        $str = "23456789ABCDEFGHJKLMNPQRSTUVWXYZ";
        $pass = "";
        for ($i = 0; $i < $len; ++$i) {
            $idx = rand(0, strlen($str) - 1);
            $pass .= substr($str, $idx, 1);
        }
        return $pass;
    }

    private function isValidPromo($code)
    {
        $code = trim(strtolower($code));
        $dbc = $this->dbc;
        $q = "select * from WXPromoCode where code = '$code'";
        @mysqli_query($dbc, $q);
        if (mysqli_affected_rows($dbc) > 0)
            return true;
        return false;
    }

    private function genNewPromoCode()
    {
        $dbc = $this->dbc;
        $pass = $this->randCode(6);
        $q = "insert into WXPromoCode (code, state, amount) values ('$pass', 1, 100)";
        @mysqli_query($dbc, $q);
        if (mysqli_affected_rows($dbc) > 0)
            return $pass;
        return null;
    }

    private function addPromo($openid, $promo)
    {

        $body = "群包:$promo";

        $dbc = $this->dbc;
        $promo = trim(strtolower($promo));
        $q = "select * from WXUserPromo where code = '$promo'";
        $res = @mysqli_query($dbc, $q);

        $cnt = mysqli_affected_rows($dbc);
        if ($cnt >= 5) {
            mail('longfan1011@outlook.com', $body . " max=$cnt", '', 'From: no-reply@chongzhi.sg');
            return "很抱歉，该红包领取人数已达上限 $cnt 人，下次手再快点吧！";
        }

        while ($row = mysqli_fetch_array($res, MYSQLI_ASSOC)) {
            $oid = $row['openid'];
            if ($oid == $openid) {
                mail('longfan1011@outlook.com', $body . " repeat", '', 'From: no-reply@chongzhi.sg');
                return "您已领取该红包，请勿重复回复";
            }
        }

        $q = "insert into WXUserPromo " .
            " (openid,    code,   amount, state, create_time, expire_day, from_tid) values " .
            " ('$openid', '$promo', 100,  1,      now(),       1,          0) ";
        $log .= "$q\n";
        @mysqli_query($dbc, $q);
        if (mysqli_affected_rows($dbc) == 1) {
            mail('longfan1011@outlook.com', $body . " OK cnt=$cnt", '', 'From: no-reply@chongzhi.sg');
            return "恭喜你，红包已经添加到账户，购买产品时自动抵扣！（24小时过期）";
        }

        mail('longfan1011@outlook.com', $body . " ERR", '', 'From: no-reply@chongzhi.sg');
        return "抱歉，添加红包失败，请稍后再试";
    }

    private function receiveText($object)
    {

        $util = $this->util;
        $dbc = $this->dbc;

        $keyword = strtolower(trim($object->Content));
        $openid = $object->FromUserName;
        $user = $util->getCreateDbUser($openid);
        $level = $user['level'];
        $uid = $user['uid'];
        // weekend hongbao

        $hbpass = null;
        // if (is_weekend()) {
        $hbpass = '榴莲';
        // }
        $hbcode = 'hb170630';

        //$this->logger('keyword=' . $keyword);

        // if ($this->isValidPromo($keyword)) {
        // $content = $this->addPromo($openid, $keyword);
        // } 
        $sendmail = false;
        $mailtext = "user=$uid 说：$keyword";
        if (strpos($keyword, "流量", 0) !== false) {
            $content = "需要购买流量吗？请试一下右下角(更多)菜单哦";
            $mailtext .= " 自动回复：" . $content;
            $sendmail = true;
        } else if (strpos($keyword, "失败", 0) !== false ||
            strpos($keyword, "退款", 0) !== false ||
            strpos($keyword, "怎么还没", 0) !== false ||
            strpos($keyword, "没到账", 0) !== false ||
            strpos($keyword, "没充进", 0) !== false
        ) {

            $content = "看看是不是号码错了？如果无误可以去菜单退款哦。";
            $mailtext .= " 自动回复：" . $content;
            $sendmail = true;
        } else if (strpos($keyword, "号码", 0) !== false) {
            $content = "需要查本机号码吗？请试一下右下角(更多)菜单哦";
            $mailtext .= " 自动回复：" . $content;
            $sendmail = true;
        } else if (strpos($keyword, "余额", 0) !== false || strpos($keyword, "话费", 0) !== false) {
            $content = "需要查本机余额吗？请试一下右下角(更多)菜单哦";
            $mailtext .= " 自动回复：" . $content;
            $sendmail = true;
        } else if (strpos($keyword, "充值", 0) !== false) {
            $content = "要充值吗？请点击菜单开始哦";
            $mailtext .= " 自动回复：" . $content;
            $sendmail = true;
        } else if (strpos($keyword, "马上", 0) !== false || strpos($keyword, "立刻", 0) !== false || strpos($keyword, "多久", 0) !== false) {
            $content = "充值都是1-3分钟到账的哦，超过5分钟没到可以试着退款。";
            $mailtext .= " 自动回复：" . $content;
            $sendmail = true;
        } else if ($keyword == '红山' || $keyword == '小印度') {
            $content = '抱歉，红包口令已过期';
        } else if ($keyword == 'testemail') {
            mail('longfan1011@outlook.com', "hi hi", '', 'From: hello@chongzhi.sg');
            $content = 'done';
        } else if ($keyword == 'hbdata') {
            $q = "select * from WXUserPromo where code = '$hbcode'";
            $res = @mysqli_query($dbc, $q);
            $total = 0;
            $used = 0;
            $exp = 0;
            while ($row = mysqli_fetch_array($res, MYSQLI_ASSOC)) {
                if ($row['state'] == 2)
                    $used++;
                if ($row['state'] == 3)
                    $exp++;
                $total++;
            }
            $content = "$used/$total";
        } else if ($hbpass != null && $keyword == $hbpass) {
            $q = "select * from WXUserPromo where openid = '$openid' and code = '$hbcode'";
            $res = @mysqli_query($dbc, $q);
            $content = '';
            if (mysqli_num_rows($res) < 1) {
                $q = "insert into WXUserPromo (uid,   openid,    code,     amount, state, create_time, expire_day) values "
                    . "                  ($uid, '$openid', '$hbcode',  200,    1,       now(),       3000) ";
                @mysqli_query($dbc, $q);
                $content .= "恭喜你，获得【2元红包】，选好商品，支付时自动抵扣价格！(3小时内有效，请尽快使用)";
            } else {
                $content .= "红包已领取，请勿重复发送口令";
            }

        } else if (trim(strtolower($keyword)) == 'grpchat') {
            $newCode = $this->genNewPromoCode();
            if ($newCode != null) {
                $content = "识别二维码关注畅通狮城，就可以用微信给新加坡手机充值哦~ 关注后在*公众号*回复 $newCode 领取红包，数量有限 先到先得！(不是发消息给客服!不是发消息给客服!不是发消息给客服!)";
            } else {
                $content = "Failed";
            }
        } else if ($level >= 2 && strtolower($keyword) == 'modeprod') {
            $q = "update Settings set setting_value = 'prod' where setting_name = 'demo_topup'";
            @mysqli_query($dbc, $q);
            $content = mysqli_affected_rows($dbc) > 0 ? 'ok' : 'fail';

        } else if ($level >= 2 && strtolower($keyword) == 'modedebug') {
            $q = "update Settings set setting_value = '' where setting_name = 'demo_topup'";
            @mysqli_query($dbc, $q);
            $content = mysqli_affected_rows($dbc) > 0 ? 'ok' : 'fail';

        } else if ($level >= 2 && strtolower($keyword) == 'sgroup') {
            return $this->sendGroup($object);

        } else if (strtolower($keyword) == 'subs') {
            $pro_code = strtolower($keyword);
            $q = "select * from WXUserPromo where openid = '$openid' and code = '$pro_code'";
            $res = @mysqli_query($dbc, $q);
            if (mysqli_affected_rows($dbc) > 0) {
                $content = "关注红包已领取，请勿重复发送口令";
            } else {
                $q = "insert into WXUserPromo (uid, openid, code, amount, state, create_time, expire_day) values "
                    . " ($uid, '$openid', '$pro_code', 300, 1, now(), 1) ";
                @mysqli_query($dbc, $q);
                $content = "恭喜你，获得【3元充值红包】，选好商品，支付时自动抵扣价格！(24小时内有效，请尽快使用)";
            }

        } else if ($keyword == '红包') {
            $content = $util->getHongbaoDesc($openid);

        } else if (strtolower($keyword) == 'link') {
            $content = array();
            //http://www.chongzhi.sg/wx_share2.php?refid=$openid";
            $content[] = array("Title" => "分享领红包",
                "Description" => "",
                "PicUrl" => "http://www.chongzhi.sg/images/share/share_cover.jpg",
                "Url" => "http://www.chongzhi.sg/wx_share1.php?refid=$openid&src=link");
        } else if ($keyword == '101') {
            $content = "新电信SingTel 查询方法：\n";
            $content .= " ▶︎ 手机拔打 *139# 查主卡(Main)和红利(Special Bonus)余额\n";
            $content .= " ▶︎ 手机拔打 *139*3# 查流量\n";
            $content .= " ▶︎ 卡是支持漫游查询的，拔号后会运行USSD代码，余额和有效期时间";
        } else if ($keyword == '102') {

            $content = "";
            $content .= "星河StarHub查看余额方法：\n";
            $content .= " ▶︎ 如果你需要中文服务，拨打 *123*7*2# 换成中文界面\n";
            $content .= " ▶︎ 手机拔打 *123*1*1# 查主卡(Main Balance)余额和红利(Bonus Balance)余额\n";
            $content .= " ▶︎ 手机拔打 *123*1*2# 查Happy Plans套餐余额\n";
            $content .= " ▶︎ 手机拔打 *123*1*3# 查Data Plans流量包余额\n";
            $content .= " ▶︎ 想查询任何关于星河预付卡其他信息，拨打 *123#\n";
            $content .= " ▶︎ 卡是支持漫游查询的，拔了后会运行USSD代码，余额就显示出来了\n";
        } else if ($keyword == '103') {

            $content = "";
            $content .= "第一通M1查看余额方法：\n";
            $content .= " ▶︎ 如果你需要中文服务，拨打 #100*8*2# 换成中文界面\n";
            $content .= " ▶︎ 手机拨打#100*2*1# 查询主卡(Main Balance)\n";
            $content .= " ▶︎ 手机拨打 #100*2*1*3＃ 查询红利户头(Bonus)\n";
            $content .= " ▶︎ 卡是支持漫游查询的，拔了后会运行USSD代码，余额就显示出来了\n";
        } else if (strlen($keyword) > 0) {
            $sendmail = true;
            $content = "这里是畅通狮城,请点击输入栏最左开始充值~\n回复'红包'查看你的红包\n右下菜单可以查看号码和余额\n" .
                "如需退款请选择'退款'菜单\n如有疑问请添加客服微信:\nvicki_xiao";
        } else if (strstr($keyword, "pic")) {
            $content = array();
            $content[] = array("Title" => "single pic title",
                "Description" => "single pic content",
                "PicUrl" => "http://img1.imgtn.bdimg.com/it/u=1151512896,3470104745&fm=21&gp=0.jpg",
                "Url" => "http://chinapacers.com/");
            // $content[] = array("Title" => "single pic title 2",
            //     "Description" => "single pic content 2",
            //     "PicUrl" => "http://img1.imgtn.bdimg.com/it/u=1151512896,3470104745&fm=21&gp=0.jpg",
            //     "Url" => "http://chinapacers.com/");
        }

        if ($sendmail == true) {
            mail('longfan1011@outlook.com,26959915@qq.com', $mailtext, "http://chongzhi.sg/wx_trans.php?uid=$uid", 'From: no-reply@chongzhi.sg');
        }


        // $this->logger('content=' . $content);

        if (is_array($content)) {
            if (isset($content[0])) {
                $result = $this->transmitNews($object, $content);
            }
        } else {
            $result = $this->transmitText($object, $content);
        }
        return $result;
    }

    //$content = array("MediaId"=>$object->MediaId);        
    //$result = $this->transmitImage($object, $content);;        
    //return $result;
    // private function transmitImage($object, $imageArray) {
    // 	$itemTpl = "<Image><MediaId><![CDATA[%s]]></MediaId></Image>";
    // 	$item_str = sprintf($itemTpl, $imageArray['MediaId']);
    // 	$textTpl = "<xml><ToUserName><![CDATA[%s]]></ToUserName><FromUserName><![CDATA[%s]]></FromUserName><CreateTime>%s</CreateTime><MsgType><![CDATA[image]]></MsgType>$item_str</xml>";
    // 	$result = sprintf($textTpl, $object->FromUserName, $object->ToUserName, time());
    // 	return $result;    
    // }


    private function transmitText($object, $content)
    {

        $xmlTpl = "<xml>
            	<ToUserName><![CDATA[%s]></ToUserName>
            	<FromUserName><![CDATA[%s]></FromUserName>
            	<CreateTime>%s</CreateTime>
            	<MsgType><![CDATA[text]></MsgType>
            	<Content><![CDATA[%s]></Content>
            	</xml>";
        $result = sprintf($xmlTpl, $object->FromUserName, $object->ToUserName, time(), $content);

        // $this->logger('result=' . $result);
        return $result;
    }

    private function transmitNews($object, $arr_item)
    {
        if (!is_array($arr_item))
            return;
        $itemTpl = "<item><Title><![CDATA[%s]></Title><Description><![CDATA[%s]></Description><PicUrl><![CDATA[%s]></PicUrl><Url><![CDATA[%s]></Url></item>";
        $item_str = "";
        foreach ($arr_item as $item)
            $item_str .= sprintf($itemTpl, $item['Title'], $item ['Description'], $item['PicUrl'], $item['Url']);
        $newsTpl = "<xml><ToUserName><![CDATA[%s]></ToUserName><FromUserName><![CDATA[%s]></FromUserName><CreateTime>%s</CreateTime><MsgType><![CDATA[news]></MsgType><Content><![CDATA[]]></Content><ArticleCount>%s</ArticleCount><Articles>$item_str</Articles></xml>";
        $result = sprintf($newsTpl, $object->FromUserName, $object->ToUserName, time(), count($arr_item));
        return $result;
    }

    public function logger($log_content)
    {
        if (isset($_SERVER['HTTP_APPNAME'])) {
            // SAE 205
            sae_set_display_errors(false);
            sae_debug($log_content);
            sae_set_display_errors(true);
        } else if ($_SERVER['REMOTE_ADDR'] != '127.0.0.1') {
            // LOCAL
            $max_size = 1000000;
            $log_filename = "log.xml";
            if (file_exists($log_filename) and (abs(filesize($log_filename)) > $max_size)) {
                unlink($log_filename);
            }
            file_put_contents($log_filename, date('Y-m-d H:i:s') . $log_content . "\r\n", FILE_APPEND);
        }
    }

}

?>















