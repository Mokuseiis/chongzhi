<?php

//echo "<html><header>";
//echo '<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=0.5, maximum-scale=2.0, user-scalable=yes" />';


require_once('../mysqli_connect.php');

function getColor($curr_amt) {

	$style = '';

	if ($curr_amt >= 22500)
		$style .= 'f93';
	else if ($curr_amt >= 20000)
		$style .= 'f33';
	else if ($curr_amt >= 17500)
		$style .= 'a472cf';
	else if ($curr_amt >= 15000)
		$style .= '5ba7ed';
	else if ($curr_amt >= 12500) 
		$style .= '61eaf3';
	else if ($curr_amt >= 10000)
		$style .= '73da0f';
	else if ($curr_amt >= 7500)
		$style .= 'ff3';
	else if ($curr_amt >= 5000) 
		$style .= 'f93';
	else if ($curr_amt >= 2500)
		$style .= 'f33';
	else 
		$style = "";
	
	return $style;
}

function getColor2($curr_amt) {

	$style = '';
	if ($curr_amt >= 15000)
		$style .= '6a92f7';
	else if ($curr_amt >= 11000) 
		$style .= '61eaf3';
	else if ($curr_amt >= 8000)
		$style .= '73da0f';
	else if ($curr_amt >= 6500)
		$style .= 'ff3';
	else if ($curr_amt >= 5000) 
		$style .= 'f93';
	else if ($curr_amt >= 3500)
		$style .= 'f33';
	else if ($curr_amt >= 3000)
		$style .= 'a77';
	else if ($curr_amt >= 2500)
		$style .= '999';
	else if ($curr_amt >= 2000)
		$style .= 'ccc';
	else if ($curr_amt >= 1500)
		$style .= 'eee';
	else 
		$style = "";
	return $style;
}

function fullDate($time) {
	$tmp = "$time";
	$tmp = substr($tmp, 0, strlen($tmp) - 8);
	return $tmp;
}

function shortTime($time) {
	$tmp = "$time";
	$tmp = substr($tmp, 5, strlen($tmp) - 8);
	return $tmp;
}

function compareTb($t1, $t2, $dbc) {

	$sum1 = 0;
	$sum2 = 0;

	$q = "SELECT sum(price_rmb) from Transactions where create_date >= '$t1' and create_date < '$t2' and status = 1";
	$res = @mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_array($res, MYSQLI_ASSOC)) {	
		$sum1 = $row['sum(price_rmb)'];
	}

	$q = "SELECT sum(prod_fee) FROM WXTrans where create_date >= '$t1' and create_date < '$t2' and state = 3";
	$res = @mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_array($res, MYSQLI_ASSOC)) {	
		$sum2 = $row['sum(prod_fee)'] / 100.0;
	}
	$rate = 100 * $sum2 / $sum1;
	$sum1 = number_format($sum1, 0);
	$sum2 = number_format($sum2, 0);
	$rate = number_format($rate, 2);

	echo "$t1 - $t2: $sum1 vs $sum2 ($rate%)<br>";
}

$uid = $_GET['uid'];

if ($uid > 0) {

	echo "all transactions by user $uid<br><br>";
	$content = '';
	
	$q = "select * from WXTrans where uid=$uid and state > 0 order by create_date desc";
	$res = @mysqli_query($dbc, $q);
	while ($row = mysqli_fetch_array($res, MYSQLI_ASSOC)) {	
		$uid = $row['uid'];
		$pid = $row['pid'];
		$topup_err = $row['topup_err'];
		$total_fee = number_format($row['total_fee']/100, 2);
		$date = shortTime($row['create_date']);
		$mobile = $row['mobile'];
		$mobile2 = $mobile;
		$state = $row['state'];
		if ((0 + $state) <= 3)
			$mobile2 = substr($mobile, 0, 6) . '**';
		$topup_err2 = $topup_err;
		if (substr($topup_err, 0, 2) == 'OK')
			$topup_err2 = '';

		$content .= "uid=$uid $pid $mobile  fee=$total_fee [$state] $date $topup_err\n\n";
		echo        "uid=$uid $pid $mobile2 fee=$total_fee [$state] $date $topup_err2<br>";

	}
	mail('26959915@qq.com,longfan1011@outlook.com', "Trans for uid=$uid", $content, 'From: no-reply@chongzhi.sg');
	exit();
}

echo "failed topups:<br>";
$q = "SELECT * FROM WXTrans where state in (101, 102) and refund_id is null and tid > 530 order by create_date desc limit 20";
$res = @mysqli_query($dbc, $q);
while ($row = mysqli_fetch_array($res, MYSQLI_ASSOC)) {
	$uid = $row['uid'];
	$pid = $row['pid'];
	$total_fee = number_format($row['total_fee']/100, 2);
	$date = shortTime($row['create_date']);
	$mobile = $row['mobile'];
	echo "uid=$uid, $mobile, fee=$total_fee, $date<br>";
}



$transtbl = "<br><table style=\"font-family: arial; text-align: right\" border=\"1\">";
$q = "select * from WXTrans where uid <> 15 and uid <> 27 and uid <> 13 and uid <> 16 and total_fee > 1 and state > 0 and tid > 530 order by tid desc";
$res = @mysqli_query($dbc, $q);
$cnt = mysqli_affected_rows($dbc);

$weekly = '<br>weekly summary: <br>';
$prev_w = -1;
$idx = $cnt;
$total = 0;
$cnt2 = 0;
$cnt3 = 0;
$prev_date = '';
$daily_order_cnt = 0;
$week = array();
$week_user = array();
$cnt_order = 0;

$day_cnt = 0;
while ($row = mysqli_fetch_array($res, MYSQLI_ASSOC)) {

	$create = $row['create_date'];
	$topup = $row['topup_date'];
	$tid = $row['tid'];
	$tdiff = strtotime($topup) - strtotime($create);
	if ($tdiff > 5000)
		$tdiff = 'big';
	if ($tdiff < -5000)
		$tdiff = 'big';

	$w = date('w', strtotime($create));

	$time = shortTime($create);
	$date = fullDate($create);

	if (strlen($prev_date) > 0 && $date != $prev_date) {
		$day_cnt++;
		$daily_sum = number_format($cnt2 / 100.0, 2);
		$q2 = "select * from WXUsers where create_time < '" . $prev_date . "' and create_time >= '" . $date . "'";
		$res2 = @mysqli_query($dbc, $q2);
		$user_cnt = mysqli_affected_rows($dbc);

		$insert = 0;
		if (count($week) < 1) {
            // 16 25 34 
			$insert = 7 - $prev_w;
			for ($i = 0; $i < $insert; ++$i) {
				array_push($week, '-');
				array_push($week_user, '-');
			}
			array_push($week_user, '-');
		}
		array_push($week, round($cnt2 / 100));

		array_push($week_user, $user_cnt);
		$transtbl .= "<tr> <td>$prev_date</td>  <td>$daily_sum</td>  <td>$daily_order_cnt</td>  </tr>";
		$daily_order_cnt = 0;
		$cnt2 = 0;
	}

	if ($prev_w >= 0 && $w != $prev_w && $w == 0) {
		$prev_w = $w;
		$weekly .= "weekly $time = " . number_format($cnt3 / 100.0, 2) . "<br>";
		$cnt3 = 0;
	}

	$prev_w = $w;
	$prev_date = $date;
	$uid = $row['uid'];
	$state = $row['state'];
	$mobile = $row['mobile'];

	$refund_id = $row['refund_id'];
	$err = $refund_id != null && $state == 3;

	if ($state == 101 || $state == 102)
		$price = "state=$state<br>$mobile";
	else if ($state == 3) {
		if ($err == false) {
			$total_fee = $row['total_fee'];
			$cnt2 += $total_fee;
			$cnt3 += $total_fee;
			$total += $total_fee;
			$price = number_format(($total_fee + 0.0) / 100.0, 2);
			$npn_cost = $row['npn_cost'] * 4.95;
			$npn_cost = 100*($total_fee - $npn_cost)/$npn_cost;

			$cnt_order++;
		}
		else {
			$price = "err";
		}
	}
	else {
		$price = "state=$state";
	}

	$pid = $row['pid'];


	$hongbao = '';
	if ($row['upid_lst']) {
		$hongbao .= $row['upid_lst'];
	}
	if ($row['upcode']) {
		$hongbao = $row['upcode'];
	}
	if ($row['upid'] > 0) {
		$hongbao .= ' ' . $row['upid'];	
	}
	$i = 0;
	while (($i + 20) < strlen($hongbao)) {
		$hongbao = substr($hongbao, 0, $i+20) . '<br>' . substr($hongbao, $i+20);
		$i += 20;
	}

	$link = "<a href='http://chongzhi.sg/wx_trans.php?uid=$uid'>$uid</a>";

	$shorttime = substr($time, 6);

	if ($day_cnt < 3)
		$transtbl .= "<tr>  <td>$tid</td>  <td>$link</td>  <td>$pid</td>  <td>$price</td> <td>$hongbao</td>  <td>$shorttime ($tdiff)</td>";
	$daily_order_cnt++;
	$idx--;
}

$transtbl .= "</table>";

$transtbl = "total=" . number_format($total / 100.0, 2) . " order=" . $cnt_order . "<br>" . count($week) . " days<br><br>" . $transtbl . "<br>";

$weekname = array('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun');

$weektbl = "<table style=\"font-family: arial; text-align: right\" border=\"1\"> <tr>";
for ($i = 0; $i < count($weekname); ++$i) {
	$weektbl .= "<th>" . $weekname[$i] . "</th>";
}
$weektbl .= "<th>week total</th> </tr>";

$col = 0;

$tmp = '';
$len = count($week);
$insert = 7 - ($len % 7);
for ($i = 0; $i < $insert; ++$i) {
	array_push($week, '-');
	array_push($week_user, '-');
}
if ($insert > 0) {
	array_pop($week_user);
}

$week = array_reverse($week);
$week_user = array_reverse($week_user);

$weeksum = 0;
$weeksum_pre = -1;
$weeksum_user = 0;
$weekfig = '';
$weekidx = 1;

for ($i = 0; $i < count($week); ++$i) {
	$prev = $i - 7;
	$prev_amt = 0;
	$curr_amt = $week[$i];
	$user_cnt = $week_user[$i];
	$perc = '';
	if ($curr_amt != '-' && $prev >= 0 && $week[$prev] != '-') {
		$prev_amt = $week[$prev] + 0;
		if ($prev_amt > 0) {
			$perc = "<br>(" . round(100 * ($curr_amt - $prev_amt) / $prev_amt) . "%)";
		}
	}

	$style = '';
    $color = getColor($curr_amt);
    if (strlen($color) > 0)
    	$style = " style=\"background-color: #$color\"";

	$tmp .= "<td" . $style . ">" . $curr_amt . "-" . $user_cnt . "</td>";
	$weeksum += $curr_amt;
	$weeksum_user += $user_cnt;

	if ($col % 7 == 6) {

		$perc = '-';

		$color = getColor($weeksum/7);
		if (strlen($color) > 0)
			$style = " style=\"background-color: #$color\"";
		else 
			$style = '';
		
		$style2 = '';
		if ($weeksum_pre >= 0 && $weeksum > 0) {
			$perc = 100*($weeksum/$weeksum_pre);
			if ($perc > 99.999)
				$style2 = " style=\"background-color: #73a270\"";
			$perc = number_format($perc, 1) . "%";
		}

		$weektbl .= "<tr>  $tmp  <td" . $style . ">$weeksum-$weeksum_user</td>  <td" . $style2 . ">$perc</td>  </tr>";

		$weeksum_pre = $weeksum;

		for ($m=0; $m<$weeksum/1000; ++$m) {
			if ($m > 0 && $m % 10 == 0)
				$weekfig .= "|";
			else 
				$weekfig .= '.';
		}
		$weekfig .= ' ' . $weekidx++ . '<br>';
		$weeksum = 0;
		$weeksum_user = 0;
		$tmp = '';
	}
	$col++;
	if ($col > 6) {
		$col = 0;
	}
}

$weektbl .= "</table>";

echo $weektbl . '<br>' . $weekfig . $transtbl;

// for ($i=0; $i<7; ++$i) {
// 	echo $weekname[$i] . '=' . $week[$i] . "<br>";
// }

compareTb('20160501', '20160601', $dbc);
compareTb('20160601', '20160701', $dbc);
compareTb('20160701', '20160801', $dbc);
compareTb('20160801', '20160901', $dbc);
compareTb('20160901', '20161001', $dbc);
compareTb('20161001', '20161101', $dbc);
compareTb('20161101', '20161201', $dbc);
compareTb('20161201', '20170101', $dbc);
compareTb('20170101', '20170201', $dbc);
compareTb('20170201', '20170301', $dbc);
compareTb('20170301', '20170401', $dbc);
compareTb('20170401', '20170501', $dbc);
compareTb('20170501', '20170601', $dbc);
compareTb('20170601', '20170701', $dbc);
compareTb('20170701', '20170801', $dbc);

exit();
?>













