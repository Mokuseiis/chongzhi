<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function redirect_user ($page = 'index.php') {
    $url = 'http://' . dirname($_SERVER['PHP_SELF']);
    $url = rtrim($url, '/\\');
    $url .= '/' . $page;
    
    header("Location: $url");
    exit();
}

function get_products_for_telco($dbc, $tcid = 'singtel') {
    $q = "select * from Products where tcid = '$tcid' order by rank";
    $r = @mysqli_query($dbc, $q);
    return $r;
}

function get_admin_balance($dbc) {
    $q = "select * from Settings where setting_name = 'npn_balance'";
    $r = @mysqli_query($dbc, $q);
    if (mysqli_affected_rows($dbc) == 1) {
        $row = mysqli_fetch_array($r, MYSQLI_ASSOC);
        $bal = floatval($row['setting_value']);
        return $bal;
    }
    return 0;
}

function check_login($dbc, $email = '', $pass = '') {
    $errors = array();
    
    if (empty($email)) {
        $erros[] = 'Please enter email address';        
    }
    else {
        $e = mysqli_real_escape_string($dbc, trim($email));        
        $e = strtolower($e);
    }
    
    if (empty($pass)) {
        $erros[] = 'Please enter password';        
    }
    else {
        $pass = mysqli_real_escape_string($dbc, trim($pass));        
    }
    
    if (empty($errors)) {
        $q = "select user_id, nick, active, email, level from Users where email='$e' and pass=SHA1('$pass')";
        $r = @mysqli_query($dbc, $q);
        if (mysqli_num_rows($r) == 1) {
            $row = mysqli_fetch_array($r, MYSQLI_ASSOC);            
            if ($row['active'] != NULL) {
                $errors[] = '账户 ' . $e . ' 尚未激活，请检查邮箱及*垃圾邮件*。';
            } else {              
                $ip = $_SERVER['REMOTE_ADDR'];
                $q = "update Users set last_login=NOW(), last_ip='$ip' where email='$e'";
                @mysqli_query($dbc, $q);
               return array(true, $row);
            }
        }
        else {
            $errors[] = 'Email不存在 或 密码不对...';
        }
    }
    
    return array(false, $errors);
}

function get_balance($dbc, $email) {
    $q = "select * from MoneyTrans where email='$email' and status=1";
    $r = @mysqli_query($dbc, $q);
    $balance = 0.0;
    if (@mysqli_affected_rows($dbc) > 0) {
        while ($row = mysqli_fetch_array($r)) {
            $amt = $row['amount'];
            $balance += $amt;
        }
    }
    return $balance;
}

function verify_pass($pass) {
    if (strlen($pass) < 8 || strlen($pass) > 16) {
        return false;
    }

    if (!preg_match('/[A-Z]/', $pass) ||
            !preg_match('/[a-z]/', $pass) ||
            !preg_match('/\d/', $pass)
    ) {
        return false;
    }

    return true;
}

function safe_value($dbc, $param, $key, $def_val = '') {
    $v = isset($param[$key]) ? $param[$key] : $def_val;
    if ($v !== $def_val) {
        $v = trim($v);
        $v = mysqli_real_escape_string($dbc, $v);
    }
    return $v;
}



