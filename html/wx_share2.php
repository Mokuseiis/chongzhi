<?php

require_once("CommonUtil.php");
require_once('../wxsdk/WXBizMsgCrypt.php');
require_once('../mysqli_connect.php');

require_once 'includes/config.inc.php';

function get_by_curl($url, $post = false) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	if ($post) {
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
	}
	$result = curl_exec($ch);
	curl_close($ch);
	return $result;
}

function https_request($url) {
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	$data = curl_exec($curl);
	if (curl_errno($curl)) {
		return 'ERROR ' . curl_error($curl);
	}
	curl_close($curl);
	return $data;
}

function getOpenidFromCode($code) {

	$url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid=' . AppId .
	'&secret=' . AppSecret . '&code=' . $code . '&grant_type=authorization_code';

	$access_token_json = https_request($url);

    //echo $access_token_json;

	$access_token_array = json_decode($access_token_json, true);
    // echo $access_token_array;
	$access_token = $access_token_array['access_token'];
	$openid = $access_token_array['openid'];

	return $openid;
}

function addRefer($dbc, $util, $refid, $openid) {

	if ($dbc == null || $refid == null || $openid == null || strlen($refid) < 1 || strlen($openid) < 1) {
		return false;
	}
	if ($refid == $openid) {
		return false;
	}

	$q = "select * from WXRefer where openid = '$openid'";
	$res = @mysqli_query($dbc, $q);
	if (mysqli_affected_rows($dbc) > 0) {
		return false;
        // do nothing, user already have a referer
	} 
	
	$q = "select * from WXRefer where refid = '$openid' and openid = '$refid' ";
	@mysqli_query($dbc, $q);
	if (mysqli_affected_rows($dbc) > 0) {
		// do nothing, do not support mutual refering
		return false;
	}

	$ref_user = $util->getUser($refid);
	$ref_uid = $ref_user == false ? -1 : $ref_user['uid'];

	$user = $util->getUser($openid);
	$uid = $user == false ? -1 : $user['uid'];

	$q = "insert into WXRefer " . 
	" (refid, openid, create_time, state, ref_uid, uid) values " .
	" ('$refid', '$openid', now(), 1, $ref_uid, $uid) ";

	@mysqli_query($dbc, $q);

	mail(EMAIL_ADMIN, "Refer ref_uid=$ref_uid, uid=$uid", "refer successful", EMAIL_NO_REPLY);

	return true;
}

$util = new CommonUtil($dbc);

$refid = isset($_GET['refid']) ? $_GET['refid'] : '';
$code = isset($_GET['code']) ? $_GET['code'] : '';
$openid = strlen($code) > 0 ? getOpenidFromCode($code) : '';

// if (strlen($refid) > 0 && strlen($code) > 0 && strlen($openid) > 0) {
	// addRefer($dbc, $util, $refid, $openid);	
// }

$src = isset($_GET['src']) ? $_GET['src'] : 'unknown';

if (strlen($refid) > 0) {

	// echo "$refid";
	echo "<html><header><title>【畅通狮城公众号】新加坡手机充值 #新电信#星河#m1 三网全覆盖!</title>";
	echo '<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0">';
	echo "</header><body style=\"background-color: rgb(131,190,70);\">";

	$link = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx70e6a86297c8b6ba&redirect_uri=http%3a%2f%2fwww.chongzhi.sg%2fwx_share3.php%2f%3frefid%3d$refid%26src%3d$src&response_type=code&scope=snsapi_userinfo&state=1#wechat_redirect";

	echo "<a href=\"$link\">";
	echo '<img width="100%" src="http://www.chongzhi.sg/images/share/share2_3.jpg"></img>';
	echo '</a>';

	echo "</body></html>";
	$user = $util->getUser($refid);
	$uid = -1;
	if ($user != false) {
		$uid = $user['uid'];
	}
	// refid is openid
	$q = "insert into WXStats (kind, tag1, uid1, openid1, create_time) values ('share2', '$src', $uid, '$refid', now() )";
	@mysqli_query($dbc, $q);
	// mail(EMAIL_ADMIN, "share2, uid=$uid", "mark", EMAIL_NO_REPLY);
}
else if (strlen($code) > 0) {
	$code = $_GET['code'];
	header("Location: https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx70e6a86297c8b6ba&redirect_uri=http%3a%2f%2fwww.chongzhi.sg%2fwx_share2.php%2f%3frefid%3d$openid&response_type=code&scope=snsapi_userinfo&state=1#wechat_redirect");
	exit();
} else {
	// echo "hello";
}

?>












