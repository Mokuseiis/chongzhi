<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$page_title = '注册';
include('includes/header.html');

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    require('../mysqli_connect.php');
    require('includes/config.inc.php');
    require('includes/login_functions.inc.php');

//    require(MYSQL);

    $ue = strtolower(safe_value($dbc, $_POST, 'email'));
    $up1 = safe_value($dbc, $_POST, 'pass1');
    $up2 = safe_value($dbc, $_POST, 'pass2');

//    echo $ue;
//    echo $up1;
//    echo $up2;

    $e = $p = FALSE;

    if (filter_var($ue, FILTER_VALIDATE_EMAIL)) {
        $e = $ue;
    } else {
        echo '<p class="error">请输入有效Email</p>';
    }

    if (verify_pass($up1)) {
        if ($up1 == $up2) {
            $p = $up1;
        } else {
            echo '<p class="error">重复密码错误</p>';
        }
    } else {
        echo '<p class="error">请输入有效密码(8-16位，包含大小写字母及数字)</p>';
    }

    if ($e & $p) {

        $q = "select active, level, email from Users where email = '$e'";
        $r = @mysqli_query($dbc, $q);

        if (mysqli_affected_rows($dbc) < 1) {

            $a = md5(uniqid(rand(), true)); // create activation code

            $q = "select email from Users where email = '$e'";
            $r = @mysqli_query($dbc, $q);
            if (mysqli_affected_rows($dbc) > 0) {
                echo '<p class="error">此Email已存在</p>';
            } else {
                $q = "insert into Users (email, pass, active, reg_date, level) values " .
                        "('$e', SHA1('$p'), '$a', NOW(), 0)";
                $r = @mysqli_query($dbc, $q);
                if (mysqli_affected_rows($dbc) == 1) {
                    $body = "谢谢你的注册，请点击以下链接来激活你的账户：\n\n" .
                            BASE_URL .
                            'activate.php?x=' . urlencode($e) .
                            "&y=$a";

                    mail($e, '注册确认', $body, EMAIL_NO_REPLY); // todo change email

                    echo "<h3>非常感谢！</h3><p>激活电邮即将发送到 " . $e . "<br>请查收收件箱(同时查看垃圾邮箱)来激活你的账户。<br>";
                    include('includes/footer.html');
                    mail(EMAIL_ADMIN, 'New user', "$e", EMAIL_NO_REPLY);
                    exit();
                } else { // insert failed?
                    echo '<p class="error">系统错误 1607，请稍后再试<p/>';
                }
                mysqli_close($dbc);
                include('includes/footer.html');
                exit();
            }
        } else { // email exist?
            if (mysqli_num_rows($r) > 0) {
                // todo check active != NULL and provide resent activation option
                echo '<p class="error">Email已经注册。'
                . 'If you have forgotten your password, use the link at right to have your password sent to you. </p>';
            }
        }
    }

    mysqli_close($dbc);
}
?>

<h3>注册</h3>

<form action="register.php" method="post">
    <table align="center" cellpadding="4">
        <tr>
            <td align="right">Email</td>
            <td align="left"><input type="text" name="email" size="20" maxlength="50" required="" 
                       value="<?php if (isset($_POST['email'])) echo $_POST['email']; ?>" /></td>
        </tr>
        <tr>
            <td></td>
            <td align="left" class="tip">请使用你的支付宝的Email注册 方便服务器查询余额</td>
        </tr>
        <tr>
            <td align="right">密码</td>
            <td align="left"><input type="password" required name="pass1" size="20" maxlength="50" value="" placeholder="8-16位密码" /></td>
        </tr>
        <tr>
            <td align="right">重复密码</td>
            <td align="left"><input type="password" required name="pass2" size="20" maxlength="50" value="" placeholder="8-16位密码" /></td>
        </tr>
        <tr><td></td>
            <td align="left" class="tip">8-16位，含有大小写密码和数字</td>
        </tr>
        <tr>
            <td></td>
            <td align="left"><input type="submit" name="submit" value="注册" /></td>
        </tr>
    </table>
</form>

<?php include('includes/footer.html'); ?>




