<?php

require_once("WxPay.config.php");
//require_once("../CommonUtil.php");
require_once("MD5SignUtil.php");

class WxPayHelper {

    var $paramLst;

    function __construct() {
    }

    function setParameter($param, $paramValue) {
        $sname = CommonUtil::trimString($param);
        $svalue = CommonUtil::trimString($paramValue);
        $this->paramLst[$sname] = $svalue;
    }

    function getParameter($param) {
        return $this->paramLst[$param];
    }

    // generate random 16 char string
    function create_noncestr($length = 16) {
        
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $str = "";
        
        while ($length > 0) {
            $min = 0;
            $max = strlen($chars) - 1;
            $idx = mt_rand($min, $max);
            $str .= substr($chars, $idx, 1);
            $length -= 1;
        }
        return $str;
    }

    function check_cft_parameters() {
        if ($this->paramLst["bank_type"] == null ||
                $this->paramLst["body"] == null ||
                $this->paramLst["partner"] == null ||
                $this->paramLst["out_trade_no"] == null ||
                $this->paramLst["total_fee"] == null ||
                $this->paramLst["notify_url"] == null ||
                $this->paramLst["spbill_create_ip"] == null ||
                $this->paramLst["input_charset"] == null) {
            return false;
        }
        return true;
    }

    protected function get_cft_package() {
        try {
            if (null == PARTNERKEY || "" == PARTNERKEY) {
                throw new SDKRuntimeException("密钥不能为空！" . "<br>");
            }
            $commonUtil = new CommonUtil();
            ksort($this->paramLst);
            $unSignParaString = $commonUtil->formatQueryParaMap($this->paramLst, false); // urlencode
            $paraString = $commonUtil->formatQueryParaMap($this->paramLst, true); // urlencode
            $md5SignUtil = new MD5SignUtil(); 

            $md5SignedStr = $md5SignUtil->sign($unSignParaString, $commonUtil->trimString(PARTNERKEY));

            return $paraString . "&sign=" . $md5SignedStr;
        }
        catch (SDKRuntimeException $e) {
            die($e->errorMessage());
        }
    }

    protected function get_biz_sign($bizObj) {

        foreach ($bizObj as $k => $v) {
            $bizParameters[strtolower($k)] = $v;
        }

        try {
            if (null == APPKEY || "" == APPKEY) {
                throw new SDKRuntimeException("APPKEY为空！" . "<br>");
            }
            $bizParameters["appkey"] = APPKEY;
            ksort($bizParameters);
            $commonUtil = new CommonUtil();
            $bizString = $commonUtil->formatBizQueryParaMap($bizParameters, false);
            return sha1($bizString);
        } 
        catch (SDKRuntimeException $e) {
            die($e->errorMessage());
        }
    }

    // generate app payment request json
    function create_app_package($traceid = "") {
        try {
            if ($this->check_cft_parameters() == false) {
                throw new SDKRuntimeException("生成package参数缺失！" . "<br>");
            }

            $nativeObj["appid"] = APPID;
            $nativeObj["package"] = $this->get_cft_package();
            $nativeObj["timestamp"] = time();
            $nativeObj["traceid"] = $traceid;
            $nativeObj["noncestr"] = $this->create_noncestr();
            $nativeObj["app_signature"] = $this->get_biz_sign();
            $nativeObj["sign_method"] = SIGNTYPE;
            return json_encode($nativeObj);
        } catch (SDKRuntimeException $e) {
            die($e->errorMessage());
        }
    }

    // generate jsapi pay request json
    function create_biz_package() {

        try {
            if ($this->check_cft_parameters() == false) {
                throw new SDKRuntimeException("生成package参数缺失！" . "<br>");
            }
            $nativeObj["appId"] = APPID;
            $nativeObj["package"] = $this->get_cft_package();
            $nativeObj["timeStamp"] = time();
            $nativeObj["nonceStr"] = $this->create_noncestr();
            $nativeObj["paySign"] = $this->get_biz_sign($nativeObj);
            $nativeObj["signType"] = SIGNTYPE; 
            return json_encode($nativeObj);
        }
        catch (SDKRuntimeException $e) {
            die($e->errorMessage());
        }
    }

    function create_native_url($productid) {

        $commonUtil = new CommonUtil();
        $nativeObj["appid"] = APPID;
        $nativeObj["productid"] = urlencode($productid);
        $nativeObj["timestamp"] = time();
        $nativeObj["noncestr"] = $this->create_noncestr();
        $nativeObj["sign"] = $this->get_biz_sign();
        $bizString = $commonUtil->formatBizQueryParaMap($nativeObj, false);
        return "weixin://wxpay/bizpayurl?" . $bizString;
    }
    
    function unifiedOrder() {
        //https://api.mch.weixin.qq.com/pay/unifiedorder
        
    }

    // generate native pay request xml
    function create_native_package($retcode = 0, $reterrmsg = "ok") {

        try {
            if ($this->check_cft_parameters() == false) {
                throw new SDKRuntimeException("生成package参数缺失！" . "<br>");
            }
            $nativeObj["AppId"] = APPID;
            $nativeObj["Package"] = $this->get_cft_package();
            $nativeObj["TimeStamp"] = time();
            $nativeObj["NonceStr"] = $this->create_noncestr();
            $nativeObj["RetCode"] = $retcode;
            $nativeObj["RetErrMsg"] = $reterrmsg;
            $nativeObj["AppSignature"] = $this->get_biz_sign($nativeObj);
            $nativeObj["SignMethod"] = SIGNTYPE;

            $commonUtil = new CommonUtil();
            return $commonUtil->arrayToXml($nativeObj);
        }
        catch (SDKRuntimeException $e) {
            die($e->errorMessage());
        }
    }

}
?>























