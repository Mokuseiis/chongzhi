<?php

$page_title = '确认购买';

require_once('../mysqli_connect.php');
require_once("CommonUtil.php");
require_once("wxpay/WxPayHelper.php");
require_once("wxpay/lib/WxPay.Api.php");
require_once("wxpay/lib/WxPay.Data.php");
require_once("wxpay/lib/WxPay.Exception.php");

function getUnifiedOrder($openid, $body, $fee, $attach) {

    $fee = strval($fee);

    //②、统一下单
    $input = new WxPayUnifiedOrder();
    $input->SetBody($body);
    $input->SetAttach($attach);
    $input->SetOut_trade_no(WxPayConfig::MCHID . date("YmdHis"));
    $input->SetTotal_fee($fee);
    $input->SetTime_start(date("YmdHis"));
    $input->SetTime_expire(date("YmdHis", time() + 600));
//    $input->SetGoods_tag("test");
    $input->SetNotify_url("http://www.chongzhi.sg/wx_payres.php");
    $input->SetTrade_type("JSAPI");
    $input->SetOpenid($openid);
    $order = WxPayApi::unifiedOrder($input);
    $jsApiParameters = GetJsApiParameters($order);

//    echo $jsApiParameters . "<br>";

    return $jsApiParameters;
}

/**
 * 
 * 获取jsapi支付的参数
 * @param array $UnifiedOrderResult 统一支付接口返回的数据
 * @throws WxPayException
 * 
 * @return json数据，可直接填入js函数作为参数
 */
function GetJsApiParameters($UnifiedOrderResult) {

    if (!array_key_exists("appid", $UnifiedOrderResult) || !array_key_exists("prepay_id", $UnifiedOrderResult) || $UnifiedOrderResult['prepay_id'] == "") {
        return null;
        // throw new WxPayException("参数错误");
    }
    $jsapi = new WxPayJsApiPay();
    $jsapi->SetAppid($UnifiedOrderResult["appid"]);
    $timeStamp = time();
    $jsapi->SetTimeStamp("$timeStamp");
    $jsapi->SetNonceStr(WxPayApi::getNonceStr());
    $jsapi->SetPackage("prepay_id=" . $UnifiedOrderResult['prepay_id']);
    $jsapi->SetSignType("MD5");
    $jsapi->SetPaySign($jsapi->MakeSign());
    $parameters = json_encode($jsapi->GetValues());
    return $parameters;
}

require_once('../wxsdk/WXBizMsgCrypt.php');

// function getCnyPrice($dbc, $pid) {
//     $prod = getProduct($dbc, $pid);
//     $price_cny = $prod['price_cny'] + 0;
//     $price_sgd = $prod['price_sgd'] + 0;
//     $q = "select * from Settings where setting_name = 'ex_rate'";
//     $res = @mysqli_query($dbc, $q);
//     if (mysqli_affected_rows($dbc) > 0) {
//         $setting = mysqli_fetch_array($res, MYSQLI_ASSOC);
//         $rate = floatval($setting['setting_value']);
//         if ($rate > 4) {
//             $price = $price_sgd * $rate;
//             return round($price);
//         }
//     }
//     if ($price_cny > 100) {
//         return $price_cny;
//     }
//     return 1000000;
// }

function getProduct($dbc, $pid) {
    $q = "select * from WXProducts where pid = '$pid'";
    $res = @mysqli_query($dbc, $q);
    $prod = flase;
    if (mysqli_affected_rows($dbc) == 1) {
        $prod = mysqli_fetch_array($res, MYSQLI_ASSOC);
    }
    return $prod;
}

function displayErrorMsg($msg) {
    $util = new CommonUtil();
    $util->writeHeader("/images/ctsc256.jpg", "无法操作");

    echo '<div style="background-color: rgb(255, 255, 255);" class="clearfix">';
    echo '  <div class="middle-box" style="padding-bottom: 0px;">';
    echo "    <p class=\"general-prompt-warn\">$msg</p>";
    echo '  </div>';
    echo '</div>';

    $util->writeFooter();

    echo '</body></html>';
}

function writeHtml1($dbc, $util, $tid, $prod, $params, $mobile, $price, $promo_amt, $price_a, $openid, $hbdesc) {

    echo '<link href="/css/bootstrap.min.css" rel="stylesheet">';
    echo '<link href="/css/font-awesome.css" rel="stylesheet">';

    echo '<link href="/css/toastr.min.css" rel="stylesheet">';

    echo '<link href="/css/animate.css" rel="stylesheet">';
    echo '<link href="/css/style.css" rel="stylesheet">';


    echo '<!DOCTYPE html><html><head>';
    echo '<link href="/css/wx_base.css" rel="stylesheet">';


    echo '<meta charset="utf-8"><title>畅通狮城</title></head>';

    $telco = $prod['telco'];
    $menu_id = $prod['menu_id'];

    echo "<script language=\"javascript\">";
    echo "function callpay() { ";
    echo "  WeixinJSBridge.invoke('getBrandWCPayRequest', $params, function(res) {";
    echo "    WeixinJSBridge.log(res.err_msg);";
    echo "    if(res.err_msg == \"get_brand_wcpay_request:ok\" ) { ";
    echo "      window.location = "; 
    echo "\"http://www.chongzhi.sg/wx_paydone.php?tid=$tid&menu_id=$menu_id&openid=$openid&telco=$telco&state=1&err_msg=\" + res.err_msg; ";
    echo "    } else { ";
    echo "      window.location = ";
    echo "\"http://www.chongzhi.sg/wx_paydone.php?tid=$tid&menu_id=$menu_id&openid=$openid&telco=$telco&state=0&err_msg=\" + res.err_msg; ";
    echo "    } ";
    // echo "    alert(res.err_code + ", " + res.err_desc + ", " + res.err_msg);";
    echo "  });";
    echo "} ";
    echo "</script>";

    echo '<body>';
    echo '<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0">';


    // create title image and text
    echo '<div style="background-color: rgb(211, 1, 50);">';
    echo '  <div class="middle-box text-center" style="font-size: 0px; padding: 0px 0px;">';
    echo '    <img style="width: 30%;" src="/images/ctsc256.jpg">';
    echo '    <span style="font-weight: bold; display: inline-block; width: 60%; color: #fff; font-size: 20px; vertical-align: middle; text-align: center;}">';
    echo '      确认付款';
    echo '    </span>';
    echo '  </div>';
    echo '</div>';

    $price_s = number_format($price / 100.0, 2);
    $promo_amt_s = number_format($promo_amt / 100.0, 2);
    $price_a_s = number_format($price_a / 100.0, 2);

    $pname = $prod['full_name'];
    $mobstr = substr($mobile, 0, 4) . ' ' . substr($mobile, 4);

    echo '<div style="background-color: rgb(255, 255, 255);" class="clearfix">';
    echo '  <div class="middle-box" style="padding-bottom: 0px;">';
    echo "    <p class=\"general-prompt\">请确认信息无误, 然后点击付款</p>";
    echo "    <p class=\"general-prompt-big\">$mobstr</p>";
    echo "    <p class=\"general-prompt-warn\">【注意】输入错误号码造成的损失，将无法退款。</p>";
    echo "    <p class=\"general-prompt\">充值类型: $pname</p>";
    echo "    <p class=\"general-prompt\">价格: ￥$price_s</p>";
    if ($promo_amt > 0) {
    	echo "    <p class=\"general-prompt\">$hbdesc</p>";
    	echo "    <p class=\"general-prompt\">付款: ￥$price_a_s</p>";
	}

    echo '  </div>';
    echo '</div>';


    echo '<div style="background-color: rgb(255, 255, 255);">';
    echo '<div class="middle-box">';
    // echo '<form method="post" action="/starhub/unified-checkout" onsubmit="return fillupform();">';
    echo '<form method="post" action="" onsubmit="return fillupform();">';
    echo '<input type="hidden" name="topup_openid" value="_VAR_OPENID_">';
    echo '<input type="hidden" name="topup_merchant" value="fomodigital">';
    echo '<input type="hidden" class="submit-topup-plan" name="topup_plan" value="">';
    echo '<input type="hidden" class="submit-topup-number" name="topup_number" value="">';
    echo '<input type="hidden" class="submit-topup-extra" name="topup_extra" value="">';

    // echo '<button class="btn btn-primary btn-block btn-lg" type="submit">立即购买</button>';
    // echo '<button class="btn btn-primary btn-block btn-lg" type="submit">立即购买</button>';
    echo "<button class=\"btn btn-primary btn-block btn-lg\" type=\"button\" onclick=\"callpay()\">微信支付 $price_a_s</button>";

    echo '</form>';
    echo '</div>';
    echo '</div>';


//    echo "<button class=\"btn btn-primary btn-block btn-lg\" type=\"button\" onclick=\"callpay()\">wx pay test</button>";
}

$util = new CommonUtil($dbc);

if (isset($_POST['openid']) && isset($_POST['pid']) && isset($_POST['mobile'])) {

    $openid = $_POST['openid'];
    $pid = $_POST['pid'];
    $mobile = $_POST['mobile'];

    $t1 = strlen($openid);
    $t2 = strlen($pid);
    $t3 = strlen($mobile);

    if ($t1 < 1 || $t2 < 1 || $t3 < 1) {
        exit();
    }

	// get product
    $prod = $util->getProduct($dbc, $pid);
    $rate = $util->getExRate($dbc);
    $price_sgd = $prod['price_sgd'];
    $price = $util->getPriceInt($price_sgd, $rate);
    $body = $prod['full_name'] . ' ' . $mobile;

	// get promotions
	$promo_lst = $util->getPromo2($openid, $mobile, $price);
    $promo_amt = 0;
    $upid_lst = '';
    $upcode = '';
    $hbdesc = '';

    foreach ($promo_lst as $promo) {
    	$upid = $promo['upid'];
		$amt1 = $promo['amount'];
		$promo_amt += $amt1;
		$code1 = $promo['code'];
		$create_time = $promo['create_time'];
		$time1 = "$create_time";
		$time1 = substr($time1, 0, strlen($time1)-3);

		$hbname = '红包';
		if ($code1 == 'subs') {
			$upcode = 'subs';
			$hbname = 'subs红包';
		}
		else if ($code1 == 'refer') {
			$hbname = '朋友红包';
		}
		else {
			$hbname = $code1 . '红包';
		}

		$upid_lst .= strlen($upid_lst) > 0 ? ",$upid" : "$upid";
        // - 朋友红包 ￥0.50  (2016-07-24 15:35)
        // - subs红包 $3.00  (2016-07-23 23:23)
		$hbdesc .= "&nbsp;&nbsp;- $hbname ￥" . number_format($amt1/100.0, 2) . "&nbsp;&nbsp;($time1)<br>";
    }

    if (strlen($hbdesc) > 0) {
    	$hbdesc = "可用红包:<br>" . $hbdesc;
    }
    
    // $promo = $util->getPromo($openid, $mobile);
    // $promo_amt = 0;
    // $upid = -1;
    // $upcode = '';
    // if ($promo != null) {
    //     $upid = $promo['upid'];
    //     $promo_amt = $promo['amount'];
    //     $upcode = $promo['code'];
    // }



    // $rate = $util->getExRate($dbc);
    // $price_sgd = $prod['price_sgd'];
    // $price = $util->getPriceInt($price_sgd, $rate);

    $price_a = $price - $promo_amt;
    if ($price_a < 1) {
    	$price_a = 1;
    }

    $user = $util->getCreateDbUser($openid);
	$uid = $user['uid'];

	// check repeat requests
    $ts_now = new DateTime('now');
    $ts_now = $ts_now->getTimestamp();
    $q = "select * from WXTrans where " .
            "mobile = '$mobile' and pid = '$pid' order by create_date desc";
    $res = @mysqli_query($dbc, $q);
    $trans = false;
    $ts_exist = 0;
    $tid = false;
    $buffer = strlen(SG_DEMO_CARD) > 0 ? 10 : 30;

    if (mysqli_affected_rows($dbc) > 0) {

        while ($row = mysqli_fetch_array($res, MYSQLI_ASSOC)) {

            $state = $row['state'];
            $ts_exist = strtotime($row['create_date']);
            $diff = $ts_now - $ts_exist;

            $paid = $state > 0;
            $repeat = $diff < $buffer;

            if ($paid && $repeat) {
                $util->addLog('trans', 'err', $row['tid'], "paid & repeat");
                $wait = $buffer - $diff;
                $msg = "有相同的充值于 " . $diff . " 秒前发生. 未防止重复操作, 请等待 " . $wait . " 秒重试。";
                displayErrorMsg($msg);
                return;
            }
        }
    }

    $util->addLog('trans', 'ok', -1, "no repeat");

    $mode = strlen(SG_DEMO_CARD) > 0 ? 1 : 2;

    $q = "insert into WXTrans " . 
    "(openid, uid, pid, mobile, prod_fee, state, create_date, update_date, upcode, mode, upid_lst) " .
    "values " .
    "('$openid', $uid, '$pid', '$mobile', $price_a, 0, now(), now(), '$upcode', $mode, '$upid_lst')";

    $res = @mysqli_query($dbc, $q);

    if (mysqli_affected_rows($dbc) == 1) {
        $trans = mysqli_fetch_array($res, MYSQLI_ASSOC);
        $tid = $dbc->insert_id;
    } else {
        displayErrorMsg('系统错误，无法生成预订单');
        return;
    }

    $attach = $tid . '';

    $params = getUnifiedOrder($openid, $body, $price_a, $attach);

    writeHtml1($dbc, $util, $tid, $prod, $params, $mobile, $price, $promo_amt, $price_a, $openid, $hbdesc);

    echo "<body>";
    echo "</body></html>";
} else {
    echo "";
}
?>















