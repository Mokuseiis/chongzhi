<?php

// Transactions is default fail (insert status = 0)
// MoneyTrans is default success (insert status = 1)
// if npnOk set Trans s = 1
// if npnFail set MoneyTran s = 0

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

session_start(); // access existing sesison
require('../mysqli_connect.php');

if (!isset($_SESSION['user_id']) || !isset($_SESSION['email'])) {
    require('includes/login_functions.inc.php');
    header("Location: login.php");
    exit();
//    redirect_user('login.php');
}

$page_title = 'Top Up';
include('includes/header.html');
require('includes/login_functions.inc.php');

$display_type = 'telco';

$tcid = '';

echo '<SCRIPT LANGUAGE="JavaScript">';
echo '  function disableSubmit(form) {';
echo '    var submit = form.submit;';
echo '    submit.style.visibility = "hidden";';
echo '    form.submit();';
echo '  }';
echo '</SCRIPT>';


//$u_mid = $_GET['menu_id'];
$u_code = safe_value($dbc, $_GET, 'code', '');

if (strlen($u_code) > 0) {
    $display_type = 'product';
} else if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    require('includes/config.inc.php');
    $email = $_SESSION['email'];

    if (isset($_POST['action'])) {
        // user clicked telco
        $tcid = safe_value($dbc, $_POST, 'action');
        $_SESSION['tcid'] = $tcid;
        $display_type = 'product';
    } else {
        // user clicked topup
        $u_mid = safe_value($dbc, $_POST, 'menu_id');
        $u_mob1 = safe_value($dbc, $_POST, 'mob1');
        $u_mob2 = safe_value($dbc, $_POST, 'mob2');
        $remark = safe_value($dbc, $_POST, 'remark');
        $remark = substr($remark, 0, 48);
        $u_mob_prev = safe_value($dbc, $_POST, 'mob_prev');

        $mid = $mob = false;

        $arr_err = array();

        if (verify_menu_id($u_mid)) {
            $mid = $u_mid;
        } else {
            array_push($arr_err, '* 请选择一个产品');
        }

        $mob = verify_mobile($u_mob_prev);

        if ($mob != false) {
            if (strlen($u_mob1) > 0 || strlen($u_mob2) > 0) {
                $mob = false;
                array_push($arr_err, '请仅填入号码 或 选择一个备选号码');
            }
        } else {
            $u_mob1 = verify_mobile($u_mob1);
            if ($u_mob1 !== false) {
                // mobile1 verified
                $u_mob2 = verify_mobile($u_mob2);
                if ($u_mob1 == $u_mob2) {
                    $mob = $u_mob1;
                } else {
                    array_push($arr_err, '* 两次输入的手机号不匹配</p>');
                }
            } else {
                array_push($arr_err, "* '$u_mob1' 不是一个合法的新加坡手机号码</p>");
            }
        }

        if ($mid && $mob) {
            topup_go($dbc, $email, $mob, $mid, $remark);
//        mysqli_close($dbc); // todo enable            
        } else {
            echo '<p class="error">';
            foreach ($arr_err as $err) {
                echo $err . '<br>';
            }
            echo '</p>';

//            $tcid = $_SESSION['tcid'];
            $display_type = 'product';
            $u_code = safe_value($dbc, $_POST, 'code');
        }
    }
}

if ($display_type == 'product') {
// check if user logged in, otherwise, redirect to login
// get a list of products

    $email = $_SESSION['email'];

    $balance = get_balance($dbc, $email);
    echo "<p align = \"center\"><a href=\"topup.php\">返回</a> $tcid 所有产品 | 余额: ";
    $balstr = number_format($balance, 2, '.', '');
    if ($balance < 5) {
        echo "<font color=\"#ea354f\">¥$balstr</font></p>";
    } else {
        echo "<font color=\"#4aa04e\">¥$balstr</font></p>";
    }

    //##
    if (strlen(SG_DEMO_CARD) > 0) {
        echo "<font color=\"#ea354f\">正在维护网站，请稍等10分钟重试...</font></p>";
        include('includes/footer.html');
        exit();
    }

    echo '<form action="topup.php" method="post" onSubmit="javascript: this.submit.disabled=true">';
    $q = "select * from Products where code='$u_code'";
    $r = @mysqli_query($dbc, $q);

    if (mysqli_affected_rows($dbc) != 1) {
        echo $q;
//       header("Location: topup.php");
        exit();
    }

    $code = $u_code;
    $row = mysqli_fetch_array($r, MYSQLI_ASSOC);
    $menu_id = $row['menu_id'];
    $prod_name = $row['name0'];
    $brief = $row['brief'];
    $tcid = $row['tcid'];
    $price_str = number_format($row['price_rmb'], 2, '.', '');

    // get history mobile numbers
    $q = "select * from Numbers where email='$email' order by last_date desc limit 6";
    $nums = @mysqli_query($dbc, $q);

    echo '<p style="color:#dd112e">' . $brief . '</p>';

    echo '<table cellspacing="0" cellpadding="6" align="center">';

    echo '<tr>';
    echo '<td colspan="2"><img width="200" src="images/cards/' . $tcid . '/' . $code . '.jpg">';
    echo '<br>' . $prod_name . '</td>';
    echo '<td><h2 style="color:#f3113b">¥' . $price_str . '</h2></td>';
    echo '</tr>';

    echo '<input type="hidden" name="menu_id" value="' . $menu_id . '"/>';
    echo '<input type="hidden" name="code" value="' . $code . '"/>';

    $idx = 0;

    echo '<tr><td class="topup_header" colspan="3">1. 选择或输入一个手机号</td></tr>';
    $prev_count = mysqli_affected_rows($dbc);
    while ($row = mysqli_fetch_array($nums, MYSQLI_ASSOC)) {
        $num = $row['mobile'];
        $num = verify_mobile($num);
        if ($num != false) {
            if ($idx == 0) {
                echo '<tr><td>备选号码</td>';
            } else if ($idx % 2 == 0) {
                echo '<tr><td></td>';
            }
            echo "<td align=\"left\"><input type=\"radio\" name=\"mob_prev\" value=\"$num\"/>$num</td>";
            if ($idx % 2 == 1 || $idx >= $prev_count - 1) {
                echo '</tr>';
            }
            $idx++;
        }
    }

    echo '<tr><td align="right">新加坡手机号</td><td align="left"><input type="text" name="mob1" value="" placeholder="8位数字"/></td></tr>';
    echo '<tr><td align="right">重复号码</td>   <td align="left"><input type="text" name="mob2" value="" placeholder="8位数字"/></td></tr>';

    // remark
    echo '<tr><td class="topup_header" colspan="3">2. 添加备注 (可选，限50字)</td></tr>';
    echo '<br><td colspan="3"><textarea name="remark" cols="40" rows="3" width="100%"></textarea></td></tr>';

    if (strlen(SG_DEMO_CARD) > 0) {
        echo '<tr><td class="error" colspan="3">注意：网站处于测试状态</td></tr>';
    }

    echo '<tr><td></td><td><input type="submit" class="btn btn-default" name="submit" value="购买"/><td></tr>';
    echo '<tr><td colspan="3" align="center">点击后请耐心等待，过程可能长达10秒</td></tr>';

    echo '</table></form>';
} else if ($display_type == 'telco') {

    // singtel m1 starhub
    $r = @mysqli_query($dbc, "select * from Products order by tcid, rank");

    if (mysqli_num_rows($r) > 0) {
        if (strlen(SG_DEMO_CARD) > 0) {
            echo '<h3>所有产品 (TESTING)</h3>';
        } else {
            echo '<h3>所有产品</h3>';
        }

        $col_max = 3;
        $tcid = '';

        echo '<table align="center" cellpadding="4">';
        $col_id = 0;
        $prod_id = 0;

        while ($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {

            $newline = false;
            $tcid_cur = $row['tcid'];
            $code = $row['code'];
            $name = $row['name0'];
            $price_rmb = $row['price_rmb'];

            $new_telco = false;

            if ($prod_id == 0 || $col_id == 0) {
                $newline = true;
            }

            if ($tcid != $tcid_cur) {
                $new_telco = true;
                $newline = true;
                $tcid = $tcid_cur;
                $col_id = 0;
            }

            if ($col_id >= $col_max) {
                $col_id = 0;
                $newline = true;
            }

            if ($newline === true) {
                if ($prod_id === 0)
                    echo "<tr>";
                else
                    echo '</tr><tr>';

                if ($new_telco == true) {
                    echo '<td colspan="3">';
                    if ($prod_id != 0)
                        echo '<br>';
                    echo '<img width="145" src="/images/' . $tcid . '.jpg"/></td></tr><tr>';
                }
            }

            echo "<td align=\"left\">";
            // 290 x 182
            echo '<a href="?code=' . $code . '"><img width="145" src="images/cards/' . $tcid_cur . '/' . $code . '.jpg"></a><br>';
            echo '<a href="?code=' . $code . '">' . $name . '</a><br>';
            echo '<div style="color:#f3113b;font-weight:bold;font-size:20px;">¥' . $price_rmb . '</div>';
            echo "</td>";

            $prod_id++;
            $col_id++;
        }
        echo '</tr>';
        echo '</table>';

//        echo '<form action="topup.php" method="post">';
//        echo '<table align="center">';
//        echo '<tr>';
//        while ($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
//            $name0 = $row['name0'];
//            $tcid = $row['tcid'];
//            echo "<td><input type=\"submit\" name=\"action\" value=\"$tcid\"/></td>";
//        }
//        echo '</tr>';
//        echo '</table></form>';
    } else {
        echo '<p class="error">数据库出错，请稍候访问</p>';
    }
}

function verify_menu_id($mid) {
    // 3 digit
    if (preg_match('/^\d{3,3}$/', $mid) || preg_match('/^\d{4,4}$/', $mid)) {
        return true;
    } else {
        return false;
    }
}

function verify_mobile($mob) {
    // remove space and +
    $mob = str_replace(array(' ', '-', '+'), '', $mob);

    if (preg_match('/^[98]\d{7,7}$/', $mob)) {
        return $mob;
    } else if (preg_match('/^65[98]\d{7,7}$/', $mob)) {
        $mob = substr($mob, 2);
        return $mob;
    } else {
        return false;
    }
}

function topup_go($dbc, $email, $mob, $menu_id, $remark) {

    if (verify_mobile($mob) == false || !verify_menu_id($menu_id)) {
        echo "<p class=\"error\">System error 1047. 请稍后再试</p>";
        return;
    }

    $q = "select price_rmb, name0 from Products where menu_id=$menu_id";
    $r = @mysqli_query($dbc, $q);
    if (mysqli_affected_rows($dbc) == 1) {
        $row = mysqli_fetch_array($r, MYSQLI_ASSOC);
        $price = $row['price_rmb'];
        $name0 = $row['name0'];
        $balance = get_balance($dbc, $email);
        if ($balance < $price) {
            echo "<p class=\"error\">余额不足，请先充值</p>";
            return;
        }
    } else {
        echo "<p class=\"error\">系统错误 915，请稍后再试</p>";
        return;
    }


    $mob = "65" . $mob;
    $menu_id_org = $menu_id;
    if (strlen(SG_DEMO_CARD) > 0) {
        $menu_id = SG_DEMO_CARD;
    }

    $q = "select price_rmb from Products where menu_id=$menu_id_org";
    $r = mysqli_query($dbc, $q);
    $price_rmb = 0;
    if (mysqli_affected_rows($dbc) == 1) {
        $row = mysqli_fetch_array($r, MYSQLI_ASSOC);
        $price_rmb = $row['price_rmb'];
    }

    // check is there a recent transaction?
//    $q = "select * from Transactions where mobile=$mob and menu_id=$menu_id and status='1' order by create_date desc limit 1";
    $q = "select * from Transactions where mobile=$mob and menu_id=$menu_id and (status='1' or status='0') order by create_date desc limit 1";

    $r = @mysqli_query($dbc, $q);

    if (mysqli_affected_rows($dbc) > 0) {
        $row = mysqli_fetch_array($r, MYSQLI_ASSOC);
        $ts = $row['create_date'];
        $ts1 = new DateTime($ts); //, new DateTimeZone('Asia/Singapore'));        
        $ts1 = $ts1->getTimestamp();

        $ts2 = new DateTime('NOW'); //, new DateTimeZone('Asia/Singapore'));        
        $ts2 = $ts2->getTimestamp();
        $status = $row['status'] + 0;

        $diff = $ts2 - $ts1;

        $limit = $status == 0 ? 60 : 600;
        if (strlen(SG_DEMO_CARD) > 0)
            $limit = 20;
        if ($diff < $limit) {
            $wait = $limit - $diff;

            if ($status == 0) {
                echo "<p class=\"error\">有类似的充值请求(相同号码，相同产品)<br>于 $diff 秒之前发生: $ts<br>"
                . "如确实需要充值，请在 $wait 秒后重试...<br>***注意：上次发生的充值尚未成功，可能正在进行中***</p>";
            } else if ($status == 1) {
                echo "<p class=\"error\">有类似的充值请求(相同号码，相同产品)<br>于 $diff 秒之前发生: $ts<br>"
                . "如确实需要充值，请在 $wait 秒后重试...<br>***注意：上次发生的充值已经成功***</p>";
            }
            $body = 'mobile=' . $mob . ' price=rmb ' . $price_rmb;
            mail(EMAIL_ADMIN, 'SgTopup repeat requests', $body, EMAIL_NO_REPLY);
            exit();
        }
    }

    // insert transaction first
    // todo we should deduct balance before actual topup

    $uid = $_SESSION['user_id'];
    $tid = -1;
    $ip = $_SERVER['REMOTE_ADDR'];
    $email = $_SESSION['email'];

    $q = "insert into Transactions (mobile, user_id, user_ip, email, menu_id,     price_rmb,   remark,    status) values " .
            "                      ('$mob', '$uid', '$ip', '$email', '$menu_id', '$price_rmb', '$remark', 0)";
    $r = @mysqli_query($dbc, $q);

    if (mysqli_affected_rows($dbc) == 1) {
        $tid = $dbc->insert_id;
        if ($tid < 0) {
            echo "<p class=\"error\">系统错误 1042:$tid 请稍后再试";
            return;
        }
    } else {
        echo "<p class=\"error\">系统错误 2308, 请稍后再试";
        return;
    }

    // add money transactions
    if (!addMoneyTrans($dbc, $tid, $name0, $uid, $email, -$price_rmb, $mob, $email, $ip)) {
        echo "<p class=\"error\">系统错误 2310, 请稍后再试";
        return;
    }

    list($resp, $out, $url) = call_npn_api($tid, $menu_id, $mob);

    $topup_ok = false;
    $body = '';

    if (count($out) >= 4) {
        $result = $out[0];
        $session = $out[2];
        $balance = $out[3];

        if ($result == 'OK') {
            $topup_ok = true;
            $_SESSION['tid'] = $tid;

            // update Transaction as successful
            $balance_admin = mark_transaction_ok($dbc, $tid, $menu_id, $price_rmb, $email, $mob, $ip, $balance, $resp, $session);

            $_SESSION['balance_admin'] = $balance_admin;

            header("Location:topup_result.php");
            exit();
        } else {
            // failed to top up...
            mail(EMAIL_ADMIN, "网站充值上游失败", $resp, EMAIL_NO_REPLY);
        }
    } else {
    	// response is in wrong format?                
    	//@@@
    	mail(EMAIL_ADMIN, "网站充值上游错误", $resp, EMAIL_NO_REPLY);
    }

    if ($topup_ok == false) {
        // set MoneyTrans record as fail
        $err = '未知错误';
        if (count($out) >= 3) {
            $err = $out[2];
        }
        if ($err == 'Subscriber not found.') {
            $err = '无效号码 或 所选充值不适用于该号码';
        }
        if ($err == 'Sorry. You have entered an invalid number.') {
            $err = '无效号码';
        }

        $q = "update MoneyTrans set status=0 where tid='$tid'";
        @mysqli_query($dbc, $q);

        $q = "update Transactions set fail_reason = '$resp' where tid='$tid'";
        @mysqli_query($dbc, $q);

        $body = "tid = $tid \n menu_id = $menu_id \n price = $price_rmb \n email = $email \n mobile = $mob \n ip = $ip \n" .
                "response = $resp";

        echo "<p class=\"error\">充值失败，原因:<br>" . $err . "<br><br>请检查号码或稍后再试";

        //mail(EMAIL_ADMIN, "网站充值错误! $tid", $body, EMAIL_NO_REPLY);
    }

    //Success
    //Result|TAG|SESSION          |BALANCE|Card-SerialNo|Card-Pin|Card-Passwd|Card-Expi redTime
    //OK    |5  |V1424744833345235|0.00   ||||1
}

function addMoneyTrans($dbc, $tid, $name0, $user_id, $email, $cost, $mob, $by_user, $ip) {
    $q = "insert into MoneyTrans (user_id,    email,    tid,     amount, mobile,  by_user,   user_ip, status, remark) values " .
            "                    ('$user_id', '$email', '$tid', '$cost', '$mob', '$by_user', '$ip',   1,     '$name0')";
    $result = @mysqli_query($dbc, $q);

    return $result;
}

function call_npn_api($tid, $menu_id, $mob) {
    $url = NPN_URL .
            "CMD=RECHARGE" .
            "&TAG=" . $tid .
            "&MENU=" . $menu_id .
            "&ACTION=13810" .
            "&PARAM=" . $mob .
            "&USER=" . NPN_USER .
            "&PASSWD=" . NPN_PASS;


//    $url = NPN_URL;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);

//    curl_setopt($ch, CURLOPT_HEADER, array(
//        "CMD: RECHARGE",
//        "TAG: $tid",
//        "MENU: $menu_id",
//        "ACTION: 13810",
//        "PARAM: $mob" .
//        "USER: " . NPN_USER,
//        "PASSWD: " . NPN_PASS));

    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); // do not print out!
    $resp = trim(curl_exec($ch));
    curl_close($ch);

    $out = array();
    $tok = strtok($resp, '|');

    while ($tok !== false) {
        array_push($out, trim($tok));
        $tok = strtok('|');
    }

    if (strlen(SG_DEMO_CARD) > 0) {
        sleep(2); // fake
    }

    return array($resp, $out, $url);
}

function mark_transaction_ok($dbc, $tid, $menu_id, $cost, $email, $mob, $ip, $balance_admin, $resp, $session) {

    $q = "update Transactions set status=1, session='$session', fail_reason = '$resp' where tid=$tid";
    $r = @mysqli_query($dbc, $q);

    // add a log
    $status = 'ok';
    if (mysqli_affected_rows($dbc) == 1) {
        // add a new log
        $q = "insert into Logs (type, detail) values ('ok', 'tid=$tid updated status to 1')";
        @mysqli_query($dbc, $q);
    } else {
        $status = 'failed';
        // add a new log
        $q = "insert into Logs (type, detail) values ('warn', 'tid=$tid failed to update status to 1')";
        @mysqli_query($dbc, $q);
    }

    // prepare email
    $subject = "SgTopup $tid, $email";
    $balance = get_balance($dbc, $email);

    // update npn balance
    $old_admin_balance = 0;
    $diff = '?';
    $q = "select * from Settings where setting_name = 'npn_balance'";
    $r = @mysqli_query($dbc, $q);
    if (mysqli_affected_rows($dbc) == 1) {
        $row = mysqli_fetch_array($r, MYSQLI_ASSOC);
        $old_admin_balance = safe_value($dbc, $row, 'setting_value');
        if (strlen($old_admin_balance) > 0) {
            $diff = floatval($old_admin_balance) - $balance_admin;
        }
    }

    // update npn balance
    if (strlen(SG_DEMO_CARD) < 1) {
        $q = "update Settings set setting_value = '$balance_admin' where setting_name = 'npn_balance'";
        @mysqli_query($dbc, $q);

        if ($balance_admin < 300) {
            mail(EMAIL_ADMIN_ALL, 'SgTopup 余额不足:$' . $balance_admin, '请尽快充值!!!', EMAIL_NO_REPLY);
        } else if ($balance_admin < 1000) {
            mail('xiaoyi0310@gmail.com', 'SgTopup 余额不足:$' . $balance_admin, '请尽快充值!!!', EMAIL_NO_REPLY);
        }
    }

    // insert mobile numbers for later use
    $q = "insert into Numbers (email, mobile) values ('$email', '$mob')";
    @mysqli_query($dbc, $q);

    // update last use time for mobile number
    $q = "update Numbers set last_date=NOW() where email='$email' and mobile='$mob'";
    @mysqli_query($dbc, $q);

    $body = " tid = $tid \n menu_id = $menu_id \n price = RMB $cost \n mobile = $mob \n" .
            " status = $status \n user = $email \n balance = $balance \n ip = $ip \n" .
            " admin cost = SGD $diff \n admin balance = SGD $balance_admin";

    mail(EMAIL_ADMIN, $subject, $body, EMAIL_NO_REPLY);

    return $balance_admin;
}

include('includes/footer.html');
















