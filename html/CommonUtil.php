<?php

require_once '../mysqli_connect.php';

class CommonUtil {

    private $dbc = null;

    // 1 normal, 2 used, 3 expired

    public function getExpireTs($expire) {

        
        if ($expire >= 1000) {
            return $expire * 60 * 60 / 1000;
        }
        else {
        	return $expire * 24 * 60 * 60;
        }

        return 24 * 60 * 60;
    }

    public function getHongbaoDesc($openid) {
        $dbc = $this->dbc;
        $q = "select * from WXRefer where refid = '$openid'";
        @mysqli_query($dbc, $q);
        $refcnt = mysqli_affected_rows($dbc);

        $q = "select * from WXUserPromo where openid = '$openid' order by create_time";
        $res = @mysqli_query($dbc, $q);
        $cnt_exp = 0;
        $cnt_used = 0;
        $cntok = 0;
        while ($row = mysqli_fetch_array($res, MYSQLI_ASSOC)) {
            $state = $row['state'];
            if ($state == 2) {
                $cnt_used++;
                continue;
            }
            if ($state == 3) {
                $cnt_exp++;
                continue;
            }
            $code = $row['code'];

            $create_time = strtotime($row['create_time']);
            $expire_ts = $this->getExpireTs($row['expire_day']);

            $ts_now = new DateTime('now');
            $ts_now = $ts_now->getTimestamp();
            $diff = $ts_now - $create_time;
            $expired = $diff >= $expire_ts;

            if ($expired) {
                $cnt_exp++;
                continue;
            }

            $ts_exp = "\n有效至 " . date("Y-m-d h:i", $create_time + $expire_ts);

            $amount = $row['amount'];
            $amount = number_format($amount / 100.0, 2);

            if ($code == 'subs') {
                $code = '关注红包';
            } else if ($code == 'refer') {
                $code = '朋友红包';
            }
            $cntok++;
            $desc .= "$code ￥$amount  $ts_exp\n-----\n";
        }
        $desc .= "你有\n - " . $cntok . "个可用红包\n";
        if ($cnt_exp > 0) {
            $desc .= " - " . $cnt_exp . "个过期红包\n";
        }
        if ($cnt_used > 0) {
            $desc .= " - " . $cnt_used . "个已使用红包\n";
        }
        $desc .= " - " . $refcnt . "个推荐好友 (回复 link 获取你的分享链接，通过链接关注的朋友每次充值系统均会送你红包!）";

        return $desc;
    }

    public function getPromo2($openid, $mobile, $price) {
        $dbc = $this->dbc;
        $q = "select * from WXUserPromo where openid = '$openid' and state = 1 order by create_time";

        $res = @mysqli_query($dbc, $q);
        $lst = array();

        $total_amt = 0;

        while ($row = mysqli_fetch_array($res, MYSQLI_ASSOC)) {
            $upid = $row['upid'];
            $code = $row['code'];
            $amount = $row['amount'];
            $promo_mobile = $row['mobile'];

            $create_time = strtotime($row['create_time']);
            $expire_day = $row['expire_day'];
            $ts_now = new DateTime('now');
            $ts_now = $ts_now->getTimestamp();
            $diff = $ts_now - $create_time;

            $expire_ts = $this->getExpireTs($expire_day);

            $expired = $diff >= $expire_ts;

            if ($expired) {
                $q = "update WXUserPromo set state = 3 where upid = $upid";
                @mysqli_query($dbc, $q);
                continue;
            }

            if ($code == 'refer' && $mobile == $promo_mobile) {
                continue;
            }

            if ($code == 'subs') {
                // subs promo can only used by 1 number
                $q = "select tid from WXTrans where mobile = '$mobile' and upcode = 'subs' and state = 3";
                @mysqli_query($dbc, $q);
                if (mysqli_affected_rows($dbc) > 0) {
                    continue;
                }
            }

            if ($total_amt + $amount >= $price) {
                break;
            }

            $total_amt += $amount;

            array_push($lst, $row);
        }
        return $lst;
    }

    public function getPromo($openid, $mobile) {
        $dbc = $this->dbc;
        $q = "select * from WXUserPromo where openid = '$openid' and state = 1 order by create_time";
        $res = @mysqli_query($dbc, $q);
        while ($row = mysqli_fetch_array($res, MYSQLI_ASSOC)) {
            $create_time = strtotime($row['create_time']);
            $expire_day = $row['expire_day'];
            $code = $row['code'];
            $ts_now = new DateTime('now');
            $ts_now = $ts_now->getTimestamp();
            $diff = $ts_now - $create_time;
            $expire_ts = $this->getExpireTs($expire_day);

            if ($diff < $expire_ts) {
                $q = "select tid from WXTrans where mobile = '$mobile' and upcode = '$code' and state = 3";
                @mysqli_query($dbc, $q);
                if (mysqli_affected_rows($dbc) < 1) {
                    return $row;
                }
            }
        }
        return null;
    }

    public function writeInfo($msg) {
        echo '<div style="background-color: rgb(255, 255, 255);" class="clearfix">';
        echo '  <div class="middle-box" style="padding-bottom: 0;">';
        echo "    <p class=\"general-prompt\">$msg</p>";
        echo '  </div>';
        echo '</div>';
    }

    public function writeFooter() {
        echo '<div id="footer" style="padding:8px;margin-bottom: 38px;" >';
        echo '<p style="color:#ddd; " align="center">版权所有 &#169chongzhi.sg 2015-2016</p>';
        echo '<p style="color:#ddd; " align="center"><a href="http://www.miitbeian.gov.cn">粤ICP备16051749号</a> | ';
        echo '<a href="http://www.chongzhi.sg/wx_refundp.php">退款政策</a></p>';
        echo '</div>';
    }

    public function writeHeader($telco, $title) {
        //test color:
//        $telco = 'sgm1';
//        $telco = 'sgsh';
//        $telco = 'sgst';

        echo '<link href="css/bootstrap.min.css" rel="stylesheet">';
        echo '<link href="css/font-awesome.css" rel="stylesheet">';

        echo '<link href="css/toastr.min.css" rel="stylesheet">';

        echo '<link href="css/animate.css" rel="stylesheet">';
        echo '<link href="css/style.css" rel="stylesheet">';

        echo '<!DOCTYPE html><html><head>';
        echo '<link href="css/wx_base.css" rel="stylesheet">';
        echo "<link href=\"css/$telco.css\" rel=\"stylesheet\">";


        echo '<meta charset="utf-8"><title>畅通狮城</title></head><body>';
        echo '<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0">';

        if ($title != null) {

            // create title image and text
            echo '<div class="header-container">';
            echo '  <div class="middle-box text-center" style="font-size: 0; padding: 0;">';
//            echo "    <img style=\"width: 30%;\" src=\"$img\">";
            echo "    <img style=\"width: 30%;\" src=\"images/telco/$telco.png\">";
            echo '    <span style="display: inline-block; width: 60%; color: #fff; font-weight: bold; font-size: 20px; vertical-align: middle; text-align: center;}">';
            echo "      $title";
            echo '    </span>';
            echo '  </div>';
            echo '</div>';
        }
    }

    /*public function writeHeader($img, $title) {
        echo '<link href="css/bootstrap.min.css" rel="stylesheet">';
        echo '<link href="css/font-awesome.css" rel="stylesheet">';

        echo '<link href="css/toastr.min.css" rel="stylesheet">';

        echo '<link href="css/animate.css" rel="stylesheet">';
        echo '<link href="css/style.css" rel="stylesheet">';


        echo '<!DOCTYPE html><html><head>';
        echo '<link href="css/wx_base.css" rel="stylesheet">';


        echo '<meta charset="utf-8"><title>畅通狮城</title></head><body>';
        echo '<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0">';

        if ($title != null) {

            // create title image and text
            echo '<div style="background-color: rgb(211, 1, 50);">';
            echo '  <div class="middle-box text-center" style="font-size: 0px; padding: 0px 0px;">';
            echo "    <img style=\"width: 30%;\" src=\"$img\">";
            echo '    <span style="display: inline-block; width: 60%; color: #fff; font-weight: bold; font-size: 20px; vertical-align: middle; text-align: center;}">';
            echo "      $title";
            echo '    </span>';
            echo '  </div>';
            echo '</div>';
        }
    }*/

    public function writeHeader2($img, $page, $title) {
        echo '<link href="/css/bootstrap.min.css" rel="stylesheet">';
        echo '<link href="/css/font-awesome.css" rel="stylesheet">';

        echo '<link href="/css/toastr.min.css" rel="stylesheet">';

        echo '<link href="/css/animate.css" rel="stylesheet">';
        echo '<link href="/css/style.css" rel="stylesheet">';


        echo '<!DOCTYPE html><html><head>';
        echo '<link href="/css/wx_base.css" rel="stylesheet">';


        echo "<meta charset=\"utf-8\"><title>$page</title></head><body>";
        echo '<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0">';

        if ($title != null) {

            // create title image and text
            echo '<div style="background-color: rgb(211, 1, 50);">';
            echo '  <div class="middle-box text-center" style="font-size: 0px; padding: 0px 0px;">';
            echo "    <img style=\"width: 30%;\" src=\"$img\">";
            echo '    <span style="display: inline-block; width: 60%; color: #fff; font-weight: bold; font-size: 20px; vertical-align: middle; text-align: center;}">';
            echo "      $title";
            echo '    </span>';
            echo '  </div>';
            echo '</div>';
        }
    }

    public function getStateStr($s) {
        $state = '未知';

        if ($s == 0) {
            $state = '【未付款】';
        } else if ($s == 1) {
            $state = '【付款中】';
        } else if ($s == 2) {
            $state = '【充值中】';
        } else if ($s == 3) {
            $state = '充值成功';
        } else if ($s >= 100 && $s <= 199) {
            $state = '【失败】';
        } else if ($s == 200) {
            $state = '退款成功';
        }

        return $state;
    }

    public function __construct($pdbc) {
        $this->dbc = $pdbc;
    }

    public function getUser($openid) {
        if ($openid == null || strlen($openid) < 1) {
            return false;
        }

        $dbc = $this->dbc;

        $q = "select * from WXUsers where openid = '$openid'";

        $r_users = @mysqli_query($dbc, $q);
        $user = mysqli_fetch_array($r_users, MYSQLI_ASSOC);
        $row_cnt = mysqli_affected_rows($dbc); // mysqli_num_rows($dbc);
        if ($row_cnt == 1) {
            return $user;
        }
        return false;
    }

    public function getRefId($openid) {
        $q = "select * from WXRefer where openid = '$openid'";
        $dbc = $this->dbc;

        $res = @mysqli_query($dbc, $q);
        if (mysqli_affected_rows($dbc) > 0) {
            $row = mysqli_fetch_array($res, MYSQLI_ASSOC);
            $refopen = $row['refid'];
            $user = getUser($refopen);
            return $user != false ? $user['uid'] : -1;
        }
        return -1;
    }

    public function getCreateDbUser($openid) {

        if ($openid == null || strlen($openid) < 1) {
            return false;
        }

        $dbc = $this->dbc;

        $q = "select * from WXUsers where openid = '$openid'";

        $r_users = @mysqli_query($dbc, $q);
        $user = mysqli_fetch_array($r_users, MYSQLI_ASSOC);
        $row_cnt = mysqli_affected_rows($dbc); // mysqli_num_rows($dbc);
        if ($row_cnt < 1) {
            // new user!
            $q = "insert into WXUsers (openid, status, balance, name, create_time, update_time, last_active) values " .
                    "('$openid', 1, 0, '', now(), now(), now())";
            // echo '<br>'.$q;
            $r_users = @mysqli_query($dbc, $q);
            // echo '<br>$r_users='.$r_users;

            $rows = mysqli_affected_rows($dbc);
            // echo '<br>new user rows='.$rows;

            if ($rows == 1) {
                $this->addLog('user', 'ok', -1, "inserted openid=$openid");
                // successfully created user
                // echo '<br>ok';
                $user = mysqli_fetch_array($r_users, MYSQLI_ASSOC);
            } else {
                $this->addLog('user', 'err', -1, "insert failed openid=$openid");
            }
        } else if ($row_cnt == 1) {
            // $this->addLog('user', 'ok', -1, "fetched openid=$openid");
        } else {
            // echo '<br>wrong';
            $this->addLog('user', 'err', -1, "insert failed openid=$openid");
            $user = false;
        }

        // update user
        $q = "update WXUsers set last_active = now() where openid = '$openid'";
        @mysqli_query($dbc, $q);

        return $user;
    }

    public function addLog($kind, $level, $id1, $msg) {
        if ($kind == null)
            $kind = '';
        if ($level == null)
            $level = 'ok';
        if ($id1 == null)
            $id1 = -1;
        if ($msg == null)
            $msg = '';

        // add a new log
        $q = "insert into WXLog (kind, id1, level, detail) values ('$kind', '$id1', '$level', '$msg')";
        @mysqli_query($this->dbc, $q);
    }

    function getPriceString($price) {
        return number_format($price / 100.0, 2);
    }

    function getPriceInt($price_sgd, $rate) {

        /*if (strlen(SG_DEMO_CARD) > 0) {
            return 1;
        }

        $price = intval($price_sgd) * floatval($rate);
        $price = round($price / 10) * 10;
        return round($price);*/
        return 1;
    }

    function getExRate($dbc) {
        $q = "select * from Settings where setting_name = 'ex_rate'";
        $res = @mysqli_query($dbc, $q);
        if (mysqli_affected_rows($dbc) > 0) {
            $row = mysqli_fetch_array($res, MYSQLI_ASSOC);
            $rate = $row['setting_value'];
            return floatval($rate);
        }
        return 5.166;
    }

    function getCnyPrice($dbc, $pid) {

        $prod = getProduct($dbc, $pid);

        $price_cny = $prod['price_cny'] + 0;
        $price_sgd = $prod['price_sgd'] + 0;

        $q = "select * from Settings where setting_name = 'ex_rate'";
        $res = @mysqli_query($dbc, $q);
        if (mysqli_affected_rows($dbc) > 0) {
            $setting = mysqli_fetch_array($res, MYSQLI_ASSOC);
            $rate = floatval($setting['setting_value']);

            if ($rate > 4.5) {
                $price = $price_sgd * $rate;
                $price = round($price / 10) * 10;
                return round($price);
            }
        }

        if ($price_cny > 100) {
            return $price_cny;
        }

        return 1000000;
    }

    function getProduct($dbc, $pid) {
        $q = "select * from WXProducts where pid = '$pid'";
        $reslst = @mysqli_query($dbc, $q);
        $prod = flase;
        if (mysqli_affected_rows($dbc) == 1) {
            $prod = mysqli_fetch_array($reslst, MYSQLI_ASSOC);
        }
        return $prod;
    }

    function genAllUrl($toURL, $paras) {
        $allUrl = null;
        if (null == $toURL) {
            die("toURL is null");
        }
        if (strripos($toURL, "?") == "") {
            $allUrl = $toURL . "?" . $paras;
        } else {
            $allUrl = $toURL . "&" . $paras;
        }
        return $allUrl;
    }

    // generate random 16 char string
    function create_noncestr($length = 16) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $str = "";

        while ($length > 0) {
            $min = 0;
            $max = strlen($chars) - 1;
            $idx = mt_rand($min, $max);
            $str .= substr($chars, $idx, 1);
            $length -= 1;
        }
        return $str;
//        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
//        $str = "";
//        for ($i = 0; i < $length; $i++) {
//            $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
//        }
//        return $str;
    }

    // split parameters to an indexed array
    function splitParaStr($src, $token) {
        $resMap = array();
        $items = explode($token, $src);
        foreach ($items as $item) {
            $paraAndValue = explode("=", $item);
            if ($paraAndValue != "") {
                $resMap[$paraAndValue[0]] = $paraAndValue[1];
            }
        }
        return $resMap;
    }

    // turn "" to null
    function trimString($value) {
        $ret = null;
        if (null != $value) {
            $ret = $value;
            if (strlen($ret) == 0) {
                $ret = null;
            }
        }
        return $ret;
    }

    // sort param alphabetically, 
    // use url key-value format to generate a string
    function formatQueryParaMap($paraMap, $urlencode) {
        $buff = "";
        ksort($paraMap); //key-sort
        foreach ($paraMap as $k => $v) {
            if (null != $v && "null" != $v && "sign" != $k) {
                if ($urlencode) {
                    $v = urldecode($v);
                }
                $buff .= $k . "=" . $v . "&";
            }
        }
        $reqPar='';
        if (strlen($buff) > 0) { // remove last '&'
            $reqPar = substr($buff, 0, strlen($buff) - 1);
        }
        return $reqPar;
    }

    // same as formatQueryParaMap, key to lowercase
    function formatBizQueryParaMap($paraMap, $urlencode) {
        $buff = "";
        ksort($paraMap);
        foreach ($paraMap as $k => $v) {
            if ($urlencode) {
                $v = urlencode($v);
            }
            $buff .= strtolower($k) . "=" . $v . "&";
        }
        $reqPar='';
        if (strlen($buff) > 0) {
            $reqPar = substr($buff, 0, strlen($buff) - 1);
        }
        return $reqPar;
    }

    // array to xml
    function arrayToXml($arr) {
        $xml = "<xml>";

        foreach ($arr as $key => $val) {
            if (is_numeric($val)) {
                $xml .= "<" . $key . ">" . $val . "</" . $key . ">";
            } else {
                $xml .= "<" . $key . "><![CDATA[" . $val . "]]></" . $key . ">";
            }
        }
        $xml .= "</xml>";
        return $xml;
    }

}
?>



