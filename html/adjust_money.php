
<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

session_start(); // access existing sesison
require('../mysqli_connect.php');

if (!isset($_SESSION['user_id']) || !isset($_SESSION['email']) || !isset($_SESSION['user_level'])) {
    require('includes/login_functions.inc.php');
    header("Location: login.php");
    exit();
//    redirect_user('login.php');
}

$email = $_SESSION['email'];

$page_title = '';
include('includes/header.html');
require('includes/login_functions.inc.php');

$level = $_SESSION['user_level'];

if ($level < 2) {
    echo 'Page not found.';
    exit();
} else {

    $page_title = '调整余额';

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {

        require('includes/config.inc.php');
        $amt1 = safe_value($dbc, $_POST, 'amount1');
        $amt2 = safe_value($dbc, $_POST, 'amount2');
        $type = safe_value($dbc, $_POST, 'trans_type');
        $em1 = strtolower(safe_value($dbc, $_POST, 'email1'));
        $em2 = strtolower(safe_value($dbc, $_POST, 'email2'));

        if (!filter_var($em1, FILTER_VALIDATE_EMAIL)) {
            echo "<p class=\"error\">- '$em1' 不是一个合法的Email";
            $em1 = false;
        } else if ($em1 != $em2) {
            echo "<p class=\"error\">- 两次Email不相同";
            $em1 = false;
        }

        if ($type != 'add' && $type != 'minus') {
            echo "<p class=\"error\">- 请选择[添加]余额或[扣除]余额";
            $type = false;
        }

        if (!verify_amount($dbc, $amt1)) {
            echo "<p class=\"error\">- '$amt1' 不是一个有效金额";
            $amt1 = false;
        } else if ($amt1 != $amt2) {
            echo "<p class=\"error\">- 两次金额不相同";
            $amt1 = false;
        }

        if (!$amt1 || !$type || !$em1) {
            echo "<p class=\"error\">- 请再次输入";
        } else {
            if ($type == 'minus') {
                $amt1 = -$amt1;
            }
            // email exist?
            $q = "select * from Users where email = '$em1'";
            $r = @mysqli_query($dbc, $q);
            if (mysqli_affected_rows($dbc) == 1) {
                $row = mysqli_fetch_array($r, MYSQLI_ASSOC);
                $active = $row['active'];
                if (strlen($active) > 0) {
                    echo "<p class=\"error\">- 用户 '$em1' 尚未激活";
                } else {
                    $ip = $_SERVER['REMOTE_ADDR'];
                    $uid = $row['user_id'];
                    $q = "insert into MoneyTrans (user_id, email, amount, by_user, user_ip, status, remark) values('$uid', '$em1', '$amt1', '$email', '$ip', 1, '管理员调整')";
                    $r = @mysqli_query($dbc, $q);
                    if (mysqli_affected_rows($dbc) == 1) {
                        $mtid = $dbc->insert_id;
                        $balance = get_balance($dbc, $em1);
                        $_SESSION['adjust_email'] = $em1;
                        $_SESSION['new_amount'] = $balance;
                        $_SESSION['adjust_amt'] = $amt1;
                        header("Location: adjust_result.php");
                        
                        $body = " mtid = $mtid \n user = $em1 \n added = rmb $amt1 \n by $email \n ip = $ip \n balance = $balance";
                        mail(EMAIL_XY, "网站调整余额 $mtid", $body, EMAIL_NO_REPLY);

                        exit();
                    } else {
                        echo "<p class=\"error\">充值失败，请稍后再试</p>";
                    }
                }
            } else {
                echo "<p class=\"error\">- 用户 '$em1' 不存在</p>";
            }
        }
    }

    // display form
    echo '为用户添加或扣除现金余额';

    echo '<br><br><form action="adjust_money.php" method="post">';
    echo '<table cellspacing="0" cellpadding="6" align="center">';

    echo '<tr><td class="topup_header" colspan="2">1. 输入用户Email</td></tr>';
    echo '<tr><td align="right">Email</td>    <td align="left"><input type="text" name="email1" value="" placeholder="" required/></td></tr>';
    echo '<tr><td align="right">重复Email</td> <td align="left"><input type="text" name="email2" value="" placeholder="" required/></td></tr>';

    echo '<tr><td class="topup_header" colspan="2">2. 选择"添加"或"扣除"余额</td></tr>';
    echo '<tr><td colspan="2"><input type="radio" name="trans_type" value="add" required>添加</></td></tr>';
    echo '<tr><td colspan="2"><input type="radio" name="trans_type" value="minus">扣除</></td></tr>';

    echo '<tr><td class="topup_header" colspan="2">3. 输入金额</td></tr>';
    echo '<tr><td align="right">金额</td>    <td align="left"><input type="text" name="amount1" value="" placeholder="0.00" required/></td></tr>';
    echo '<tr><td align="right">重复金额</td> <td align="left"><input type="text" name="amount2" value="" placeholder="0.00" required/></td></tr>';

    echo '<tr><td colspan="2"><input class="btn_submit" type="submit" name="submit" value="确定" /><td></tr>';
    echo '</table></form>';
}

function verify_amount($dbc, $amt) {
    // /^([1-9]*|\d*\.\d{1}?\d*)$/
    // /^\d*\.?\d*$/
    // /^(\d{1,4}|\d{1,4}\.\d{1,2})$/
    $q = "select * from Settings where setting_name = 'max_cash_adjust'";
    $r = @mysqli_query($dbc, $q);
    $max = 500;
    if (mysqli_affected_rows($dbc) == 1) {
        $row = mysqli_fetch_array($r, MYSQLI_ASSOC);
        $max = floatval(safe_value($dbc, $row, 'setting_value', 500));
    }
    
    if (preg_match('/^(\d{1,4}|\d{1,4}\.\d{1,2})$/', $amt)) {
        if ($amt <= $max && $amt > 0) {
            return true;
        }
    }
    return false;
}

include('includes/footer.html');
