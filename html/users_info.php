<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

session_start(); // access existing sesison
require('../mysqli_connect.php');

if (!isset($_SESSION['user_id']) || !isset($_SESSION['email'])) {
    require('includes/login_functions.inc.php');
    header("Location: login.php");
    exit();
//    redirect_user('login.php');
}

$page_title = '用户资料';
include('includes/header.html');
require('includes/login_functions.inc.php');
require('includes/config.inc.php');

$email = trim($_SESSION['email']);
$level = $_SESSION['user_level'];

if ($level >= 2) {
    $q = "select email, last_login from Users where active is NULL order by user_id desc";
    $r = @mysqli_query($dbc, $q);
    if (mysqli_affected_rows($dbc) > 0) {
        echo '<table align="center" cellpadding="10" cellspacing="0"';
        echo '<tr><th>Email</th><th>余额</th><th>最后登录</th></tr>';

        while ($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
            $ue = $row['email'];
            $balance = get_balance($dbc, $ue);
            $balance = number_format($balance, 2, '.', '');
            $last_login = $row['last_login'];
            echo "<tr><td align=\"left\">$ue</td><td align=\"right\">¥$balance</td><td>$last_login</td></tr>";
        }
        echo '</table>';
    } else {
        echo "没有记录";
    }
} else {
    echo "页面不存在... ";
}

include('includes/footer.html');
