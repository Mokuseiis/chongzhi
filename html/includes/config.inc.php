<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define('LIVE', FALSE);

//xiaoyi0310@gmail.com
define('EMAIL_XY', 'xiaoyi0310@gmail.com,26959915@qq.com');
define('EMAIL_ADMIN', 'longfan1011@outlook.com');
define('EMAIL_ADMIN_ALL', 'xiaoyi0310@gmail.com,longfan1011@outlook.com');

define('EMAIL_NO_REPLY', 'From: no-reply@chongzhi.sg');

define('BASE_URL', 'http://119.29.245.63/'); // qcloud

define('MYSQL', '../mysqli_connect.php');

// Adjust the time zone for PHP 5.1 and greater:27 
date_default_timezone_set('Asia/Singapore');

// ************ SETTINGS ************ 
// ************ ERROR MANAGEMENT ************ 
// Create the error handler:

function my_error_handler($e_number, $e_message, $e_file, $e_line, $e_vars) {
// Build the error message:40   
    $message = "An error occurred in script '$e_file' on line $e_line: $e_message\n";
// Add the date and time:
    $message .= "Date/Time: " . date('n-j-Y H:i:s') . "\n";
    if (!LIVE) { // Development (print the error).46 47     
        // Show the error message:  
        echo '<div class="error">' . nl2br($message);

        // Add the variables and a backtrace:
        echo '<pre>' . print_r($e_vars, 1) . "\n";
        debug_print_backtrace();
        echo '</pre></div>';
    } else { // Don't show the error:
        // Send an email to the admin:
        $body = $message . "\n" . print_r($e_vars, 1);
        mail(EMAIL_ADMIN, 'Site Error!', $body, 'From: email@example.com');
        // Only print an error message if the error isn't a notice:
        if ($e_number != E_NOTICE) {
            echo '<div class="error">A system error occurred. We apologize for the inconvenience.</div><br />';
        }
    } // End of !LIVE IF.
} // End of my_error_handler() definition.
//
// Use my error handler:
set_error_handler('my_error_handler');

// ************ ERROR MANAGEMENT ************
