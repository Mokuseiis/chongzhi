<?php

require_once('../mysqli_connect.php');
require_once("CommonUtil.php");
require_once('../wxsdk/WXBizMsgCrypt.php');

function get_by_curl($url, $post = false) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    if ($post) {
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    }
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}

function https_request($url) {
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $data = curl_exec($curl);
    if (curl_errno($curl)) {
        return 'ERROR ' . curl_error($curl);
    }
    curl_close($curl);
    return $data;
}

function getOpenidFromCode($code) {

    $url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid=' . AppId .
            '&secret=' . AppSecret . '&code=' . $code . '&grant_type=authorization_code';

    $access_token_json = https_request($url);

    $access_token_array = json_decode($access_token_json, true);
    $access_token = $access_token_array['access_token'];
    $openid = $access_token_array['openid'];

    return $openid;
}

function writeHtml1($user) {

    echo '<div style="background-color: rgb(255, 255, 255);" class="clearfix">';
    echo '  <div class="middle-box" style="padding-bottom: 0px;">';
	
	echo '    <p class="general-prompt">输入好友的【预付卡】电话号码，当Ta第一次充值时，你就会获得￥3元红包哦！</p>';
    echo '    <p class="general-prompt">好友的手机号码</p>';
    echo '    <div class="input-group input-group-lg m-b">';
    echo '      <span class="input-group-addon">+65</span>';
    echo '      <input class="topup-number form-control" type="text" placeholder="手机号码" type="tel" inputmode="numeric" pattern="[0-9]*">';
    echo '    </div>';
    echo '  </div>';
    writeHtmlForm();
    echo '</div>';
}

function writeHtmlForm() {
    // create form
    echo '<div>';
    echo '<div class="middle-box">';
    echo '<form method="post" action="http://www.chongzhi.sg/wx_refer.php/" onsubmit="return fillupform()">';
    echo '  <input type="hidden" class="submit-topup-number" name="mobile" value="">';
    echo '  <input type="hidden" class="submit-topup-openid" name="openid" value="">';
    echo '  <button class="btn btn-primary btn-block btn-lg" type="submit">提交</button>';
    echo '</form>';

    echo '</div>';
    echo '</div>';
}

function writeHtmlScripts($openid) {


    echo '<div class="alert-layer">';
    echo '  <div class="alert-layer-mask"></div>';
    echo '  <div class="middle-box" style="padding-top: 48px; height: 100%; overflow-y: auto;">';
    echo '    <div class="alert-layer-background">';
    echo '      <a class="pull-right alert-layer-close">&times;</a>';
    echo '      <h3 class="alert-layer-title"><i class="fa fa-warning"></i> <span>错误</span></h3>';
    echo '      <p><strong>请填写正确的预付手机号码。</strong></p>';
    echo '    </div>';
    echo '  </div>';
    echo '</div>';

    echo '<script src="/static/jquery-1.11.3-min.js"></script>';
    echo '<script src="/static/jquery.formatter.min.js"></script>';

    echo "<script>";

    echo "$('.alert-layer-close').click(function() {";
    echo "  $('body').removeClass('show-alert');";
    echo "});";

    echo "var show_alert = function() {";
    echo '  $(\'body\').addClass(\'show-alert\');';
    echo "};";

    echo '$(\'.topup-number\').formatter({';
    echo "  'pattern': '{{9999}} {{9999}}'";
    echo "});";

    echo "function fillupform() {";

    echo "  var phonenumber = $('.topup-number').val();";
    echo "  phonenumber = phonenumber.split(' ').join('');";

    if (strlen(SG_DEMO_CARD) > 0) {
        echo " phonenumber = '80001234'; ";
    } else {
        echo "  if (!(/^\d{8}$/.test(phonenumber)) || (phonenumber.charAt(0) != '9' && phonenumber.charAt(0) != '8') ) {";
        echo "    show_alert();";
        echo "    return false;";
        echo "  }";
    }

    echo "  $('.topup-number').val(''); "; // make it empty
    echo "  $('.button').attr('disabled', 'true'); ";

    echo '  var $plan = $(\'.plan-selection-container span.plan-selection-selected\');';
    echo "  var plan = 'voice';";

    echo '  var $submit_number = $(\'.submit-topup-number\');';
    echo '  var $submit_openid =  $(\'.submit-topup-openid\');';

    echo '  $submit_number.val(phonenumber);';
    echo '  $submit_openid.val(\'' . $openid . '\'); ';
    echo '  return true; ';
    echo "}";

    echo '</script>';
}

function showProduct($dbc, $util, $code) {

    $util->writeHeader("/images/ctsc256.jpg", "推荐朋友");

    $openid = getOpenidFromCode($code);
    $nba = $_GET['nba'];
    if ($nba == 'pacers' && strlen($openid) < 1) {
        $openid = 'oawUFwbvmUbnVT_NHrXsZyk0PntA';
    }

    $user = $util->getCreateDbUser($openid);
    // check $user is false?

    if ($user == false || $user == null) {
        $util->writeInfo("请用微信客户端打开本页.");
        $util->writeFooter();
        exit();
    }

    $level = $user['level'];
    if ($debug && $level < 2) {
        $util->writeInfo("十分抱歉, 本站正在维护中, 请10分钟后再试.");
        $util->writeFooter();
        exit();
    }

    writeHtml1($user, $telco);
    writeHtmlScripts($openid);

    $util->writeFooter();
}

$util = new CommonUtil($dbc);

if (isset($_GET['nba']) || isset($_GET['code'])) {
    $query = $_SERVER['QUERY_STRING'];
    $code = $_GET['code'];

    showProduct($dbc, $util, $code);
}
else if (isset($_POST['openid'])) {
	
	$openid = $_POST['openid'];
	$mobile = $_POST['mobile'];

	$user = $util->getCreateDbUser($openid);
	$uid = $user['uid'];

	$q = "select refid from WXRefer where mobile = '$mobile'";
	@mysqli_query($dbc, $q);
	$insert = false;
	if (mysqli_affected_rows($dbc) < 1) {
		$insert = true;
		$q = "insert into WXRefer (uid, openid, mobile, create_time) " .
		     " values ($uid, '$openid', '$mobile', now()) ";
		@mysqli_query($dbc, $q);
	}

	$util->writeHeader("/images/ctsc256.jpg", "推荐朋友");

	if ($insert) {
		$util->writeInfo("更新成功!");
	} else {
		$util->writeInfo("该号码已经被人推荐过了哦!");
	}

	$util->writeFooter();
	
}


?>















