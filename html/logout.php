<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

session_start(); // access existing sesison

if (!isset($_SESSION['user_id'])) {
    require('includes/login_functions.inc.php');
    redirect_user();
}
else {
    $_SESSION = array(); // clear variables
    session_destroy();
    setcookie('PHPSESSID', '', time()-3600, '/', '', 0);
//    setcookie('user_id', '', time()-1800, '/', '', 0, 0);
//    setcookie('nick', '', time()-1800, '/', '', 0, 0);
}

$page_title = '已登出';

include('includes/header.html');

echo "<h3>已登出<h3>"
. "<p>请继续使用我们的服务</p>";

include('includes/footer.html');

?>

