<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require('includes/config.inc.php');
$page_title = '重置密码';
include('includes/header.html');

require('../mysqli_connect.php');

// todo check loggedin

$display_form = false;

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    require('includes/login_functions.inc.php');

    $display_form = true;

    $up1 = isset($_POST['pass1']) ? $_POST['pass1'] : '';
    $up2 = isset($_POST['pass2']) ? $_POST['pass2'] : '';
    $email = isset($_SESSION['forgot_email']) ? $_SESSION['forgot_email'] : '';

//    echo "$up1, $up2, $email <br>"; // todo remove

    $up1 = mysqli_real_escape_string($dbc, $up1);

    $valid = true;
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        echo '<p class="error">无效Email</p>';
        $valid = false;
    } else if (!verify_pass($up1)) {
        echo '<p class="error">密码必须至少8位 包含大小写字母和数字</p>';
        $valid = false;
    } else if ($up1 != $up2) {
        echo '<p class="error">两次密码不相同</p>';
        $valid = false;
    }

    if ($valid) {
        $q = "update Users set pass=SHA1('$up1') where email='$email'";
        $r = @mysqli_query($dbc, $q);
        if (mysqli_affected_rows($dbc) == 1) {
            echo '<p>重置密码成功！</p>';
            $display_form = false;
            $q = "delete from UserPass where email = '$email'";
            @mysqli_query($dbc, $q);
        } else {
            echo '<p class="error">系统错误 1132，请稍后再试</p>';
        }
    }
    mysqli_close($dbc);
} else {
    $display_form = true;
    $pass = isset($_GET['x']) ? $_GET['x'] : '';
    $e = isset($_GET['y']) ? $_GET['y'] : '';
    $email = '';
    // user click email
    if (strlen($pass) > 0 && strlen($e) > 0) {
        $q = "select * from UserPass where pass_sys = SHA1('$pass') and email_hash = SHA1('$e')";
        $r = @mysqli_query($dbc, $q);
        if (mysqli_affected_rows($dbc) == 1) {
            $row = mysqli_fetch_array($r, MYSQLI_ASSOC);
            $email = $row['email'];

            $ts = $row['pass_sys_date'];
            $ts1 = new DateTime($ts); //, new DateTimeZone('Asia/Singapore'));        
            $ts1 = $ts1->getTimestamp();
            $ts2 = new DateTime('NOW'); //, new DateTimeZone('Asia/Singapore'));        
            $ts2 = $ts2->getTimestamp();
            $diff = $ts2 - $ts1;
            
            if ($diff < 0 || $diff > 600) {
                $display_form = false;
                echo '链接过期...';
            } else {
                $_SESSION['forgot_email'] = $email;
                $q = "select * from Users where email = '$email'";
                $r = @mysqli_query($dbc, $q);
                if (mysqli_affected_rows($dbc) != 1) {
                    $display_form = false;
                    echo '系统错误 1230，请稍后再试';
                }
            }
        } else {
            $display_form = false;
            echo '系统错误 1310，请稍后再试';
        }
    } else {
        $display_form = false;
        echo '系统错误 1254，请稍后再试';
    }
}

if ($display_form) {
    echo $email;
    echo '<form action="reset_forgot.php" method="post">';
    echo '<table align="center">';
    echo '<tr><td align="right">新密码</td><td><input type="password" name="pass1"/></td></tr>';
    echo '<tr><td align="right">重复密码</td><td><input type="password" name="pass2"/></td></tr>';
    echo '<tr><td></td><td><input type="submit" name="submit" value="修改密码"></td></tr>';
    echo '</table>';
    echo '</form>';
}

include('includes/footer.html');
?>

