<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

session_start(); // access existing sesison
require('../mysqli_connect.php');
require('includes/login_functions.inc.php');



if (!isset($_SESSION['user_id']) || !isset($_SESSION['email']) || !isset($_SESSION['user_level'])) {
    header("Location: login.php");
    exit();
//    redirect_user('login.php');
}

$page_title = '调整金额结果';
include('includes/header.html');
require('includes/config.inc.php');

$level = safe_value($dbc, $_SESSION, 'user_level');

if ($level === '' || $level < 2) {
    echo 'Page not found.';
    exit();
}

$email = safe_value($dbc, $_SESSION, 'adjust_email', false);
$amount = safe_value($dbc, $_SESSION, 'new_amount', false);
$adjust = safe_value($dbc, $_SESSION, 'adjust_amt', false);

if ($email && $amount && $adjust) {
    echo "<h3>调整金额成功!</h3><br><br>";
    echo '<table align="center" cellpadding = "6">';
    echo "<tr><td align=\"right\">Email</td><td align=\"right\">$email</td></tr>";
    echo "<tr><td align=\"right\">添加\扣除</td><td align=\"right\">¥$adjust</td></tr>";
    echo "<tr><td align=\"right\">最新余额</td><td align=\"right\">¥$amount</td></tr>";
    echo "</table>";
    echo '<br><br><a href="adjust_money.php">返回</a>';
    
    mail($email, "SgTopup 现金余额调整", "你的余额被调整：rmb $adjust \n 最新余额: rmb $amount", EMAIL_NO_REPLY);
}

include('includes/footer.html');
