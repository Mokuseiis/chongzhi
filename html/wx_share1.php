<?php

require_once("CommonUtil.php");
require_once('../wxsdk/WXBizMsgCrypt.php');
require_once('../mysqli_connect.php');

require_once 'includes/config.inc.php';

function get_by_curl($url, $post = false) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	if ($post) {
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
	}
	$result = curl_exec($ch);
	curl_close($ch);
	return $result;
}

function https_request($url) {
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	$data = curl_exec($curl);
	if (curl_errno($curl)) {
		return 'ERROR ' . curl_error($curl);
	}
	curl_close($curl);
	return $data;
}

function getOpenidFromCode($code) {

	$url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid=' . AppId .
	'&secret=' . AppSecret . '&code=' . $code . '&grant_type=authorization_code';

	$access_token_json = https_request($url);

    //echo $access_token_json;

	$access_token_array = json_decode($access_token_json, true);
    // echo $access_token_array;
	$access_token = $access_token_array['access_token'];
	$openid = $access_token_array['openid'];

	return $openid;
}


$util = new CommonUtil($dbc);

$refid = isset($_GET['refid']) ? $_GET['refid'] : '';
if (strlen($refid) < 1) {
	$code = isset($_GET['code']) ? $_GET['code'] : '';
	$refid = strlen($code) > 0 ? getOpenidFromCode($code) : '';
}

if (strlen($refid) > 0) {

	$src = isset($_GET['src']) ? $_GET['src'] : 'unknown';

    // echo "<html><header><title>畅通狮城</title></header><body style=\"background-color: rgb(99,186,177);\">";
    echo "<html><header><title>不是分享此页！要点进绿色按钮分享~</title>";
    echo '<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0">';
    echo '<style>green{background-color:#57c4a7;}</style>';
    echo '</header><body>';

    $link = "http://www.chongzhi.sg/wx_share2.php?refid=$refid&src=$src";

	echo "<table width=\"100%\" >";
	echo "<tr><td>";
    echo "<a href=\"$link\">";
    echo '<img width="100%" src="images/share/share1_3.jpg"></img>';
    echo '</a>';
    echo "</tr></td>";
    echo "<tr><td align=\"center\"> <a href=\"http://chongzhi.sg/wx_hongbaop.php\">查看红包规则</a>  </td></tr>";
    echo "<tr><td align=\"center\">  <img width=\"10%\" src=\"images/icon/point_down.gif\"></img>  </td></tr>";
    echo "<tr><td align=\"center\">  <a href=\"$link\"><img width=\"80%\" src=\"images/icon/btn-share-page.png\"></img></a>   </td></tr>";
    echo "</table>";

    echo "</body></html>";

    $user = $util->getUser($refid);
	$uid = $refid;
	if ($user != false) {
		$uid = $user['uid'];
	}
	$q = "insert into WXStats (kind, tag1, uid1, create_time) values ('share1', '$src', $uid, now() )";
	@mysqli_query($dbc, $q);

	//mail(EMAIL_ADMIN, "share1, uid=$uid", "mark", EMAIL_NO_REPLY);

    //mail(EMAIL_ADMIN, "share1, refid=$refid", "mark", EMAIL_NO_REPLY);
} else {
    echo "";
}
?>





















